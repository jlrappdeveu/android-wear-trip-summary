/*
 * Copyright Notice: This software is proprietary to Jaguar Land Rover Limited.  
 *  ©  2015 Jaguar Land Rover Limited
 * 
 */
package com.jlr.ica.csi;

import com.jlr.ica.csi.CANServerSDK.DoorID;
import com.jlr.ica.csi.CANServerSDK.DoorStatus;

/**
 * The IVehicleEquipmentListener interface provides access to specific equipment
 * available in the vehicle. A class wishing to implement this is interface is
 * to register itself with the interface and implement all the abstract methods
 * contained within.
 * <p>
 * See {@link CANServerSDK#registerIVehicleEquipmentListener}.
 * <p>
 * CAN Signals used in this interface include:
 * <p>
 * <ul>
 * <li>Rain Sense Status<b> - Not currently available.</b></li>
 * <li>Heating Ventilation and Air Conditioning (HVAC) Auto Blower Status</li>
 * <li>Rear Climate On/Off Setting</li>
 * <li>Door Status</li>
 * <li>Wiper Status</li>
 * 
 * </ul>
 * 
 * @author JLR
 * @version 2015.01
 * @since 1.0
 * 
 * @see IHighFrequencyDataListener
 * @see ILowFrequencyDataListener
 * @see ICurrentLocationListener
 * @see IEcoCurrentTripListener
 * @see ISeatBeltStatusListener
 * @see IVehicleSuspensionListener
 * @see IOffRoadStatusListener
 * 
 */
public interface IVehicleEquipmentListener {

	// @formatter:off
	/**
	 * <b>This API is not currently available.</b>
	 * <p>  
	 * 
	 * The position of the front wiper setting has changed.
	 * If set, RainSenseStatus = 1 (Rain sensor control), 
	 * the front wiper shall operate in the auto mode if 
	 * selected via the column wiper stalk. If RainSenseStatus = 0, 
	 * (intermittent mode), the front wiper shall operate in the 
	 * intermittent mode if selected via the column wiper stalk. 
	 * 
	 * @param rainWiperStatus. Valid range:
	 *   	    <ul>
     *             <li>0&nbsp&nbsp	Intermittent Mode</li>
     *             <li>1&nbsp&nbsp	Automatic Mode</li>   
     *          </ul>
     *          <P>
	 * @param rainWiperStatus Error:&nbsp&nbsp&nbsp{@link CANServerSDK#INT_ERROR}
	 * 
	 */
	// @formatter:on	
	public void onRainWiperStatusChanged(int rainWiperStatus);

	// @formatter:off
	/**
	 * The driver selected HVAC Blower Auto intensity Level.
	 * 
	 * @param HVACBlowerStatus Valid range:
	 * 
	 *         <ul>
     *             <li>0&nbsp&nbsp	Undefined</li>
     *             <li>1&nbsp&nbsp	Low Intensity</li>
     *             <li>2&nbsp&nbsp	Medium Intensity</li>   
     *             <li>3&nbsp&nbsp	High Intensity</li>   
     *          </ul>
     *          <P>
	 * @param HVACBlowerStatus Error:&nbsp&nbsp&nbsp{@link CANServerSDK#INT_ERROR}
	 */
	// @formatter:on
	public void onHVACBlowerStatusChanged(int HVACBlowerStatus);

	// @formatter:off
	/**
	 * The rear climate setting has been turned on/off.
	 * 
	 * @param rearClimateStatus Valid range:
	 * 
	 *         <ul>
     *             <li>0&nbsp&nbsp	Rear climate off</li>
     *             <li>1&nbsp&nbsp	Rear climate on</li>
     *          </ul>
     *          <P>
	 * @param rearClimateStatus Error:&nbsp&nbsp&nbsp{@link CANServerSDK#INT_ERROR}
	 */
	// @formatter:on
	public void onRearClimateStatusChanged(int rearClimateStatus);

	// @formatter:off
	/**
	 * The status of each individual door.
	 * 
	 * @param doorID Valid range:
	 * 
	 *         <ul>
     *             <li>0&nbsp&nbsp	DRIVERS_DOOR</li>
     *             <li>1&nbsp&nbsp	FRONT_PASSENGER_DOOR</li>
     *             <li>2&nbsp&nbsp	REAR_LEFT_DOOR</li>
     *             <li>3&nbsp&nbsp	REAR_RIGHT_DOOR</li>             
     *             <li>4&nbsp&nbsp	TAILGATE</li>
     *             <li>5&nbsp&nbsp	BONNET</li>
     *          </ul>
     *          <P>
	 * @param doorStatus Valid range:
	 * 
	 *         <ul>
     *             <li>0&nbsp&nbsp	OPEN</li>
     *             <li>1&nbsp&nbsp	CLOSED</li>
     *             <li>2&nbsp&nbsp	ERROR</li>
     *          </ul> 
	 */
	// @formatter:on
	public void onDoorStatusChanged(DoorID doorID, DoorStatus doorStatus);

	// @formatter:off
	/**
	 * The wiper status has changed.
	 * 
	 * @param wiperStatus Valid range:
	 * 
	 *         <ul>
     *             <li>0&nbsp&nbsp	Wipers Off</li>
     *             <li>1&nbsp&nbsp	Intermittent Wipe</li>
     *             <li>2&nbsp&nbsp	Normal (Slow) Continuous Wipe</li>
     *             <li>3&nbsp&nbsp	Fast Continuous Wipe</li>             
     *          </ul>
     *          <P>
	 * @param wiperStatus Error:&nbsp&nbsp&nbsp{@link CANServerSDK#INT_ERROR}
	 * 
	 */
	// @formatter:on
	public void onWiperStatusChanged(int wiperStatus);
}
