/*
 * Copyright Notice: This software is proprietary to Jaguar Land Rover Limited.  
 *  ©  2015 Jaguar Land Rover Limited
 * 
 */
package com.jlr.ica.csi;

//@formatter:off
/**
 * The IHighFrequencyDataListener interface represents specific dynamic CAN
 * signals whose values change at a high frequency.
 * 
 * A class wishing to implement this is interface is to register itself with the
 * interface and implement all the abstract methods contained within.
 * <p>
 * See {@link CANServerSDK#registerIHighFrequencyDataListener}. 
 * <p>
 * CAN Signals used in this interface include:
 * <p>
 * <ul>
 * <li>Vehicle Speed</li>
 * <li>Throttle Pedal Position</li>
 * <li>Steering Wheel Angle</li>
 * <li>Lateral Acceleration</li> 
 * </ul>
 * 
 * @author JLR
 * @version 2015.01
 * @since 1.0
 * 
 * @see ILowFrequencyDataListener
 * @see ICurrentLocationListener
 * @see IEcoCurrentTripListener 
 * @see ISeatBeltStatusListener
 * @see IVehicleSuspensionListener
 * @see IVehicleEquipmentListener
 * @see IOffRoadStatusListener
 * 
 */
//@formatter:on
public interface IHighFrequencyDataListener {

	// @formatter:off
	/**
	 * The vehicle speed has changed.
	 * 
	 * @param vehicleSpeed Valid range: 0 - 320 Km/h 
	 * 
	 * @param vehicleSpeed Error:&nbsp&nbsp&nbsp {@link CANServerSDK#DBL_ERROR}
	 *       
	 */
	// @formatter:on
	public void onVehicleSpeedChanged(double vehicleSpeed);

	// @formatter:off
	/**
	 * The driver demanded throttle pedal position has changed in %.
	 * 
	 * @param pedalPosition Valid range: 0 - 108.5 %
	 * 
	 * @param pedalPosition Error:&nbsp&nbsp&nbsp {@link CANServerSDK#DBL_ERROR}
	 */
	// @formatter:on
	public void onPedalPositionChanged(double pedalPosition);

	// @formatter:off
	/**
	 * This API provides the a measurement of the steering wheel position.
	 * The angle is measured as referenced from a 0 degree straight-ahead 
	 * position.
	 * 
	 * @param steeringWheelAngle Valid range: +780 degrees to -780 degrees
	 * 
	 * @param steeringWheelAngle Error:&nbsp&nbsp&nbsp {@link CANServerSDK#INT_ERROR}
	 */
	// @formatter:on
	public void onSteeringWheelAngleChanged(int steeringWheelAngle);

	// @formatter:off
	/**
	 * Value of lateral acceleration, positive in a right (clockwise) turn.
	 * 
	 * @param lateralAcceleration Valid range: (-11) - 11 m/s/s 
     *           
	 * @param lateralAcceleration Error:&nbsp&nbsp&nbsp{@link CANServerSDK#DBL_ERROR} 
	 * 
	 */
	// @formatter:on
	public void onLateralAccelerationChanged(double lateralAcceleration);

}
