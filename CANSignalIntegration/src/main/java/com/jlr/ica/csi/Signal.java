/*
 * Copyright Notice: This software is proprietary to Jaguar Land Rover Limited.  
 *  ©  2015 Jaguar Land Rover Limited
 * 
 */
package com.jlr.ica.csi;

/**
 * <h1>The Signal Class.</h1>
 */
public class Signal {

	/**
	 * Instantiates a new signal.
	 * 
	 * @param name
	 *            the name
	 * @param blockNumber
	 *            the blockNumber
	 * @param byteSwap
	 *            the byteSwap
	 * @param startBit
	 *            the start bit
	 * @param length
	 *            the length
	 * @param offset
	 *            the offset
	 * @param scale
	 *            the scale
	 */
	public Signal(final String name, final int blockNumber, final int byteSwap,
			final int startBit, final int length, final int offset,
			final float scale) {
		this.name = name;
		this.blockNumber = blockNumber;
		this.byteSwap = byteSwap;
		this.startBit = startBit;
		this.length = length;
		this.offset = offset;
		this.scale = scale;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Name: " + this.name + ", Block Number: " + this.blockNumber
				+ ", Byte Swap = " + this.byteSwap + ", Start Bit = "
				+ this.startBit + ", Length = " + this.length + ", Offset = "
				+ this.offset + ", Scale = " + this.scale;
	}

	/**
	 * Gets the name.
	 * 
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 * 
	 * @param name
	 *            the new name
	 */
	public void setName(final String name) {
		this.name = name;
	}

	/**
	 * Gets the block number of the multiplexed CAN signal.
	 * 
	 * @return the blockNumber
	 */
	public final int getBlockNumber() {
		return blockNumber;
	}

	/**
	 * Sets the blockNumber of the multiplexed CAN signal.
	 * 
	 * @param blockNum
	 *            the block number
	 */
	public void setBlockNumber(final int blockNum) {
		this.blockNumber = blockNum;
	}

	/**
	 * Determines whether the CAN signal is to be byte swapped prior to
	 * obtaining its value.
	 * 
	 * @return the byteSwap
	 */
	public final int getByteSwap() {
		return byteSwap;
	}

	/**
	 * Sets the status of whether this CAN signal requires byte swapping prior
	 * to determining its value.
	 * 
	 * @param byteSwap
	 *            the block number
	 */
	public void setByteSwap(final int byteSwap) {
		this.byteSwap = byteSwap;
	}

	/**
	 * Gets the start bit.
	 * 
	 * @return the start bit
	 */
	public int getStartBit() {
		return startBit;
	}

	/**
	 * Sets the start bit.
	 * 
	 * @param startBit
	 *            the new start bit
	 */
	public void setStartBit(final int startBit) {
		this.startBit = startBit;
	}

	/**
	 * Gets the length.
	 * 
	 * @return the length
	 */
	public int getLength() {
		return length;
	}

	/**
	 * Sets the length.
	 * 
	 * @param length
	 *            the new length
	 */
	public void setLength(final int length) {
		this.length = length;
	}

	/**
	 * Gets the offset.
	 * 
	 * @return the offset
	 */
	public int getOffset() {
		return offset;
	}

	/**
	 * Sets the offset.
	 * 
	 * @param offset
	 *            the new offset
	 */
	public void setOffset(final int offset) {
		this.offset = offset;
	}

	/**
	 * Gets the scale.
	 * 
	 * @return the scale
	 */
	public float getScale() {
		return scale;
	}

	/**
	 * Sets the scale.
	 * 
	 * @param scale
	 *            the new scale
	 */
	public void setScale(final float scale) {
		this.scale = scale;
	}

	/** The name. */
	private String name;

	/** The blockNumber. */
	private int blockNumber;

	/** The byteSwap. */
	private int byteSwap;

	/** The start bit. */
	private int startBit;

	/** The length. */
	private int length;

	/** The offset. */
	private int offset;

	/** The scale. */
	private float scale;

}
