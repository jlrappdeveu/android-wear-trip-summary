/*
 * Copyright Notice: This software is proprietary to Jaguar Land Rover Limited.  
 *  ©  2015 Jaguar Land Rover Limited
 * 
 */

package com.jlr.ica.csi;

import android.location.Location;

/**
 * The ICurrentLocationListener interface will report the location for a given
 * route.
 * 
 * A class wishing to implement this is interface is to register itself with the
 * interface and implement all the abstract methods contained within.
 * <p>
 * See {@link CANServerSDK#registerICurrentLocationListener}.
 * <p>
 * CAN Signals used in this interface include:
 * <p>
 * <ul>
 * <li>Vehicle Location</li>
 * </ul>
 * 
 * @author JLR
 * @version 2015.01
 * @since 1.0
 * 
 * @see IHighFrequencyDataListener
 * @see ILowFrequencyDataListener
 * @see IEcoCurrentTripListener
 * @see ISeatBeltStatusListener
 * @See IVehicleSuspensionListener
 */
public interface ICurrentLocationListener {

	// @formatter:off
	/**
	 * The location position has changed.
	 * 
	 * @param newLocation New location object.
	 *   
	 */
	// @formatter:on
	public void onNewLocationUpdate(Location newLocation);
}
