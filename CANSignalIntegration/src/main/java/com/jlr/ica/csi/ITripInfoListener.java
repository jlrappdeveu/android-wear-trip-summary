/*
 * Copyright Notice: This software is proprietary to Jaguar Land Rover Limited.
 *  ©  2015 Jaguar Land Rover Limited
 *
 */


package com.jlr.ica.csi;

/**
 * The IADASListener interface will report CAN data related to the
 * ADAS-data collated by the vehicle for the current trip
 *
 * A class wishing to implement this is interface is to register itself with the
 * interface and implement all the abstract methods contained within.
 * <p>
 * See {@link CANServerSDK#registerITripInfoListener}.
 * <p>
 * CAN Signals used in this interface include:
 * <p>
 * <ul>
 * <li>Traffic Sign Recognised Speed</li>
 * <li>Traffic Sign Recognised Speed Units</li>

 * </ul>
 *
 * @author JLR
 * @version 2015.01
 * @since 1.0
 *
 * @see IHighFrequencyDataListener
 * @see ILowFrequencyDataListener
 * @see ICurrentLocationListener
 * @see ISeatBeltStatusListener
 * @see IVehicleEquipmentListener
 * @see IVehicleSuspensionListener
 * @see IEcoCurrentTripListener
 * @see IADASListener
 */
public interface ITripInfoListener {

    // @formatter:off
    /**
     * HUD requirement status
     *
     * @param hudNaviControlStatus Valid range:
     *   	     <ul>
     *             <li>0&nbsp&nbsp	HUD Navi display not required</li>
     *             <li>1&nbsp&nbsp	HUD Navi display required</li>
     *             <li>2&nbsp&nbsp	Reserved</li>
     *             <li>3&nbsp&nbsp	Reserved</li>
     *          </ul>
     *
     * @param hudNaviControlStatus Error:&nbsp&nbsp&nbsp{@link CANServerSDK#INT_ERROR}
     *
     */
    // @formatter:on
    public void onHUDNaviControlReqChanged(int hudNaviControlStatus);

    // @formatter:off
    /**
     * HUD current count down bar for navigation instruction
     *
     * @param hudNaviCurrentCountdownBar Valid range:
     *   	    <ul>
     *             <li>0&nbsp&nbsp	0 countdown bars</li>
     *             <li>1&nbsp&nbsp	1 countdown bar/h</li>
     *             <li>2&nbsp&nbsp	2 countdown bar</li>
     *             <li>3&nbsp&nbsp	3 countdown bar</li>
     *             <li>4&nbsp&nbsp	4 countdown bars</li>
     *             <li>5&nbsp&nbsp	5 countdown bar/h</li>
     *             <li>6&nbsp&nbsp	6 countdown bar</li>
     *             <li>7&nbsp&nbsp	7 countdown bar</li>
     *             <li>8&nbsp&nbsp	8 countdown bars</li>
     *             <li>9&nbsp&nbsp	9 countdown bar/h</li>
     *             <li>10&nbsp&nbsp	10 countdown bar</li>
     *             <li>11&nbsp&nbsp	Reserved</li>
     *             <li>12&nbsp&nbsp	Reserved</li>
     *             <li>13&nbsp&nbsp	Reserved</li>
     *             <li>14&nbsp&nbsp	Reserved</li>
     *             <li>15&nbsp&nbsp	Reserved</li>
     *          </ul>
     *
     * @param hudNaviCurrentCountdownBar Error:&nbsp&nbsp&nbsp{@link CANServerSDK#INT_ERROR}
     *
     */
    // @formatter:on
    public void onHUDNaviCurrentCountdownBarChanged(int hudNaviCurrentCountdownBar);


}
