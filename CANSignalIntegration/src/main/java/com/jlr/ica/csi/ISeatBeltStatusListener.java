/*
 * Copyright Notice: This software is proprietary to Jaguar Land Rover Limited.  
 *  ©  2015 Jaguar Land Rover Limited
 * 
 */
package com.jlr.ica.csi;

import com.jlr.ica.csi.CANServerSDK.SeatBeltID;
import com.jlr.ica.csi.CANServerSDK.SeatBeltStatus;

/**
 * The ISeatBeltStatusListener interface provides access to the seat belt status
 * of the vehicle. Note, this only whether the seat belt is buckled or not. If
 * the seat is occupied, but the seat belt is un-buckled, only the un-buckled
 * status is reported.
 * 
 * A class wishing to implement this is interface is to register itself with the
 * interface and implement all the abstract methods contained within.
 * <p>
 * See {@link CANServerSDK#registerISeatBeltStatusListener}.
 * <p>
 * CAN Signals used in this interface include:
 * <p>
 * <ul>
 * <li>Drivers Seatbelt Status<b> - Not currently available.</b></li>
 * <li>Front Passenger Seatbelt Status<b> - Not currently available.</b></li>
 * <li>Rear Left Passenger SeatBelt Status</li>
 * <li>Rear Middle Passenger SeatBelt Status</li>
 * <li>Rear Right Passenger SeatBelt Status</li>
 * <li>Third Row Left Passenger SeatBelt Status<b> - Not currently
 * available.</b></li>
 * <li>Third Row Right Passenger SeatBelt Status<b> - Not currently
 * available.</b></li>
 * </ul>
 * 
 * @author JLR
 * @version 2015.01
 * @since 1.0
 * 
 * @see IHighFrequencyDataListener
 * @see ILowFrequencyDataListener
 * @see ICurrentLocationListener
 * @see IEcoCurrentTripListener
 * @See IVehicleSuspensionListener
 * @see IOffRoadStatusListener
 * @see IVehicleEquipmentListener
 */
public interface ISeatBeltStatusListener {

	// @formatter:off
	/**
	 * The seat belt status has changed. 
	 * 
	 * @param seatBeltID. The following seat belt IDs are supported:
	 *   	    <ul>
     *             <li>0&nbsp&nbsp	DRIVERS_SEAT<b> - Not currently available.</b></li>
     *             <li>1&nbsp&nbsp	FRONT_PASSENGER<b> - Not currently available.</b></li>   
     *             <li>2&nbsp&nbsp	REAR_LEFT_PASSENGER</li>   
     *             <li>3&nbsp&nbsp	REAR_MIDDLE_PASSENGER</li>   
     *             <li>4&nbsp&nbsp	REAR_RIGHT_PASSENGER</li>                      
     *             <li>5&nbsp&nbsp	THIRD_ROW_LEFT_PASSENGER<b> - Not currently available.</b></li>   
     *             <li>6&nbsp&nbsp	THIRD_ROW_RIGHT_PASSENGER<b> - Not currently available.</b></li>                          
     *          </ul>
     *           
	 * @param seatBeltStatus Valid range:
	 *    	    <ul>
     *             <li>0&nbsp&nbsp	UNBUCKLED</li>
     *             <li>1&nbsp&nbsp	BUCKLED</li>                          
     *             <li>2&nbsp&nbsp	ERROR</li>
     *          </ul> 
	 * 
	 */
	// @formatter:on	
	public void onSeatBeltStatusChanged(SeatBeltID seatBeltID,
			SeatBeltStatus seatBeltStatus);
}
