/*
 * Copyright Notice: This software is proprietary to Jaguar Land Rover Limited.  
 *  ©  2015 Jaguar Land Rover Limited
 * 
 */

package com.jlr.ica.csi;

/**
 * The IEcoCurrentTripListener interface will report CAN data related to the
 * Eco-data collated by the vehicle for the current trip
 * 
 * A class wishing to implement this is interface is to register itself with the
 * interface and implement all the abstract methods contained within.
 * <p>
 * See {@link CANServerSDK#registerIEcoCurrentTripListener}.
 * <p>
 * CAN Signals used in this interface include:
 * <p>
 * <ul>
 * <li>Eco Current Speed Score</li>
 * <li>Eco Current Throttle Score</li>
 * <li>Eco Current Brake Score</li>
 * <li>Eco Current Fuel Economy Units</li>
 * <li>Eco Current Trip Score</li>
 * <li>Eco Current Trip Distance</li>
 * <li>Eco Stop Start Timer<b> - Not currently available.</b></li>
 * <li>Eco Current Average Speed</li>
 * <li>Fuel Economy Units</li>
 * <li>Stop Start Status<b> - Not currently available.</b></li>
 * <li>Transmission Upshift Indicator Status</li>
 * </ul>
 * 
 * @author JLR
 * @version 2015.01
 * @since 1.0
 * 
 * @see IHighFrequencyDataListener
 * @see ILowFrequencyDataListener
 * @see ICurrentLocationListener
 * @see ISeatBeltStatusListener
 * @see IVehicleEquipmentListener
 * @see IVehicleSuspensionListener
 * @see IADASListener
 */
public interface IEcoCurrentTripListener {

	// @formatter:off
	/**
	 * The Driver rating speed feedback score for current trip has changed.
	 * 
	 * @param currSpeedScore Valid range: 1.0 - 5.0
     *
	 * @param currSpeedScore Error:&nbsp&nbsp&nbsp   {@link CANServerSDK#INT_ERROR}
	 */
	// @formatter:on
	public void onCurrentSpeedScorechanged(double currSpeedScore);

	// @formatter:off
	/**
	 * The driver rating throttle feedback score for current trip has changed.
	 * 
	 * @param currThrottleScore Valid range: 1.0 - 5.0
	 * 
	 * @param currThrottleScore Error:&nbsp&nbsp&nbsp   {@link CANServerSDK#INT_ERROR} 
	 * 
	 */
	// @formatter:on
	public void onCurrentThrottleScoreChanged(double currThrottleScore);

	// @formatter:off
	/**
	 * The driver rating brake feedback score for current trip has changed.
	 * 
	 * @param currBrakeScore Valid range: 1.0 - 5.0
	 * 
	 * @param currBrakeScore Error:&nbsp&nbsp&nbsp   {@link CANServerSDK#INT_ERROR}  
	 * 
	 */
	// @formatter:on
	public void onCurrentBrakeScoreChanged(double currBrakeScore);

	// @formatter:off
	/**
	 * The fuel economy for current trip changed. Note, units are depending on 
	 * value of FuelEconomyunits
	 * 
	 * @param currFuelEconomy Valid range: 0 - 99
	 * 
	 * @param currFuelEconomy Error:&nbsp&nbsp&nbsp   {@link CANServerSDK#DBL_ERROR} 
	 * 
	 */
	// @formatter:on
	public void onCurrentFuelEconomyChanged(double currFuelEconomy);

	// @formatter:off
	/**
	 * The feedback summary percentage for current trip has changed.
	 * 
	 * @param currTripScore Valid range: 0 - 100 %
	 * 
	 * @param currTripScore Error:&nbsp&nbsp&nbsp   {@link CANServerSDK#INT_ERROR}
	 * 
	 */
	// @formatter:on
	public void onCurrentTripScoreChanged(int currTripScore);

	// @formatter:off
	/**
	 * The distance travelled for current trip has changed.
	 * 
	 * @param currTripDistance  Valid range: 0 -  1677721 Km
	 * 
	 * @param currTripDistance  Error:&nbsp&nbsp&nbsp   {@link CANServerSDK#DBL_ERROR} 
	 *  
	 */
	// @formatter:on
	public void onCurrentTripDistanceChanged(double currTripDistance);

	// @formatter:off
	/**
	 * <b>This API is not currently available.</b>
	 * <p> 
	 * 
	 * The period for which ECO Start/Stop was active for
	 * current trip changed.
	 * 
	 * @param currEcoStopTimer Valid range: Unknown 
	 * 
	 * @param currEcoStopTimer Error:&nbsp&nbsp&nbsp   {@link CANServerSDK#INT_ERROR} 
	 *  
	 */
	// @formatter:on
	public void onCurrentEcoStopTimerChanged(double currEcoStopTimer);

	// @formatter:off
	/**
	 * The average speed over duration current trip has changed.
	 * Note, units are depending on value of FuelEconomyunits. 
	 * 
	 * @param currTripAverageSpeed Valid range: 0 - 509 Km/h 
	 * 
	 * @param currTripAverageSpeed Error:&nbsp&nbsp&nbsp   {@link CANServerSDK#INT_ERROR} 
	 * 
	 * @see onFuelEconomyUnitsChanged.
	 * 
	 */
	// @formatter:on
	public void onCurrentTripAverageSpeedChanged(int currTripAverageSpeed);

	// @formatter:off
	
	/**
	 * The fuel economy units has changed.
	 * 
	 * @param economyUnits Valid range:
	 * 
	 *         <ul>
     *             <li>0&nbsp&nbsp	MPG</li>
     *             <li>1&nbsp&nbsp	MPL</li>
     *             <li>2&nbsp&nbsp	KM/L</li>   
     *             <li>3&nbsp&nbsp	l/100KM</li>   
     *             <li>4&nbsp&nbsp	Unknown</li>   
     *             <li>5&nbsp&nbsp	Initialisation</li>                      
     *          </ul>
     *           
	 *  @param economyUnits Error:&nbsp&nbsp&nbsp{@link CANServerSDK#INT_ERROR}  
	 */
	// @formatter:on
	public void onFuelEconomyUnitsChanged(int economyUnits);

	// @formatter:off
	/**
	 * <b>This API is not currently available.</b>
	 * <p>
	 * Status of the Eco-Start Stop-Start Feature.
	 * 
	 * @param stopStartStatus Valid range:
	 *   	    <ul>
     *             <li>0&nbsp&nbsp	STARTUP</li>
     *             <li>1&nbsp&nbsp	DEACTIVATED</li>
     *             <li>2&nbsp&nbsp	DESELECTED</li>   
     *             <li>3&nbsp&nbsp	STOP AVAILABLE</li>   
     *             <li>4&nbsp&nbsp	STOP NOT AVAILABLE</li>   
     *             <li>5&nbsp&nbsp	STOPPED</li>                      
     *             <li>6&nbsp&nbsp	PRESS CLUTCH PEDAL</li>   
     *             <li>7&nbsp&nbsp	SELECT NEUTRAL</li>             
     *             <li>8&nbsp&nbsp	PRESS BRAKE PEDAL</li>                      
     *             <li>9&nbsp&nbsp	IMMINENT START WARNING</li>   
     *             <li>10&nbsp&nbsp	ERROR</li>                      
     *             <li>11&nbsp&nbsp	EXT TEMP</li>             
     *             <li>12&nbsp&nbsp	DPF REGEN</li>                 
     *             <li>13&nbsp&nbsp	BATT CHARGE</li>
     *             <li>14&nbsp&nbsp	SYS DEMAND</li>
     *             <li>15&nbsp&nbsp	A/C DEMAND</li>                                  
     *          </ul>
     *           
	 * @param stopStartStatus Error:&nbsp&nbsp&nbsp{@link CANServerSDK#INT_ERROR} 
	 * 
	 */
	// @formatter:on
	public void onStopStartStatusChanged(int stopStartStatus);


	// @formatter:off
	/**
	 * Indication to prompt the driver to perform an upshift (only applies in Dynamic and Permanent (S) Tip Mode)
	 *
	 * @param transShiftInd Valid range:
	 *   	    <ul>
	 *             <li>0&nbsp&nbsp	No indication</li>
	 *             <li>1&nbsp&nbsp	Upshift warning</li>
	 *             <li>2&nbsp&nbsp	Not used</li>
	 *             <li>3&nbsp&nbsp	Not used</li>
	 *          </ul>
	 *
	 * @param transShiftInd Error:&nbsp&nbsp&nbsp{@link CANServerSDK#INT_ERROR}
	 *
	 */
	// @formatter:on
	public void onTransShiftIndChanged(int transShiftInd);
}
