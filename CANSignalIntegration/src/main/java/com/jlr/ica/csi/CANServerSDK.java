/*
 * Copyright Notice: This software is proprietary to Jaguar Land Rover Limited.
 *  ©  2015 Jaguar Land Rover Limited
 * 
 */
package com.jlr.ica.csi;

import java.io.IOException;
import java.math.BigInteger;
import java.net.DatagramPacket;
import java.net.MulticastSocket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import android.app.Application;
import android.content.Context;
import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;
import android.util.Pair;

// @formatter:off
/**
 * <h1>CAN Signal Integration (CSI)</h1>
 * <h1>The CANServerSDK Class.</h1>
 */
// @formatter:on
public class CANServerSDK {

	/** The tag. */
	static String TAG = "[CANServerSDK]";

	/** The Constant MAX_UDP_DATAGRAM_LEN. */
	private static final int MAX_UDP_DATAGRAM_LEN = 25000;

	/** The CAN data payload max number of bits */
	private static final int MAX_BITS = 64;

	/** The b. */
	private static CANServerSDK CANServerSDKInstance = null;

	/** The app context. */
	private static ArrayList<Application> appContextList = new ArrayList<Application>();

	/** The activity context. */
	private static ArrayList<Context> activityContextList = new ArrayList<Context>();

	/** The array list of high frequency data listeners. */
	private static ArrayList<IHighFrequencyDataListener> highFreqDataListenerList = new ArrayList<IHighFrequencyDataListener>();

	/** The array list of low frequency data listeners. */
	private static ArrayList<ILowFrequencyDataListener> lowFreqListenerList = new ArrayList<ILowFrequencyDataListener>();

	/** The array list of eco data current trip listeners. */
	private static ArrayList<IEcoCurrentTripListener> ecoCurrentTripListenerList = new ArrayList<IEcoCurrentTripListener>();

	/** The array list of location listeners. */
	private static ArrayList<ICurrentLocationListener> locationListenerList = new ArrayList<ICurrentLocationListener>();

	/** The array list of seat belt listeners. */
	private static ArrayList<ISeatBeltStatusListener> seatBeltListenerList = new ArrayList<ISeatBeltStatusListener>();

	/** The array list of suspension height listeners. */
	private static ArrayList<IVehicleSuspensionListener> suspensionListenerList = new ArrayList<IVehicleSuspensionListener>();

	/** The array list of vehicle equipment listeners. */
	private static ArrayList<IVehicleEquipmentListener> equipmentListenerList = new ArrayList<IVehicleEquipmentListener>();

	/** The array list of vehicle off-road listeners. */
	private static ArrayList<IOffRoadStatusListener> offRoadListenerList = new ArrayList<IOffRoadStatusListener>();

	/** The array list of vehicle ADAS listeners. */
	private static ArrayList<IADASListener> adasListenerList = new ArrayList<IADASListener>();

	/** The array list of vehicle Trip Info listeners. */
	private static ArrayList<ITripInfoListener> tripInfoListenerList = new ArrayList<ITripInfoListener>();

	/** The udp running. */
	private static boolean udpAlreadyRunning = false;

	/** The can signal map. */
	private static Map<Pair<String, String>, ArrayList<Signal>> canSignalMap = new HashMap<Pair<String, String>, ArrayList<Signal>>();

	/** The current values map. */
	private static Map<String, Double> currentValuesMap = new HashMap<String, Double>();

	/** The Async callback lookup map table. */
	private static Map<String, Runnable> aSyncMap = new HashMap<String, Runnable>();

	/** The application registered flag. */
	private static boolean appRegistered = false;

	/** The application not registered exception message. */
	private final String AppNotRegistered = "AppNotRegistered";

	/** The port number supplied is out of range */
	private final String PortOutOfRange = "PortOutOfRange";

	/** The port number supplied is already in use */
	private final String PortAlreadyInUse = "PortAlreadyInUse";

	/** The index of the bus ID in the translation file . */
	private final int BUS_ID_INDEX = 0;
	/** The index of the CAN ID in he translation file . */
	private final int CAN_ID_INDEX = 1;
	/** The index of the signal name in the translation file . */
	private final int SIG_NAME_INDEX = 2;
	/** The index of the block number in the translation file . */
	private final int BLOCK_NUM_INDEX = 3;
	/** The index of the byte swap in the translation file . */
	private final int BYTE_SWAP_INDEX = 4;
	/** The index of the start bit in the translation file . */
	private final int START_BIT_INDEX = 5;
	/** The index of the number of bits in the translation file . */
	private final int NUM_BIT_INDEX = 6;
	/** The index of the offset in the translation file . */
	private final int OFFSET_INDEX = 7;
	/** The index of the scaling in the translation file . */
	private final int SCALING_INDEX = 8;

	/** The seat belt enumeration */
	public enum SeatBeltID {
		DRIVERS_SEAT, FRONT_PASSENGER, REAR_LEFT_PASSENGER, REAR_MIDDLE_PASSENGER, REAR_RIGHT_PASSENGER, THIRD_ROW_LEFT_PASSENGER, THIRD_ROW_RIGHT_PASSENGER
	}

	/** The seat belt status enumeration */
	public enum SeatBeltStatus {
		UNBUCKLED, BUCKLED, ERROR
	}

	/** The suspension ID enumeration */
	public enum SuspensionID {
		FRONT_LEFT, FRONT_RIGHT, REAR_LEFT, REAR_RIGHT
	}

	/** The vehicle door ID enumeration */
	public enum DoorID {
		DRIVERS_DOOR, FRONT_PASSENGER_DOOR, REAR_LEFT_DOOR, REAR_RIGHT_DOOR, TAILGATE, BONNET
	}

	/** The vehicle door status ID enumeration */
	public enum DoorStatus {
		OPEN, CLOSED, ERROR
	}

	/** The Integer Error value for all int values out of range */
	public final static int INT_ERROR = -999;

	/** The Double Error value for all double values out of range */
	public final static double DBL_ERROR = -999.0;

	// @formatter:off
	// Eco-data Current Trip Keys *******************************************************************************************
	// @formatter:on

	/** The TripCurrSpeedFdbScr currentValuesMap key . */
	private final String TripCurrSpeedFdbScr = "TripCurrSpeedFdbScr";
	/** The TripCurrThrottleFdbScr currentValuesMap key . */
	private final String TripCurrThrottleFdbScr = "TripCurrThrottleFdbScr";
	/** The TripCurrBrakeFdbScr currentValuesMap key . */
	private final String TripCurrBrakeFdbScr = "TripCurrBrakeFdbScr";
	/** The TripCurrFuelEconomy currentValuesMap key . */
	private final String TripCurrFuelEconomy = "TripCurrFuelEconomy";
	/** The TripCurrFdbSummary currentValuesMap key . */
	private final String TripCurrFdbSummary = "TripCurrFdbSummary";
	/** The TripCurrDistance currentValuesMap key . */
	private final String TripCurrDistance = "TripCurrDistance";
	/** The TripCurrECOStopTimer currentValuesMap key . */
	private final String TripCurrECOStopTimer = "TripCurrECOStopTimer";
	/** The TripCurrAvSpeed currentValuesMap key . */
	private final String TripCurrAvSpeed = "TripCurrAvSpeed";
	/** The FuelEconomyUnits currentValuesMap key . */
	private final String FuelEconomyUnits = "FuelEconomyUnits";
	/** The MHStopStartStatus currentValuesMap key . */
	private final String MHStopStartStatus = "MHStopStartStatus";
	/** The TransShiftInd current ValuesMap key. */
	private final String TransShiftInd = "TransShiftInd";

	// @formatter:off
	// High Frequency Data Keys *********************************************************************************************
	// @formatter:on

	/** The VehicleSpeed currentValuesMap key . */
	private final String VehicleSpeed = "VehicleSpeed";
	/** The PedalPos currentValuesMap key . */
	private final String PedalPos = "PedalPos";
	/** The SteeringWheelAngle currentValuesMap key . */
	private final String SteeringWheelAngle = "SteeringWheelAngle";
	/** The LateralAcceleration currentValuesMap key . */
	private final String LateralAcceleration = "LateralAcceleration";

	// @formatter:off
	// Low Frequency Data Keys *********************************************************************************************
	// @formatter:on

	/** The PowerMode currentValuesMap key . */
	private final String PowerMode = "PowerMode";
	/** The BrakePressure currentValuesMap key . */
	private final String BrakePressure = "BrakePressure";
	/** The DistanceToEmpty currentValuesMap key . */
	private final String DistanceToEmpty = "DistanceToEmpty";
	/** The GearLeverPosition currentValuesMap key . */
	private final String GearLeverPosition = "GearLeverPosition";
	/** The LowFuelLevel currentValuesMap key . */
	private final String LowFuelLevel = "LowFuelLevel";
	/** The EPBStatus currentValuesMap key . */
	private final String EPBStatus = "EPBStatus";



	 // @formatter:off
	// Seat belt Data Keys *********************************************************************************************
	// @formatter:on
	/** The DrivSeatBeltBcklState currentValuesMap key . */
	private final String DrivSeatBeltBcklState = "DrivSeatBeltBcklState";
	/** The BeltReminderSensor currentValuesMap key . */
	private final String BeltReminderSensor = "BeltReminderSensor";
	/** The BeltReminderSensorRL currentValuesMap key . */
	private final String BeltReminderSensorRL = "BeltReminderSensorRL";
	/** The BeltReminderSensorRM currentValuesMap key . */
	private final String BeltReminderSensorRM = "BeltReminderSensorRM";
	/** The BeltReminderSensorRR currentValuesMap key . */
	private final String BeltReminderSensorRR = "BeltReminderSensorRR";
	/** The BeltRmderSensorRow3L currentValuesMap key . */
	private final String BeltRmderSensorRow3L = "BeltRmderSensorRow3L";
	/** The BeltRmderSensorRow3R currentValuesMap key . */
	private final String BeltRmderSensorRow3R = "BeltRmderSensorRow3R";

	// @formatter:off
	// Suspension Data Keys *********************************************************************************************
	// @formatter:on
	/** The front left suspension height */
	private final String SuspensionHeightFL = "SuspensionHeightFL";
	/** The front right suspension height */
	private final String SuspensionHeightFR = "SuspensionHeightFR";
	/** The rear left suspension height */
	private final String SuspensionHeightRL = "SuspensionHeightRL";
	/** The rear right suspension height */
	private final String SuspensionHeightRR = "SuspensionHeightRR";


	// @formatter:off
	// Vehicle equipment Data Keys ***************************************************************************************
	// @formatter:on
	/** The rain sense wiper setting */
	private final String RainSenseStatus = "RainSenseStatus";
	/** The driver selected HVAC setting */
	private final String HVACAutoBlowerStatus = "HVACAutoBlowerStatus";
	/** The rear climate on/off setting */
	private final String RearSystemOnCmd = "RearSystemOnCmd";
	/** The Drivers Door */
	private final String DoorDrivers = "DoorDrivers";
	/** The front passenger Door */
	private final String DoorPassenger = "DoorPassenger";
	/** The Rear Left Door */
	private final String DoorRearLeft = "DoorRearLeft";
	/** The Rear Right Door */
	private final String DoorRearRight = "DoorRearRight";
	/** The Tailgate Door */
	private final String DoorTailgate = "DoorTailgate";
	/** The Bonnet Door */
	private final String DoorBonnet = "DoorBonnet";
	/** The Wiper status */
	private final String WiperStatus = "WiperStatus";

	// @formatter:off
	// Vehicle Off-Road Data Keys ***************************************************************************************
	// @formatter:on

	/** The vehicle incline indication */
	private final String WadeInclineDisplay = "WadeInclineDisplay";
	/** The maximum wade depth of the vehicle */
	private final String WadeMaxDepth = "WadeMaxDepth";
	/** The maximum wade speed of the vehicle */
	private final String WadeMaxSpeed = "WadeMaxSpeed";
	/** The current wade status */
	private final String WadeMessageReq = "WadeMessageReq";
	/** The current deepest depth of water around the vehicle */
	private final String WadeWaterDepth = "WadeWaterDepth";
	/** The transfer box range signal */
	private final String CDiffRangeActual = "CDiffRangeActual";
	/** The Auto-Terrain response program is on/off */
	private final String AutoTRStatus = "AutoTRStatus";
	/** The Terrain mode selected by the user */
	private final String TerrainModeReq = "TerrainModeReq";
	/** The DriverRatingStatus currentValuesMap key . */
	private final String DriverRatingStatus = "DriverRatingStatus";

	// @formatter:off
	// Vehicle ADAS Data Keys ***************************************************************************************
	// @formatter:on

	/** Traffic sign recognised speed */
	private final String TSRSpdLimit = "TSRSpdLimit";
	/** Traffic sign recognised speed units */
	private final String TSRSpdLimitUnits = "TSRSpdLimitUnits";
	/** Blind spot monitor alert (left) */
	private final String BSMLeftAlert = "BSMLeftAlert";
	/** Blind spot monitor alert (right) */
	private final String BSMRightAlert = "BSMRightAlert";

	// @formatter:off
	// Trip Info Data Keys ***************************************************************************************
	// @formatter:on
	/** HUD requirement status */
	private final String HUDNaviControlReq = "HUDNaviControlReq";

	/** HUD current count down bar for navigation instruction */
	private final String HUDNaviCurCntDwnBarReq = "HUDNaviCurCntDwnBarReq";


	/**
	 * Array of signal names currently supported by CSI. New signals are to be
	 * added here
	 */
	// @formatter:off
	private final String signalNameList[] = {
			VehicleSpeed,
			PedalPos,
			SteeringWheelAngle,
			LateralAcceleration,

			PowerMode,
			BrakePressure,
			DistanceToEmpty,
			GearLeverPosition,
			LowFuelLevel,
			EPBStatus,

			DrivSeatBeltBcklState,
			BeltReminderSensor,
			BeltReminderSensorRL,
			BeltReminderSensorRM,
			BeltReminderSensorRR,
			BeltRmderSensorRow3L,
			BeltRmderSensorRow3R,

			TripCurrSpeedFdbScr,
			TripCurrThrottleFdbScr,
			TripCurrBrakeFdbScr,
			TripCurrFuelEconomy,
			TripCurrFdbSummary,
			TripCurrDistance,
			TripCurrECOStopTimer,
			TripCurrAvSpeed,
			FuelEconomyUnits,
			MHStopStartStatus,
			TransShiftInd,

			SuspensionHeightFL,
			SuspensionHeightFR,
			SuspensionHeightRL,
			SuspensionHeightRR,

			TSRSpdLimit,
			TSRSpdLimitUnits,
			BSMLeftAlert,
			BSMRightAlert,


			RainSenseStatus,
			HVACAutoBlowerStatus,
			RearSystemOnCmd,
			DoorDrivers,
			DoorPassenger,
			DoorRearLeft,
			DoorRearRight,
			DoorTailgate,
			DoorBonnet,
			WiperStatus,

			WadeInclineDisplay,
			WadeMaxDepth,
			WadeMaxSpeed,
			WadeMessageReq,
			WadeWaterDepth,
			CDiffRangeActual,
			AutoTRStatus,
			TerrainModeReq,
			DriverRatingStatus,

			HUDNaviControlReq,
			HUDNaviCurCntDwnBarReq

	};
	// @formatter:on

	// @formatter:off
	// CAN BUS,CAN ID,SIGNAL NAME,BLOCK NUM,BYTE SWAP,START BIT,NUM OF BITS,OFFSET,SCALING

	private final String csvData[] = {

			"2,0BA,AutoTRStatus,0,0,45,1,0,1",
			"2,0BA,TerrainModeReq,0,0,0,3,0,1",
			"2,28F,BeltReminderSensorRL,0,0,22,1,0,1",
			"2,28F,BeltReminderSensorRM,0,0,0,1,0,1",
			"2,28F,BeltReminderSensorRR,0,0,23,1,0,1",
			"2,28F,CDiffRangeActual,0,0,6,2,0,1",
			"1,3CA,BeltRmderSensorRow3L,0,0,45,1,0,1",
			"1,3CA,BeltRmderSensorRow3R,0,0,46,1,0,1",
			"2,216,BrakePressure,0,0,24,10,0,0.2",
			"2,216,PedalPos,0,0,48,8,0,0.5",
			"2,216,DoorDrivers,0,0,40,1,0,1",
			"2,216,DoorPassenger,0,0,41,1,0,1",
			"2,216,DoorRearLeft,0,0,42,1,0,1",
			"2,216,DoorRearRight,0,0,43,1,0,1",
			"2,216,DoorTailgate,0,0,44,1,0,1",
			"2,216,DoorBonnet,0,0,45,1,0,1",
			"2,427,DistanceToEmpty,0,0,24,10,0,1",
			"1,3B2,DrivSeatBeltBcklState,0,0,7,1,0,1",
			"2,06A,DriverRatingStatus,1,0,50,4,0,1",
			"2,06A,FuelEconomyUnits,1,0,48,2,0,1",
			"2,06A,TripCurrSpeedFdbScr,1,0,42,6,0,0.1",
			"2,06A,TripCurrThrottleFdbScr,1,0,26,6,0,0.1",
			"2,06A,TripCurrBrakeFdbScr,1,0,10,6,0,0.1",
			"2,06A,TripCurrFuelEconomy,1,0,0,10,0,0.1",
			"2,06A,TripCurrFdbSummary,2,0,0,7,0,1",
			"2,06A,TripCurrDistance,3,0,24,18,0,0.1",
			"2,06A,TripCurrECOStopTimer ,3,0,0,22,0,1",
			"2,06A,TripCurrAvSpeed,4,0,24,9,0,1",
			"2,07A,BSMLeftAlert,0,0,7,1,0,1",
			"2,07B,BSMRightAlert,0,0,6,1,0,1",
			"2,04A,EPBStatus,0,0,24,2,0,1",
			"2,04A,GearLeverPosition,0,0,29,3,0,1",
			"2,04A,SteeringWheelAngle,0,1,40,16,-780,0.1",
			"2,4A0,LowFuelLevel,0,0,7,1,0,1",
			"2,120,TerrainModeReqExt,0,0,1,4,0,1",
			"2,11A,WadeInclineDisplay,0,0,11,2,0,1",
			"2,11A,WiperStatus,0,0,8,2,0,1",
			"2,28A,WadeMaxDepth,0,0,16,6,0,0.025",
			"2,28A,WadeMaxSpeed,0,0,56,5,0,1",
			"2,28A,WadeMessageReq,0,0,48,4,0,1",
			"2,28A,WadeWaterDepth,0,0,40,6,0,0.025",
			"2,040,VehicleSpeed,0,1,32,16,0,0.01",
			"2,040,LateralAcceleration,0,0,16,8,-11,0.1",
			"2,040,PowerMode,0,0,42,5,0,1",
			"2,32F,SuspensionHeightFL,0,0,8,9,-200,1",
			"2,32F,SuspensionHeightFR,0,0,56,9,-200,1",
			"2,32F,SuspensionHeightRL,0,0,40,9,-200,1",
			"2,32F,SuspensionHeightRR,0,0,24,9,-200,1",
			"2,32F,TSRSpdLimit,0,0,17,5,0,1",
			"2,32F,TSRSpdLimitUnits,0,0,38,2,0,1",
			"2,13D,HVACAutoBlowerStatus,0,0,6,2,0,1",
			"2,13D,RearSystemOnCmd,0,0,46,1,0,1",
			"2,17B,HUDNaviControlReq,0,0,54,2,0,1",
			"2,17B,HUDNaviCurCntDwnBarReq,0,0,50,4,0,1",
			"2,125,TransShiftInd,0,0,52,2,0,1"
			// CAN BUS,CAN ID,SIGNAL NAME,BLOCK NUM,BYTE SWAP,START BIT,NUM OF BITS,OFFSET,SCALING

	};
	// @formatter:on

	// CAN Bus IDs

	/** The Body Systems CAN bus. */
	private final String CAN_BUS_BO = "0";

	/** The Chasis Systems CAN bus. */
	private final String CAN_BUS_CH = "1";

	/** The Comfort Systems CAN bus. */
	private final String CAN_BUS_CO = "2";

	/** The Powertrain Systems CAN bus. */
	private final String CAN_BUS_PT = "3";

	/** The location. */
	private final String LOCATION = "Location";

	// CAN Frame ID

	/**
	 * Instantiates a new CAN server sdk.
	 */

	/**
	 * Instantiates a new CAN server sdk.
	 */
	private CANServerSDK() {
		buildAsyncMap();
		initSignalValueMap();

		Pair<String, String> currentBusCanPair = new Pair<String, String>("",
				"");
		ArrayList<Signal> signalList = new ArrayList<Signal>();
		// Read data line by line
		for (final String line : csvData) {

			// @formatter:off
			/*
			 * The following code builds the canSignalMap which is a hash map,
			 * where the 'key' is comprised of a pair consisting of Bus ID an
			 * CAN ID. For each key, the 'value' is an array list of 'Signal'
			 * objects, each of which detail information on how to decode the
			 * CAN signal, described by the signal object.
			 * 
			 * In the csvData string table above, all CAN signals sharing the
			 * same CAN bus and CAN ID will have Signal object created for them
			 * and stored in the canSignalMap hash map.
			 * 
			 * For Example;
			 * 
			 * 	"2,0BA,AutoTRStatus,0,0,45,1,0,1"
			 *  "2,0BA,TerrainModeReq,0,0,0,3,0,1"
			 * 
			 *  Will result canSignalMap entry where the key is <2,0BA>
			 *  and value will be two signal objects;
			 * 
			 *  Signal  object 1              Signal Object 2
			 *  Name: AutoTRStatus            Name: TerrainModeReq
			 *  BlockNumber: 0                BlockNumber: 0
			 *  ByteSwap:0                    ByteSwap:0
			 *  StartBit:45                   StartBit:0
			 *  Length: 1                     Length: 3
			 *  Offset: 0                     Offset: 0
			 *  Scale: 1                      Scale: 1
			 * 
			 *  Note:
			 *  All common combinations of CAN Bus and CAN ID must follow each other.
			 *  The following cannot be performed in the csvData string table;
			 * 
			 * 	"2,0BA,AutoTRStatus,0,0,45,1,0,1"
			 *  "2,28F,BeltReminderSensorRL,0,0,22,1,0,1",
			 *  "2,0BA,TerrainModeReq,0,0,0,3,0,1"
			 * 
			 */
			// @formatter:on

			final String[] nextLine = line.split(",");
			Log.d(TAG, "CSV = " + nextLine[BUS_ID_INDEX]
					+ nextLine[CAN_ID_INDEX] + nextLine[SIG_NAME_INDEX]
					+ nextLine[BLOCK_NUM_INDEX] + nextLine[BYTE_SWAP_INDEX]
					+ nextLine[START_BIT_INDEX] + nextLine[NUM_BIT_INDEX]
					+ nextLine[OFFSET_INDEX] + nextLine[SCALING_INDEX]);

			final Pair<String, String> pair = new Pair<String, String>(
					nextLine[BUS_ID_INDEX], nextLine[CAN_ID_INDEX]);
			if (currentBusCanPair.equals(pair) == false) {
				if (currentBusCanPair.equals(new Pair<String, String>("", "")) == false) {
					canSignalMap.put(currentBusCanPair, signalList);
					signalList = new ArrayList<Signal>();
				}
			}
			final Signal signal = new Signal(nextLine[SIG_NAME_INDEX],
					Integer.parseInt(nextLine[BLOCK_NUM_INDEX]),
					Integer.parseInt(nextLine[BYTE_SWAP_INDEX]),
					Integer.parseInt(nextLine[START_BIT_INDEX]),
					Integer.parseInt(nextLine[NUM_BIT_INDEX]),
					Integer.parseInt(nextLine[OFFSET_INDEX]),
					Float.parseFloat(nextLine[SCALING_INDEX]));
			signalList.add(signal);

			currentBusCanPair = pair;
		}
		canSignalMap.put(currentBusCanPair, signalList);

		// printCANSignalMap();
	}

	/**
	 * Prints the can signal map.
	 */
	public static void printCANSignalMap() {
		Log.d(TAG,
				"---------------------------------- BUS CAN MAP ----------------------------------------------");
		for (final Map.Entry<Pair<String, String>, ArrayList<Signal>> entry : canSignalMap
				.entrySet()) {
			final Pair<String, String> key = entry.getKey();
			final ArrayList<Signal> value = entry.getValue();
			for (final Signal signal : value) {
				Log.d(TAG, "Key: " + key.first + "," + key.second
						+ " : Value: " + signal.toString());
			}
		}
		Log.d(TAG,
				"---------------------------------- BUS CAN MAP ----------------------------------------------");
	}

	/**
	 * Prints the current values map.
	 */
	private static void printCurrentValuesMap() {
		Log.d(TAG,
				"---------------------------------- CURRENT VALUE MAP ----------------------------------------------");
		for (final Map.Entry<String, Double> entry : currentValuesMap
				.entrySet()) {
			final String key = entry.getKey();
			final double value = entry.getValue();

			Log.d(TAG, "Key: " + key + ", Value: " + value);

		}
		Log.d(TAG,
				"---------------------------------- CURRENT VALUE MAP ----------------------------------------------");

	}

	/**
	 * Shared instance.
	 * 
	 * @return the CAN server sdk
	 */
	public static CANServerSDK sharedInstance() {
		if (CANServerSDKInstance != null) {
			return CANServerSDKInstance;
		} else {
			CANServerSDKInstance = new CANServerSDK();
			return CANServerSDKInstance;
		}
	}

	// ************************************************************************************************************************
	// Asynchronous Callback Map
	// ************************************************************************************************************************

	/**
	 * Builds a hash map of Runnable objects, indexable on signal name. The
	 * signal name will be used to identify a runnable method, that will invoke
	 * the necessary listener and pass the updated signal value.
	 */
	private void buildAsyncMap() {

		// @formatter:off
		// ========================================================================================================================
		// Runnables for IHighFrequencyDataListener interface
		// ========================================================================================================================
		// @formatter:on

		// Vehicle Speed
		final Runnable vehicleSpeed_Runnable = new Runnable() {
			@Override
			public void run() {

				if (highFreqDataListenerList.isEmpty() == false) {

					Double sigValue;
					final Double value = currentValuesMap.get(VehicleSpeed);
					if (value >= 0 && value <= 320) {
						sigValue = value;
					} else {
						sigValue = DBL_ERROR;
					}

					for (final IHighFrequencyDataListener listener : highFreqDataListenerList) {
						listener.onVehicleSpeedChanged(sigValue);
					}
				}
			}
		};

		// Pedal/Throttle position
		final Runnable PedalPos_Runnable = new Runnable() {
			@Override
			public void run() {

				if (highFreqDataListenerList.isEmpty() == false) {

					Double value = currentValuesMap.get(PedalPos);
					if (value < 0 || value > 108.5) {
						value = DBL_ERROR;
					}

					for (final IHighFrequencyDataListener listener : highFreqDataListenerList) {
						listener.onPedalPositionChanged(value);
					}
				}
			}
		};

		// Steering Wheel Angle
		final Runnable SteeringWheelAngle_Runnable = new Runnable() {
			@Override
			public void run() {

				if (highFreqDataListenerList.isEmpty() == false) {

					Integer sigValue;
					final Double value = currentValuesMap
							.get(SteeringWheelAngle);
					if (value >= -780 && value <= 780) {
						sigValue = value.intValue();
					} else {
						sigValue = INT_ERROR;
					}

					for (final IHighFrequencyDataListener listener : highFreqDataListenerList) {
						listener.onSteeringWheelAngleChanged(sigValue);
					}
				}
			}
		};

		// Lateral Acceleration
		final Runnable lateralAcceleration_Runnable = new Runnable() {
			@Override
			public void run() {

				if (lowFreqListenerList.isEmpty() == false) {

					Double sigValue;
					final Double value = currentValuesMap
							.get(LateralAcceleration);
					if (value >= -11 && value <= 11) {
						sigValue = value;
					} else {
						sigValue = DBL_ERROR;
					}

					for (final IHighFrequencyDataListener listener : highFreqDataListenerList) {
						listener.onLateralAccelerationChanged(sigValue);
					}
				}
			}
		};

		// @formatter:off
		// ========================================================================================================================
		// Runnables for ILowFrequencyDataListener interface
		// ========================================================================================================================
		// @formatter:on

		// Power Mode
		final Runnable powerMode_Runnable = new Runnable() {
			@Override
			public void run() {

				if (lowFreqListenerList.isEmpty() == false) {

					Integer sigValue;
					final Double value = currentValuesMap.get(PowerMode);
					if (value >= 0 && value <= 9) {
						sigValue = value.intValue();
					} else {
						sigValue = INT_ERROR;
					}

					for (final ILowFrequencyDataListener listener : lowFreqListenerList) {
						listener.onPowerModeChanged(sigValue.intValue());
					}
				}
			}
		};

		// Brake Pressure
		final Runnable BrakePressure_Runnable = new Runnable() {
			@Override
			public void run() {

				if (lowFreqListenerList.isEmpty() == false) {

					Double value = currentValuesMap.get(BrakePressure);
					if (value >= 0 && value <= 205) {

					} else {
						value = DBL_ERROR;
					}

					for (final ILowFrequencyDataListener listener : lowFreqListenerList) {
						listener.onBrakePressureChanged(value);
					}
				}
			}
		};

		// Distance To Empty
		final Runnable DistanceToEmpty_Runnable = new Runnable() {
			@Override
			public void run() {

				if (lowFreqListenerList.isEmpty() == false) {

					Integer sigValue;
					final Double value = currentValuesMap.get(DistanceToEmpty);
					if (value >= 0 && value <= 1022) {
						sigValue = value.intValue();
					} else {
						sigValue = INT_ERROR;
					}

					for (final ILowFrequencyDataListener listener : lowFreqListenerList) {
						listener.onDistanceToEmptyChanged(sigValue);
					}
				}
			}
		};

		// Gear Lever Position
		final Runnable GearLeverPosition_Runnable = new Runnable() {
			@Override
			public void run() {

				if (lowFreqListenerList.isEmpty() == false) {

					Integer sigValue;
					final Double value = currentValuesMap
							.get(GearLeverPosition);
					if (value >= 0 && value <= 7) {
						sigValue = value.intValue();
					} else {
						sigValue = INT_ERROR;
					}

					for (final ILowFrequencyDataListener listener : lowFreqListenerList) {
						listener.onGearLeverChanged(sigValue);
					}
				}
			}
		};

		// Low fuel level
		final Runnable LowFuelLevel_Runnable = new Runnable() {
			@Override
			public void run() {

				if (lowFreqListenerList.isEmpty() == false) {

					Integer sigValue;
					final Double value = currentValuesMap.get(LowFuelLevel);
					if (value == 0 || value == 1) {
						sigValue = value.intValue();
					} else {
						sigValue = INT_ERROR;
					}

					for (final ILowFrequencyDataListener listener : lowFreqListenerList) {
						listener.onLowFuelLevelChanged(sigValue);
					}
				}
			}
		};

		// Electronic Park Brake Status
		final Runnable EPBStatus_Runnable = new Runnable() {
			@Override
			public void run() {

				if (lowFreqListenerList.isEmpty() == false) {

					Integer sigValue;
					final Double value = currentValuesMap.get(EPBStatus);
					if (value == 0 || value == 1) {
						sigValue = value.intValue();
					} else {
						sigValue = INT_ERROR;
					}

					for (final ILowFrequencyDataListener listener : lowFreqListenerList) {
						listener.onParkBrakeChanged(sigValue);
					}
				}
			}
		};

		// @formatter:off
		// ========================================================================================================================
		// Runnables for IEcoCurrentTripListener interface
		// ========================================================================================================================
		// @formatter:on

		// Current Trip Speed Feedback Score
		final Runnable TripCurrSpeedFdbScr_Runnable = new Runnable() {
			@Override
			public void run() {

				if (ecoCurrentTripListenerList.isEmpty() == false) {

					Double sigValue;
					final Double value = currentValuesMap
							.get(TripCurrSpeedFdbScr);
					if (value >= 0 && value <= 5.0) {
						sigValue = value;
					} else {
						sigValue = DBL_ERROR;
					}

					for (final IEcoCurrentTripListener listener : ecoCurrentTripListenerList) {
						listener.onCurrentSpeedScorechanged(sigValue);
					}
				}
			}
		};

		// Current Trip Throttle Feedback Score
		final Runnable TripCurrThrottleFdbScr_Runnable = new Runnable() {
			@Override
			public void run() {

				if (ecoCurrentTripListenerList.isEmpty() == false) {

					Double sigValue;
					final Double value = currentValuesMap
							.get(TripCurrThrottleFdbScr);
					if (value >= 0 && value <= 5.0) {
						sigValue = value;
					} else {
						sigValue = DBL_ERROR;
					}

					for (final IEcoCurrentTripListener listener : ecoCurrentTripListenerList) {
						listener.onCurrentThrottleScoreChanged(sigValue);
					}
				}
			}
		};

		// Current Trip Brake Feedback Score
		final Runnable TripCurrBrakeFdbScr_Runnable = new Runnable() {
			@Override
			public void run() {

				if (ecoCurrentTripListenerList.isEmpty() == false) {

					Double sigValue;
					final Double value = currentValuesMap
							.get(TripCurrBrakeFdbScr);
					if (value >= 0 && value <= 5.0) {
						sigValue = value;
					} else {
						sigValue = DBL_ERROR;
					}

					for (final IEcoCurrentTripListener listener : ecoCurrentTripListenerList) {
						listener.onCurrentBrakeScoreChanged(sigValue);
					}
				}
			}
		};

		// Current Trip Fuel Economy Feedback Score
		final Runnable TripCurrEconomyFdbScr_Runnable = new Runnable() {
			@Override
			public void run() {

				if (ecoCurrentTripListenerList.isEmpty() == false) {

					Double sigValue;
					final Double value = currentValuesMap
							.get(TripCurrFuelEconomy);
					if (value >= 0 && value <= 99) {
						sigValue = value;
					} else {
						sigValue = DBL_ERROR;
					}

					for (final IEcoCurrentTripListener listener : ecoCurrentTripListenerList) {
						listener.onCurrentFuelEconomyChanged(sigValue);
					}
				}
			}
		};

		// Current Trip Feedback Score
		final Runnable TripCurrFdbScr_Runnable = new Runnable() {
			@Override
			public void run() {

				if (ecoCurrentTripListenerList.isEmpty() == false) {

					Integer sigValue;
					final Double value = currentValuesMap
							.get(TripCurrFdbSummary);
					if (value >= 0 && value <= 100) {
						sigValue = value.intValue();
					} else {
						sigValue = INT_ERROR;
					}

					for (final IEcoCurrentTripListener listener : ecoCurrentTripListenerList) {
						listener.onCurrentTripScoreChanged(sigValue);
					}
				}
			}
		};

		// Current Trip Distance Changed
		final Runnable TripCurrDistance_Runnable = new Runnable() {
			@Override
			public void run() {

				if (ecoCurrentTripListenerList.isEmpty() == false) {

					Double value = currentValuesMap.get(TripCurrDistance);
					if (value < 0 || value > 1677721) {
						value = DBL_ERROR;
					}

					for (final IEcoCurrentTripListener listener : ecoCurrentTripListenerList) {
						listener.onCurrentTripDistanceChanged(value);
					}
				}
			}
		};

		// Current Trip Eco Stop/Start Timer
		final Runnable TripCurrEcoStopTimer_Runnable = new Runnable() {
			@Override
			public void run() {

				if (ecoCurrentTripListenerList.isEmpty() == false) {
					for (final IEcoCurrentTripListener listener : ecoCurrentTripListenerList) {
						listener.onCurrentEcoStopTimerChanged(currentValuesMap
								.get(TripCurrECOStopTimer));
					}
				}
			}
		};

		// Current Trip Average Speed
		final Runnable TripCurrAverageSpeed_Runnable = new Runnable() {
			@Override
			public void run() {

				if (ecoCurrentTripListenerList.isEmpty() == false) {

					Integer sigValue;
					final Double value = currentValuesMap.get(TripCurrAvSpeed);
					if (value >= 0 && value <= 509) {
						sigValue = value.intValue();
					} else {
						sigValue = INT_ERROR;
					}

					Log.d(TAG, "Decoded Average Speed: " + value);

					for (final IEcoCurrentTripListener listener : ecoCurrentTripListenerList) {
						listener.onCurrentTripAverageSpeedChanged(sigValue);
					}
				}
			}
		};

		// Fuel Economy Units
		final Runnable FuelEconomyUnits_Runnable = new Runnable() {
			@Override
			public void run() {

				if (ecoCurrentTripListenerList.isEmpty() == false) {

					Integer sigValue;
					final Double value = currentValuesMap.get(FuelEconomyUnits);
					if (value >= 0 && value <= 5) {
						sigValue = value.intValue();
					} else {
						sigValue = INT_ERROR;
					}

					for (final IEcoCurrentTripListener listener : ecoCurrentTripListenerList) {
						listener.onFuelEconomyUnitsChanged(sigValue);
					}
				}
			}
		};

		// Stop Start Status
		final Runnable MHStopStartStatus_Runnable = new Runnable() {
			@Override
			public void run() {

				if (ecoCurrentTripListenerList.isEmpty() == false) {

					Integer sigValue;
					final Double value = currentValuesMap
							.get(MHStopStartStatus);
					if (value >= 0 && value <= 15) {
						sigValue = value.intValue();
					} else {
						sigValue = INT_ERROR;
					}

					for (final IEcoCurrentTripListener listener : ecoCurrentTripListenerList) {
						listener.onStopStartStatusChanged(sigValue);
					}
				}
			}
		};



		// Transmission Upshift indicator
		final Runnable TransShiftInd_Runnable = new Runnable() {
			@Override
			public void run() {



				if (ecoCurrentTripListenerList.isEmpty() == false) {

					Integer sigValue;
					final Double value = currentValuesMap.get(TransShiftInd);
					if (value >= 0 && value <= 3) {
						sigValue = value.intValue();
					} else {
						sigValue = INT_ERROR;
					}

					Log.d("jj","TransShiftInd_Runnable: sigValue: " + Double.toString(sigValue));


					for (final IEcoCurrentTripListener listener : ecoCurrentTripListenerList) {
						listener.onTransShiftIndChanged(sigValue);
					}
				}
				else {
					Log.d("jj","TransShiftInd_Runnable: ecoCurrentTripListenerList.isEmpty()");
				}
			}
		};


		// @formatter:off
		// ========================================================================================================================
		// Runnables for IVehicleEquipmentListener interface
		// ========================================================================================================================
		// @formatter:on

		// Rain Wiper Sense Status
		final Runnable RainSenseStatus_Runnable = new Runnable() {
			@Override
			public void run() {

				if (equipmentListenerList.isEmpty() == false) {

					Integer sigValue;
					final Double value = currentValuesMap.get(RainSenseStatus);
					if (value == 0 || value == 1) {
						sigValue = value.intValue();
					} else {
						sigValue = INT_ERROR;
					}

					for (final IVehicleEquipmentListener listener : equipmentListenerList) {
						listener.onRainWiperStatusChanged(sigValue);
					}
				}
			}
		};

		// HVAC Blower Status
		final Runnable HVACAutoBlowerStatus_Runnable = new Runnable() {
			@Override
			public void run() {

				if (equipmentListenerList.isEmpty() == false) {

					Integer sigValue;
					final Double value = currentValuesMap
							.get(HVACAutoBlowerStatus);
					if (value >= 0 && value <= 3) {
						sigValue = value.intValue();
					} else {
						sigValue = INT_ERROR;
					}

					for (final IVehicleEquipmentListener listener : equipmentListenerList) {
						listener.onHVACBlowerStatusChanged(sigValue);
					}
				}
			}
		};

		// Rear Climate Status
		final Runnable RearSystemOnCmd_Runnable = new Runnable() {
			@Override
			public void run() {

				if (equipmentListenerList.isEmpty() == false) {

					Integer sigValue;
					final Double value = currentValuesMap.get(RearSystemOnCmd);
					if (value == 0 || value == 1) {
						sigValue = value.intValue();
					} else {
						sigValue = INT_ERROR;
					}

					for (final IVehicleEquipmentListener listener : equipmentListenerList) {
						listener.onRearClimateStatusChanged(sigValue);
					}
				}
			}
		};

		final Runnable WiperStatus_Runnable = new Runnable() {
			@Override
			public void run() {

				if (equipmentListenerList.isEmpty() == false) {

					Integer sigValue;
					final Double value = currentValuesMap.get(WiperStatus);
					if (value >= 0 && value <= 3) {
						sigValue = value.intValue();
					} else {
						sigValue = INT_ERROR;
					}

					for (final IVehicleEquipmentListener listener : equipmentListenerList) {
						listener.onWiperStatusChanged(sigValue);
					}
				}
			}
		};

		// @formatter:off
		// ========================================================================================================================
		// Runnables for IOffRoadStatusListener interface
		// ========================================================================================================================
		// @formatter:on

		// Wade Incline Display
		final Runnable WadeInclineDisplay_Runnable = new Runnable() {
			@Override
			public void run() {

				if (offRoadListenerList.isEmpty() == false) {

					Integer sigValue;
					final Double value = currentValuesMap
							.get(WadeInclineDisplay);
					if (value >= 0 && value <= 3) {
						sigValue = value.intValue();
					} else {
						sigValue = INT_ERROR;
					}

					for (final IOffRoadStatusListener listener : offRoadListenerList) {
						listener.onInclineStatusChanged(sigValue);
					}
				}
			}
		};

		// Current Wade Status
		final Runnable WadeMessageReq_Runnable = new Runnable() {
			@Override
			public void run() {

				if (offRoadListenerList.isEmpty() == false) {

					Integer sigValue;
					final Double value = currentValuesMap.get(WadeMessageReq);
					if (value >= 0 && value <= 15) {
						sigValue = value.intValue();
					} else {
						sigValue = INT_ERROR;
					}

					for (final IOffRoadStatusListener listener : offRoadListenerList) {
						listener.onWadeStatusChanged(sigValue);
					}
				}
			}
		};

		// Wade Water Depth
		final Runnable WadeWaterDepth_Runnable = new Runnable() {
			@Override
			public void run() {

				if (offRoadListenerList.isEmpty() == false) {

					Double value = currentValuesMap.get(WadeWaterDepth);
					if (value >= 0 && value <= 1.5) {

					} else {
						value = DBL_ERROR;
					}

					for (final IOffRoadStatusListener listener : offRoadListenerList) {
						listener.onWadeDepthChanged(value);
					}
				}
			}
		};

		// Diff Range Actual
		final Runnable CDiffRangeActual_Runnable = new Runnable() {
			@Override
			public void run() {

				if (offRoadListenerList.isEmpty() == false) {

					Integer sigValue;
					final Double value = currentValuesMap.get(CDiffRangeActual);
					if (value >= 0 && value <= 3) {
						sigValue = value.intValue();
					} else {
						sigValue = INT_ERROR;
					}

					for (final IOffRoadStatusListener listener : offRoadListenerList) {
						listener.onDiffRangeActualChanged(sigValue);
					}
				}
			}
		};

		// Auto Terrain Program On/Off
		final Runnable AutoTRStatus_Runnable = new Runnable() {
			@Override
			public void run() {

				if (offRoadListenerList.isEmpty() == false) {

					Integer sigValue;
					final Double value = currentValuesMap.get(AutoTRStatus);
					if (value == 0 || value == 1) {
						sigValue = value.intValue();
					} else {
						sigValue = INT_ERROR;
					}

					for (final IOffRoadStatusListener listener : offRoadListenerList) {
						listener.onAutoTerrainProgramChanged(sigValue);
					}
				}
			}
		};

		// Driver selected Terrain Mode
		final Runnable TerrainModeReq_Runnable = new Runnable() {
			@Override
			public void run() {

				if (offRoadListenerList.isEmpty() == false) {

					Integer sigValue;
					final Double value = currentValuesMap.get(TerrainModeReq);
					if (value >= 0 && value <= 7) {
						sigValue = value.intValue();
					} else {
						sigValue = INT_ERROR;
					}

					for (final IOffRoadStatusListener listener : offRoadListenerList) {
						listener.onManualTerrainProgramChanged(sigValue);
					}
				}
			}
		};

		// Driver Rating Status
		final Runnable DriverRatingStatus_Runnable = new Runnable() {
			@Override
			public void run() {

				if (offRoadListenerList.isEmpty() == false) {

					Integer sigValue;
					final Double value = currentValuesMap
							.get(DriverRatingStatus);
					if (value >= 0 && value <= 10) {
						sigValue = value.intValue();
					} else {
						sigValue = INT_ERROR;
					}

					for (final IOffRoadStatusListener listener : offRoadListenerList) {
						listener.onDriverRatingStatusChanged(sigValue);
					}
				}
			}
		};


		// @formatter:off
		// ========================================================================================================================
		// Runnables for IADASStatusListener interface
		// ========================================================================================================================
		// @formatter:on


		// Traffic sign recognised speed
		final Runnable TSRSpdLimit_Runnable = new Runnable() {
			@Override
			public void run() {

				if (adasListenerList.isEmpty() == false) {

					Integer sigValue;
					final Double value = currentValuesMap.get(TSRSpdLimit);
					if (value >= 0 && value <= 31) {
						sigValue = value.intValue();
					} else {
						sigValue = INT_ERROR;
					}

					for (final IADASListener listener : adasListenerList) {
						listener.onTSRSpeedLimitChanged(sigValue);
					}
				}
			}
		};


		// Traffic sign recognised speed unit
		final Runnable TSRSpdLimitUnits_Runnable = new Runnable() {
			@Override
			public void run() {

				if (adasListenerList.isEmpty() == false) {

					Integer sigValue;
					final Double value = currentValuesMap.get(TSRSpdLimitUnits);
					if (value >= 0 && value <= 3) {
						sigValue = value.intValue();
					} else {
						sigValue = INT_ERROR;
					}

					for (final IADASListener listener : adasListenerList) {
						listener.onTSRSpeedLimitUnitsChanged(sigValue);
					}
				}
			}
		};


		// Blind spot monitor alert (left)
		final Runnable BSMLeftAlert_Runnable = new Runnable() {
			@Override
			public void run() {

				if (adasListenerList.isEmpty() == false) {

					Integer sigValue;
					final Double value = currentValuesMap.get(BSMLeftAlert);
					if (value >= 0 && value <= 1) {
						sigValue = value.intValue();
					} else {
						sigValue = INT_ERROR;
					}

					for (final IADASListener listener : adasListenerList) {
						listener.onBSMLeftAlertChanged(sigValue);
					}
				}
			}
		};

		// Blind spot monitor alert (right)
		final Runnable BSMRightAlert_Runnable = new Runnable() {
			@Override
			public void run() {

				if (adasListenerList.isEmpty() == false) {

					Integer sigValue;
					final Double value = currentValuesMap.get(BSMRightAlert);
					if (value >= 0 && value <= 1) {
						sigValue = value.intValue();
					} else {
						sigValue = INT_ERROR;
					}

					for (final IADASListener listener : adasListenerList) {
						listener.onBSMRightAlertChanged(sigValue);
					}
				}
			}
		};

		// HUD Navi Control Request status
		final Runnable HUDNaviControlReq_Runnable = new Runnable() {
			@Override
			public void run() {

				Log.d("jj","runnable HUDNaviControlReq_Runnable");


				if (tripInfoListenerList.isEmpty() == false) {

					Integer sigValue;
					final Double value = currentValuesMap.get(HUDNaviControlReq);
					if (value >= 0 && value <= 3) {
						sigValue = value.intValue();
					} else {
						sigValue = INT_ERROR;
					}

					Log.d("jj","runnable HUDNaviControlReq_Runnable: " + Integer.toString(sigValue));


					for (final ITripInfoListener listener : tripInfoListenerList) {
						listener.onHUDNaviControlReqChanged(sigValue);
					}
				}
			}
		};


		// HUD Navi Count Down Bar Request status
		final Runnable HUDNaviCurCntDwnBarReq_Runnable = new Runnable() {
			@Override
			public void run() {

				Log.d("jj","runnable HUDNaviCurCntDwnBarReq");

				if (tripInfoListenerList.isEmpty() == false) {

					Integer sigValue;
					final Double value = currentValuesMap.get(HUDNaviCurCntDwnBarReq);
					if (value >= 0 && value <= 15) {
						sigValue = value.intValue();
					} else {
						sigValue = INT_ERROR;
					}

					Log.d("jj","runnable HUDNaviCurCntDwnBarReq: " + Integer.toString(sigValue));


					for (final ITripInfoListener listener : tripInfoListenerList) {
						listener.onHUDNaviCurrentCountdownBarChanged(sigValue);
					}
				}
			}
		};





		// IHighFrequencyDataListener
		aSyncMap.put(VehicleSpeed, vehicleSpeed_Runnable);
		aSyncMap.put(PedalPos, PedalPos_Runnable);
		aSyncMap.put(SteeringWheelAngle, SteeringWheelAngle_Runnable);

		// ILowFrequencyDataListener
		aSyncMap.put(PowerMode, powerMode_Runnable);
		aSyncMap.put(FuelEconomyUnits, FuelEconomyUnits_Runnable);
		aSyncMap.put(LateralAcceleration, lateralAcceleration_Runnable);
		aSyncMap.put(BrakePressure, BrakePressure_Runnable);
		aSyncMap.put(DistanceToEmpty, DistanceToEmpty_Runnable);
		aSyncMap.put(GearLeverPosition, GearLeverPosition_Runnable);
		aSyncMap.put(LowFuelLevel, LowFuelLevel_Runnable);
		aSyncMap.put(EPBStatus, EPBStatus_Runnable);
		aSyncMap.put(MHStopStartStatus, MHStopStartStatus_Runnable);



		// ISeatBeltStatusListener
		// See SeatBeltStatus_Runnable(String) and handleSeatBeltStatus(String)

		// IEcoCurrentTripListener
		aSyncMap.put(TripCurrSpeedFdbScr, TripCurrSpeedFdbScr_Runnable);
		aSyncMap.put(TripCurrThrottleFdbScr, TripCurrThrottleFdbScr_Runnable);
		aSyncMap.put(TripCurrBrakeFdbScr, TripCurrBrakeFdbScr_Runnable);
		aSyncMap.put(TripCurrFuelEconomy, TripCurrEconomyFdbScr_Runnable);
		aSyncMap.put(TripCurrFdbSummary, TripCurrFdbScr_Runnable);
		aSyncMap.put(TripCurrDistance, TripCurrDistance_Runnable);
		aSyncMap.put(TripCurrECOStopTimer, TripCurrEcoStopTimer_Runnable);
		aSyncMap.put(TripCurrAvSpeed, TripCurrAverageSpeed_Runnable);
		aSyncMap.put(TransShiftInd, TransShiftInd_Runnable);


		// IVehicleEquipmentListener
		aSyncMap.put(RainSenseStatus, RainSenseStatus_Runnable);
		aSyncMap.put(HVACAutoBlowerStatus, HVACAutoBlowerStatus_Runnable);
		aSyncMap.put(RearSystemOnCmd, RearSystemOnCmd_Runnable);
		aSyncMap.put(WiperStatus, WiperStatus_Runnable);

		// IOffRoadStatusListener
		aSyncMap.put(WadeInclineDisplay, WadeInclineDisplay_Runnable);
		aSyncMap.put(WadeMessageReq, WadeMessageReq_Runnable);
		aSyncMap.put(WadeWaterDepth, WadeWaterDepth_Runnable);
		aSyncMap.put(CDiffRangeActual, CDiffRangeActual_Runnable);
		aSyncMap.put(AutoTRStatus, AutoTRStatus_Runnable);
		aSyncMap.put(TerrainModeReq, TerrainModeReq_Runnable);
		aSyncMap.put(DriverRatingStatus, DriverRatingStatus_Runnable);


		//IADASListener
		aSyncMap.put(TSRSpdLimit, TSRSpdLimit_Runnable);
		aSyncMap.put(TSRSpdLimitUnits, TSRSpdLimitUnits_Runnable);
		aSyncMap.put(BSMLeftAlert, BSMLeftAlert_Runnable);
		aSyncMap.put(BSMRightAlert, BSMRightAlert_Runnable);

		//ITripInfoListener
		aSyncMap.put(HUDNaviControlReq, HUDNaviControlReq_Runnable);
		aSyncMap.put(HUDNaviCurCntDwnBarReq,HUDNaviCurCntDwnBarReq_Runnable);
	}

	/**
	 * This method will satisfy the onSeatBeltStatusChanged() method of the
	 * ISeatBeltStatusListener interface.
	 * 
	 * @param seatBeltCanSignalName
	 *            The CAN signal name of the updated seat belt.
	 */
	private void handleSeatBeltStatus(final String seatBeltCanSignalName) {

		if (seatBeltListenerList.isEmpty() == false) {

			SeatBeltID seatBeltID = null;
			SeatBeltStatus beltStatus = null;

			// determine CAN signal value
			final Double value = currentValuesMap.get(seatBeltCanSignalName);
			if (value == 0 || value == 1) {
				if (value == 0) {
					beltStatus = SeatBeltStatus.UNBUCKLED;
				} else {
					beltStatus = SeatBeltStatus.BUCKLED;
				}
			} else {
				beltStatus = SeatBeltStatus.ERROR;
			}

			// resolve the seat belt ID from signal name
			if (seatBeltCanSignalName.equalsIgnoreCase(DrivSeatBeltBcklState)) {
				seatBeltID = SeatBeltID.DRIVERS_SEAT;
			} else if (seatBeltCanSignalName
					.equalsIgnoreCase(BeltReminderSensor)) {
				seatBeltID = SeatBeltID.FRONT_PASSENGER;
			} else if (seatBeltCanSignalName
					.equalsIgnoreCase(BeltReminderSensorRL)) {
				seatBeltID = SeatBeltID.REAR_LEFT_PASSENGER;
			} else if (seatBeltCanSignalName
					.equalsIgnoreCase(BeltReminderSensorRM)) {
				seatBeltID = SeatBeltID.REAR_MIDDLE_PASSENGER;
			} else if (seatBeltCanSignalName
					.equalsIgnoreCase(BeltReminderSensorRR)) {
				seatBeltID = SeatBeltID.REAR_RIGHT_PASSENGER;
			} else if (seatBeltCanSignalName
					.equalsIgnoreCase(BeltRmderSensorRow3L)) {
				seatBeltID = SeatBeltID.THIRD_ROW_LEFT_PASSENGER;
			} else if (seatBeltCanSignalName
					.equalsIgnoreCase(BeltRmderSensorRow3R)) {
				seatBeltID = SeatBeltID.THIRD_ROW_RIGHT_PASSENGER;
			}

			// inform registered subscribers
			for (final ISeatBeltStatusListener listener : seatBeltListenerList) {
				listener.onSeatBeltStatusChanged(seatBeltID, beltStatus);
			}
		}
	}

	/**
	 * This method will satisfy the onSuspensionChanged() method of the
	 * IVehicleSuspensionListener interface.
	 * 
	 * @param seatBeltCanSignalName
	 *            The CAN signal name of the updated seat belt.
	 */
	private void handleSuspension(final String suspensionCANSignalName) {

		if (suspensionListenerList.isEmpty() == false) {

			SuspensionID suspID = null;
			int suspValue = 0;

			// resolve signals value
			final Double value = currentValuesMap.get(suspensionCANSignalName);
			if (value >= -200 && value <= 300) {
				suspValue = value.intValue();
			} else {
				suspValue = INT_ERROR;
			}

			// resolve the suspension ID
			if (suspensionCANSignalName.equalsIgnoreCase(SuspensionHeightFL)) {
				suspID = SuspensionID.FRONT_LEFT;
			} else if (suspensionCANSignalName
					.equalsIgnoreCase(SuspensionHeightFR)) {
				suspID = SuspensionID.FRONT_RIGHT;
			} else if (suspensionCANSignalName
					.equalsIgnoreCase(SuspensionHeightRL)) {
				suspID = SuspensionID.REAR_LEFT;
			} else if (suspensionCANSignalName
					.equalsIgnoreCase(SuspensionHeightRR)) {
				suspID = SuspensionID.REAR_RIGHT;

			}
			// inform registered subscribers
			for (final IVehicleSuspensionListener listener : suspensionListenerList) {
				listener.onSuspensionChanged(suspID, suspValue);
			}
		}
	}

	/**
	 * This method will satisfy the onDoorStatusChanged() method of the
	 * IVehicleEquipmentListener interface.
	 * 
	 * @param seatBeltCanSignalName
	 *            The CAN signal name of the updated seat belt.
	 */
	private void handleDoorStatus(final String suspensionCANSignalName) {

		if (equipmentListenerList.isEmpty() == false) {

			DoorID doorID = null;
			DoorStatus doorStatus = null;

			// resolve signals value
			final Double value = currentValuesMap.get(suspensionCANSignalName);
			if (value == 0 || value == 1) {
				if (value == 0) {
					doorStatus = CANServerSDK.DoorStatus.OPEN;
				} else {
					doorStatus = CANServerSDK.DoorStatus.CLOSED;
				}
			} else {
				doorStatus = CANServerSDK.DoorStatus.ERROR;
			}

			// resolve the door status
			if (suspensionCANSignalName.equalsIgnoreCase(DoorDrivers)) {
				doorID = DoorID.DRIVERS_DOOR;
			} else if (suspensionCANSignalName.equalsIgnoreCase(DoorPassenger)) {
				doorID = DoorID.FRONT_PASSENGER_DOOR;
			} else if (suspensionCANSignalName.equalsIgnoreCase(DoorRearLeft)) {
				doorID = DoorID.REAR_LEFT_DOOR;
			} else if (suspensionCANSignalName.equalsIgnoreCase(DoorRearRight)) {
				doorID = DoorID.REAR_RIGHT_DOOR;
			} else if (suspensionCANSignalName.equalsIgnoreCase(DoorTailgate)) {
				doorID = DoorID.TAILGATE;
			} else if (suspensionCANSignalName.equalsIgnoreCase(DoorBonnet)) {
				doorID = DoorID.BONNET;
			}

			// inform registered subscribers
			for (final IVehicleEquipmentListener listener : equipmentListenerList) {
				listener.onDoorStatusChanged(doorID, doorStatus);
			}
		}

	}

	// @formatter:off
	// ========================================================================================================================
	// Runnables for ISeatBeltStatusListener & IVehicleSuspensionListener interface
	// ========================================================================================================================
	// @formatter:on

	/**
	 * We are wrapping a runnable in a method, so that an argument can be passed
	 * to it. The argument is the CAN signal name
	 * 
	 * @param theSignalName
	 *            The CAN signal name of the updated CAN signal
	 */
	private Runnable Super_Runnable(final String theSignalName) {

		final Runnable aRunnable = new Runnable() {
			@Override
			public void run() {
				if (theSignalName.contains("Belt")) {
					handleSeatBeltStatus(theSignalName);
				} else if (theSignalName.contains("Susp")) {
					handleSuspension(theSignalName);
				} else if (theSignalName.contains("Door")) {
					handleDoorStatus(theSignalName);
				}
			}
		};

		return aRunnable;
	}

	/**
	 * Initializes currentValuesMap for each CAN signal that can be returned.
	 */
	private void initSignalValueMap() {

		for (final String signal : signalNameList) {
			currentValuesMap.put(signal, DBL_ERROR);
		}
	}

	// ************************************************************************************************************************
	// Synchronous Methods
	// ************************************************************************************************************************

	// ========================================================================================================================
	// Synchronous Methods: High Frequency Data
	// ========================================================================================================================

	// @formatter:off
	/**
	 * Get the current vehicle speed.
	 * 
	 * @return Vehicle Speed Valid range: 0 - 320 Km/h
	 * 
	 * <ul>
	 *     <li>Error&nbsp&nbsp	{@link CANServerSDK#DBL_ERROR}</li>
	 * </ul>
	 * 
	 */
	// @formatter:on
	public double getVehicleSpeed() {

		final Double value = currentValuesMap.get(VehicleSpeed);
		if (value >= 0 && value <= 320) {
			return value;
		} else {
			return DBL_ERROR;
		}
	}

	// @formatter:off
	/**
	 * Get the current throttle pedal position.
	 * 
	 * @return Throttle pedal position. Valid range: 0 - 108.5 %
	 * 
	 * <ul>
	 *     <li>Error&nbsp&nbsp	{@link CANServerSDK#DBL_ERROR}</li>
	 * </ul>
	 * 
	 */
	// @formatter:on
	public double getPedalPosition() {

		final Double value = currentValuesMap.get(PedalPos);
		if (value >= 0 && value <= 108.5) {
			return value;
		} else {
			return DBL_ERROR;
		}
	}

	// @formatter:off
	/**
	 * Get the current steering wheel angle.
	 * 
	 * @return Steering wheel angle. Valid range: +780 degrees to -780 degrees
	 * 
	 * <ul>
	 *     <li>Error&nbsp&nbsp	{@link CANServerSDK#INT_ERROR}</li>
	 * </ul>
	 * 
	 */
	// @formatter:on
	public int getSteeringWheelAngle() {

		final Double value = currentValuesMap.get(SteeringWheelAngle);
		if (value >= -780 && value <= 780) {
			return value.intValue();
		} else {
			return INT_ERROR;
		}
	}

	// @formatter:off
	/**
	 * Gets the current lateral acceleration.
	 * 
	 * @return The lateral acceleration. Valid range: (-11) - 11 m/s/s
	 *          <p>
	 * Error:&nbsp&nbsp	{@link CANServerSDK#DBL_ERROR}
	 * 
	 */
	// @formatter:on
	public double getLateralAcceleration() {

		final Double value = currentValuesMap.get(LateralAcceleration);
		if (value >= -11 && value <= 11) {
			return value;
		} else {
			return DBL_ERROR;
		}
	}

	// ========================================================================================================================
	// Synchronous Methods: Low Frequency Data
	// ========================================================================================================================

	// @formatter:off
	/**
	 * Gets the current power mode.
	 * 
	 * @return The current power mode.
	 * 
	 *         <ul>
	 *             <li>0&nbsp&nbsp	KeyOut</li>
	 *             <li>1&nbsp&nbsp	KeyRecentlyOut</li>
	 *             <li>2&nbsp&nbsp	KeyApproved_0</li>
	 *             <li>3&nbsp&nbsp	PostAccessory_0</li>
	 *             <li>4&nbsp&nbsp	Accessory_1</li>
	 *             <li>5&nbsp&nbsp	PostIgnition_1</li>
	 *             <li>6&nbsp&nbsp	IgnitionOn_2</li>
	 *             <li>7&nbsp&nbsp	Running_2</li>
	 *             <li>8&nbsp&nbsp	(Not used)</li>
	 *             <li>9&nbsp&nbsp	Crank_3</li>
	 *          </ul>
	 *          <p>
	 * Error:&nbsp&nbsp	{@link CANServerSDK#INT_ERROR}
	 */
	// @formatter:on
	public int getPowerMode() {

		final Double value = currentValuesMap.get(PowerMode);
		if (value >= 0 && value <= 9) {
			return value.intValue();
		} else {
			return INT_ERROR;
		}
	}

	// @formatter:off
	/**
	 * Gets the currently user selected economy units.
	 * 
	 * @return The fuel economy units.
	 *         <ul>
	 *             <li>0&nbsp&nbsp	MPG</li>
	 *             <li>1&nbsp&nbsp	MPL</li>
	 *             <li>2&nbsp&nbsp	KM/L</li>
	 *             <li>3&nbsp&nbsp	l/100KM</li>
	 *             <li>4&nbsp&nbsp	Unknown</li>
	 *             <li>5&nbsp&nbsp	Initialisation</li>
	 *          </ul>
	 *          <p>
	 * Error:&nbsp&nbsp	{@link CANServerSDK#INT_ERROR}
	 * 
	 */
	// @formatter:on
	public int getFuelEconomyUnits() {

		final Double value = currentValuesMap.get(FuelEconomyUnits);
		if (value >= 0 && value <= 5) {
			return value.intValue();
		} else {
			return INT_ERROR;
		}
	}

	// @formatter:off
	/**
	 * Gets the current brake pressure.
	 * 
	 * @return The current brake pressure. Valid range: 0 - 205 bar
	 *          <p>
	 * Error:&nbsp&nbsp	{@link CANServerSDK#DBL_ERROR}
	 * 
	 */
	// @formatter:on
	public double getBrakePressure() {

		final Double value = currentValuesMap.get(BrakePressure);
		if (value >= 0 && value <= 205) {
			return value;
		} else {
			return DBL_ERROR;
		}
	}

	// @formatter:off
	/**
	 * Gets the current distance to empty in Km.
	 * 
	 * @return The current distance to empty in Km. Valid range: 0 - 1022 Km.
	 *          <p>
	 * Error:&nbsp&nbsp	{@link CANServerSDK#INT_ERROR}
	 * 
	 */
	// @formatter:on
	public int getDistanceToEmpty() {

		final Double value = currentValuesMap.get(DistanceToEmpty);
		if (value >= 0 && value <= 1022) {
			return value.intValue();
		} else {
			return INT_ERROR;
		}
	}

	// @formatter:off
	/**
	 * Gets the current gear lever selection.
	 * 
	 * @return The current gear lever selection. Valid range:
	 *   	    <ul>
	 *             <li>0&nbsp&nbsp	P (Park)</li>
	 *             <li>1&nbsp&nbsp	R (Reverse)</li>
	 *             <li>2&nbsp&nbsp	N (Neutral)</li>
	 *             <li>3&nbsp&nbsp	D (Drive)</li>
	 *             <li>4&nbsp&nbsp	4th</li>
	 *             <li>5&nbsp&nbsp	3rd</li>
	 *             <li>5&nbsp&nbsp	L (Low)</li>
	 *             <li>7&nbsp&nbsp	Not defined</li>
	 *          </ul>
	 *          <p>
	 * Error:&nbsp&nbsp	{@link CANServerSDK#INT_ERROR}
	 */
	// @formatter:on
	public int getGearPosition() {

		final Double value = currentValuesMap.get(GearLeverPosition);
		if (value >= 0 && value <= 7) {
			return value.intValue();
		} else {
			return INT_ERROR;
		}
	}

	// @formatter:off
	/**
	 * Gets the current low fuel status value.
	 * 
	 * @return The current low fuel status value. Valid range:
	 *   	    <ul>
	 *             <li>0&nbsp&nbsp	Low Fuel Light Off (Fuel Level Ok)</li>
	 *             <li>1&nbsp&nbsp	Low Fuel Light On (Fuel Level Low)</li>
	 *          </ul>
	 *          <p>
	 * Error:&nbsp&nbsp	{@link CANServerSDK#INT_ERROR}
	 */
	// @formatter:on
	public int getLowFuelStatus() {

		final Double value = currentValuesMap.get(LowFuelLevel);
		if (value == 0 || value == 1) {
			return value.intValue();
		} else {
			return INT_ERROR;
		}
	}

	// @formatter:off
	/**
	 * Gets the status of the electronic park brake.
	 * 
	 * @return The current electronic park brake status. Valid range:
	 *   	    <ul>
	 *             <li>0&nbsp&nbsp	Electric Park Brake disengaged.</li>
	 *             <li>1&nbsp&nbsp	Electric Park Brake engaged.</li>
	 *          </ul>
	 *          <p>
	 * Error:&nbsp&nbsp	{@link CANServerSDK#INT_ERROR}
	 */
	// @formatter:on
	public int getParkBrakeStatus() {

		final Double value = currentValuesMap.get(EPBStatus);
		if (value == 0 || value == 1) {
			return value.intValue();
		} else {
			return INT_ERROR;
		}
	}

	// @formatter:off
	/**
	 * <b>This API is not currently available.</b>
	 * <p> 
	 * Gets the status of the the Eco-Start Stop-Start Feature.
	 * 
	 * @return The current Eco-Start Stop-Start status. Valid range:
	 *   	    <ul>
	 *             <li>0&nbsp&nbsp	STARTUP</li>
	 *             <li>1&nbsp&nbsp	DEACTIVATED</li>
	 *             <li>2&nbsp&nbsp	DESELECTED</li>
	 *             <li>3&nbsp&nbsp	STOP AVAILABLE</li>
	 *             <li>4&nbsp&nbsp	STOP NOT AVAILABLE</li>
	 *             <li>5&nbsp&nbsp	STOPPED</li>
	 *             <li>6&nbsp&nbsp	PRESS CLUTCH PEDAL</li>
	 *             <li>7&nbsp&nbsp	SELECT NEUTRAL</li>
	 *             <li>8&nbsp&nbsp	PRESS BRAKE PEDAL</li>
	 *             <li>9&nbsp&nbsp	IMMINENT START WARNING</li>
	 *             <li>10&nbsp&nbsp	ERROR</li>
	 *             <li>11&nbsp&nbsp	EXT TEMP</li>
	 *             <li>12&nbsp&nbsp	DPF REGEN</li>
	 *             <li>13&nbsp&nbsp	BATT CHARGE</li>
	 *             <li>14&nbsp&nbsp	SYS DEMAND</li>
	 *             <li>15&nbsp&nbsp	A/C DEMAND</li>
	 *          </ul>
	 *          <p>
	 * Error:&nbsp&nbsp	{@link CANServerSDK#INT_ERROR}
	 */
	// @formatter:on
	public int getStopStartStatus() {

		final Double value = currentValuesMap.get(MHStopStartStatus);
		if (value >= 0 && value <= 15) {
			return value.intValue();
		} else {
			return INT_ERROR;
		}
	}




	// ========================================================================================================================
	// Synchronous Methods: Eco-data Current Trip
	// ========================================================================================================================

	// @formatter:off
	/**
	 * Driver rating speed feedback score for current trip.
	 * 
	 * @return The current speed feedback score.
	 *         Valid range: 1.0 - 5.0
	 *         <p>
	 * Error:&nbsp&nbsp	{@link CANServerSDK#DBL_ERROR}
	 */
	// @formatter:on
	public double getCurrentSpeedScore() {

		final Double value = currentValuesMap.get(TripCurrSpeedFdbScr);
		if (value >= 1.0 && value <= 5.0) {
			return value;
		} else {
			return DBL_ERROR;
		}
	}

	// @formatter:off
	/**
	 * Driver rating throttle feedback score for current trip.
	 * 
	 * @return The current throttle feedback score.
	 *         Valid range: 1.0 - 5.0
	 *         <p>
	 * Error:&nbsp&nbsp	{@link CANServerSDK#DBL_ERROR}
	 */
	// @formatter:on
	public double getCurrentThrottleScore() {

		final Double value = currentValuesMap.get(TripCurrThrottleFdbScr);
		if (value >= 1.0 && value <= 5.0) {
			return value;
		} else {
			return DBL_ERROR;
		}
	}

	// @formatter:off
	/**
	 * Driver rating brake feedback score for current trip.
	 * 
	 * @return The current brake feedback score.
	 *         Valid range: 1.0 - 5.0
	 *         <p>
	 * Error:&nbsp&nbsp	{@link CANServerSDK#DBL_ERROR}
	 */
	// @formatter:on
	public double getCurrentBrakeScore() {

		final Double value = currentValuesMap.get(TripCurrBrakeFdbScr);
		if (value >= 1.0 && value <= 5.0) {
			return value;
		} else {
			return DBL_ERROR;
		}
	}

	// @formatter:off
	/**
	 * The current fuel economy for current trip.
	 * 
	 * @return Valid range: 0 - 99.
	 *         Units depend on FuelEconomyunits.
	 *         <p>
	 * Error:&nbsp&nbsp	{@link CANServerSDK#DBL_ERROR}
	 * 
	 */
	// @formatter:on
	public double getCurrentFuelEconomy() {

		final Double value = currentValuesMap.get(TripCurrFuelEconomy);
		if (value >= 0 && value <= 99) {
			return value;
		} else {
			return DBL_ERROR;
		}
	}

	// @formatter:off
	/**
	 * Driver feedback summary percentage for current trip.
	 * 
	 * @return Valid range: 0 - 100 %.
	 *         <p>
	 * Error:&nbsp&nbsp	{@link CANServerSDK#INT_ERROR}
	 */
	// @formatter:on
	public int getCurrentTripScore() {

		final Double value = currentValuesMap.get(TripCurrFdbSummary);
		if (value >= 0 && value <= 100) {
			return value.intValue();
		} else {
			return INT_ERROR;
		}
	}

	// @formatter:off
	/**
	 * Distance travelled for current trip.
	 * 
	 * @return Valid range:	0 -  1677721 Km
	 *         <p>
	 * Error:&nbsp&nbsp	{@link CANServerSDK#DBL_ERROR}
	 */
	// @formatter:on
	public double getCurrentTripDistance() {

		final Double value = currentValuesMap.get(TripCurrDistance);
		if (value >= 0 && value <= 1677721) {
			return value;
		} else {
			return DBL_ERROR;
		}
	}

	// @formatter:off
	/**
	 * <b>This API is not currently available.</b>
	 * <p>
	 * Period for which the ECO Start/Stop was active for
	 * current trip.
	 * 
	 * 
	 * @return Time for which the ECO Engine off, for current trip.
	 * 
	 *         <p>
	 * Error:&nbsp&nbsp	{@link CANServerSDK#INT_ERROR}
	 */
	// @formatter:on
	public double getCurrentEcoStopTimer() {

		final Double value = currentValuesMap.get(TripCurrECOStopTimer);
		return value;
	}

	// @formatter:off
	/**
	 * Average speed over duration current trip.
	 * 
	 * @return Valid range:	0 - 509 Km/h
	 * 
	 *         <p>
	 * Error:&nbsp&nbsp	{@link CANServerSDK#INT_ERROR}
	 */
	// @formatter:on
	public int getCurrentTripAverageSpeed() {

		final Double value = currentValuesMap.get(TripCurrAvSpeed);
		if (value >= 0 && value <= 509) {
			return value.intValue();
		} else {
			return INT_ERROR;
		}
	}

	// @formatter:off
	/**
	 * Gets the current transmission upshift alert status
	 *
	 * @return The current transmisison upshift alert status. Valid range: 0 - 1
	 *          <p>
	 * Error:&nbsp&nbsp	{@link CANServerSDK#INT_ERROR}
	 *
	 */
	// @formatter:on
	public int getTransShiftIndStatus() {


		final Double value = currentValuesMap.get(TransShiftInd);

		if (value >= 0 && value <= 3) {
			return value.intValue();
		} else {
			return INT_ERROR;
		}
	}


	// ========================================================================================================================
	// Synchronous Methods: Seat Belts
	// ========================================================================================================================

	// @formatter:off
	/**
	 * Get seat belt status of a specific seat.
	 * 
	 * @param seatBeltID:
	 *    	    <ul>
	 *             <li>0&nbsp&nbsp	DRIVERS_SEAT<b> - Not currently available.</b></li>
	 *             <li>1&nbsp&nbsp	FRONT_PASSENGER<b> - Not currently available.</b></li>
	 *             <li>2&nbsp&nbsp	REAR_LEFT_PASSENGER</li>
	 *             <li>3&nbsp&nbsp	REAR_MIDDLE_PASSENGER</li>
	 *             <li>4&nbsp&nbsp	REAR_RIGHT_PASSENGER</li>
	 *             <li>5&nbsp&nbsp	THIRD_ROW_LEFT_PASSENGER<b> - Not currently available.</b></li>
	 *             <li>6&nbsp&nbsp	THIRD_ROW_RIGHT_PASSENGER<b> - Not currently available.</b></li>
	 *          </ul>
	 *         <p>
	 * @return seatBeltStatus:
	 * 	      <ul>
	 *             <li>0&nbsp&nbsp	UNBUCKLED</li>
	 *             <li>1&nbsp&nbsp	BUCKLED</li>
	 *             <li>2&nbsp&nbsp	ERROR</li>
	 *          </ul>
	 */
	// @formatter:on
	public SeatBeltStatus getSeatBeltStatus(final SeatBeltID seatBeltID) {

		String seatBeltKey = "";

		// resolve seat belt ID
		switch (seatBeltID) {
		case DRIVERS_SEAT:
			seatBeltKey = DrivSeatBeltBcklState;
			break;
		case FRONT_PASSENGER:
			seatBeltKey = BeltReminderSensor;
			break;
		case REAR_LEFT_PASSENGER:
			seatBeltKey = BeltReminderSensorRL;
			break;
		case REAR_MIDDLE_PASSENGER:
			seatBeltKey = BeltReminderSensorRM;
			break;
		case REAR_RIGHT_PASSENGER:
			seatBeltKey = BeltReminderSensorRR;
			break;
		case THIRD_ROW_LEFT_PASSENGER:
			seatBeltKey = BeltRmderSensorRow3L;
			break;
		case THIRD_ROW_RIGHT_PASSENGER:
			seatBeltKey = BeltRmderSensorRow3R;
			break;
		default:
			Log.e(TAG, "Unknown seat belt ID passed!!");
			break;

		}

		if (seatBeltKey.isEmpty() == false) {
			final Double value = currentValuesMap.get(seatBeltKey);
			if (value == 0 || value == 1) {
				if (value == 0) {
					return SeatBeltStatus.UNBUCKLED;
				} else {
					return SeatBeltStatus.BUCKLED;
				}
			} else {
				return SeatBeltStatus.ERROR;
			}
		} else {
			return SeatBeltStatus.ERROR;
		}
	}

	// @formatter:off
	/**
	 * Get the specific vehicle suspension height.
	 * 
	 * @param SuspensionID. The following suspension IDs are supported:
	 *   	    <ul>
	 *             <li>0&nbsp&nbsp	FRONT_LEFT</li>
	 *             <li>1&nbsp&nbsp	FRONT_RIGHT</li>
	 *             <li>2&nbsp&nbsp	REAR_LEFT</li>
	 *             <li>3&nbsp&nbsp	REAR_RIGHT</li>
	 *          </ul>
	 *         <p>
	 * @return int representing suspension height. Valid range:&nbsp&nbs (-200) - 300mm
	 *        <p>
	 * Error:&nbsp&nbsp	{@link CANServerSDK#INT_ERROR}
	 */
	// @formatter:on
	public int getVehicleSuspension(final SuspensionID suspensionID) {

		String suspensionKey = "";

		// resolve suspension ID
		switch (suspensionID) {
		case FRONT_LEFT:
			suspensionKey = SuspensionHeightFL;
			break;
		case FRONT_RIGHT:
			suspensionKey = SuspensionHeightFR;
			break;
		case REAR_LEFT:
			suspensionKey = SuspensionHeightRL;
			break;
		case REAR_RIGHT:
			suspensionKey = SuspensionHeightRR;
			break;

		default:
			Log.e(TAG, "Unknown suspension ID passed!!");
			break;

		}

		if (suspensionKey.isEmpty() == false) {
			final Double value = currentValuesMap.get(suspensionKey);
			if (value >= -200 && value <= 300) {
				return value.intValue();
			} else {
				return INT_ERROR;
			}
		} else {
			return INT_ERROR;
		}
	}

	// ========================================================================================================================
	// Synchronous Methods: Vehicle Equipment
	// ========================================================================================================================

	// @formatter:off
	/**
	 * <b>This API is not currently available.</b>
	 * <p>   
	 * Get the rain sense wiper status.
	 * 
	 * @return int representing the rain wiper sense status.
	 * 	      <ul>
	 *             <li>0&nbsp&nbsp	Intermittent Mode</li>
	 *             <li>1&nbsp&nbsp	Automatic Mode</li>
	 *          </ul>
	 *        <p>
	 * Error:&nbsp&nbsp	{@link CANServerSDK#INT_ERROR}
	 */
	public int getRainWiperStatus() {

		Integer sigValue;
		final Double value = currentValuesMap.get(RainSenseStatus);
		if (value == 0 || value == 1) {
			sigValue = value.intValue();
		} else {
			sigValue = INT_ERROR;
		}

		return sigValue;
	}


	// @formatter:off
	/**
	 * Get the driver selectd HVAC blower setting.
	 * 
	 * @return int representing the selected HVAC setting.
	 * 	      <ul>
	 *             <li>0&nbsp&nbsp	Undefined</li>
	 *             <li>1&nbsp&nbsp	Low Intensity</li>
	 *             <li>2&nbsp&nbsp	Medium Intensity</li>
	 *             <li>3&nbsp&nbsp	High Intensity</li>
	 *          </ul>
	 *        <p>
	 * Error:&nbsp&nbsp	{@link CANServerSDK#INT_ERROR}
	 */
	public int getHVACBlowerStatus() {

		Integer sigValue;
		final Double value = currentValuesMap.get(HVACAutoBlowerStatus);
		if (value >= 0 && value <= 3) {
			sigValue = value.intValue();
		} else {
			sigValue = INT_ERROR;
		}

		return sigValue;
	}


	// @formatter:off
	/**
	 * Get the rear climate on/off status.
	 * 
	 * @return int representing the rear climate on/off setting
	 * 	      <ul>
	 *             <li>0&nbsp&nbsp	Rear climate off</li>
	 *             <li>1&nbsp&nbsp	Rear climate on</li>
	 *          </ul>
	 *        <p>
	 * Error:&nbsp&nbsp	{@link CANServerSDK#INT_ERROR}
	 */
	public int getRearHVACStatus() {

		Integer sigValue;
		final Double value = currentValuesMap.get(RearSystemOnCmd);
		if (value == 0 || value == 1) {
			sigValue = value.intValue();
		} else {
			sigValue = INT_ERROR;
		}

		return sigValue;
	}



	// @formatter:off
	/**
	 * Gets the status of a particular door.
	 * 
	 * @param doorID Valid range:
	 * 
	 *         <ul>
	 *             <li>0&nbsp&nbsp	DRIVERS_DOOR</li>
	 *             <li>1&nbsp&nbsp	FRONT_PASSENGER_DOOR</li>
	 *             <li>2&nbsp&nbsp	REAR_LEFT_DOOR</li>
	 *             <li>3&nbsp&nbsp	REAR_RIGHT_DOOR</li>
	 *             <li>4&nbsp&nbsp	TAILGATE</li>
	 *             <li>4&nbsp&nbsp	BONNET</li>
	 *          </ul>
	 *          <P>
	 * @return DoorStatus Valid range:
	 * 
	 *         <ul>
	 *             <li>0&nbsp&nbsp	OPEN</li>
	 *             <li>1&nbsp&nbsp	CLOSED</li>
	 *             <li>2&nbsp&nbsp	ERROR</li>
	 *          </ul>
	 */
	// @formatter:on
	public DoorStatus getDoorStatus(final DoorID doorID) {

		DoorStatus doorStatus = null;
		String doorSignalName = null;

		// resolve door ID
		switch (doorID) {

		case DRIVERS_DOOR:
			doorSignalName = DoorDrivers;
			break;
		case FRONT_PASSENGER_DOOR:
			doorSignalName = DoorPassenger;
			break;
		case REAR_LEFT_DOOR:
			doorSignalName = DoorRearLeft;
			break;
		case REAR_RIGHT_DOOR:
			doorSignalName = DoorRearRight;
			break;
		case BONNET:
			doorSignalName = DoorBonnet;
			break;
		case TAILGATE:
			doorSignalName = DoorTailgate;
			break;
		default:
			break;

		}

		// resolve door status
		if (doorSignalName != null && doorSignalName.isEmpty() == false) {

			final Double value = currentValuesMap.get(doorSignalName);
			if (value == 0 || value == 1) {
				if (value == 0) {
					doorStatus = CANServerSDK.DoorStatus.OPEN;
				} else {
					doorStatus = CANServerSDK.DoorStatus.CLOSED;
				}
			} else {
				doorStatus = CANServerSDK.DoorStatus.ERROR;
			}

		} else {
			doorStatus = CANServerSDK.DoorStatus.ERROR;
		}

		return doorStatus;
	}

	// @formatter:off
	/**
	 * Get the current wiper status.
	 * 
	 * @return wiperStatus Valid range:
	 * 
	 *         <ul>
	 *             <li>0&nbsp&nbsp	Wipers Off</li>
	 *             <li>1&nbsp&nbsp	Intermittent Wipe</li>
	 *             <li>2&nbsp&nbsp	Normal (Slow) Continuous Wipe</li>
	 *             <li>3&nbsp&nbsp	Fast Continuous Wipe</li>
	 *         </ul>
	 *          <P>
	 * @param wiperStatus Error:&nbsp&nbsp&nbsp{@link CANServerSDK#INT_ERROR}
	 * 
	 */
	// @formatter:on
	public int getWiperStatus() {

		Integer sigValue;
		final Double value = currentValuesMap.get(WiperStatus);
		if (value >= 0 && value <= 3) {
			sigValue = value.intValue();
		} else {
			sigValue = INT_ERROR;
		}

		return sigValue;
	}

	// ========================================================================================================================
	// Synchronous Methods: Off Road
	// ========================================================================================================================

	// @formatter:off
	/**
	 * The signal indicates whether the vehicle is level
	 * or on an incline nose up or nose down.
	 * 
	 * @return int representing the inclineStatus:
	 * 	      <ul>
	 *             <li>0&nbsp&nbsp	Reserved</li>
	 *             <li>1&nbsp&nbsp	Vehicle is level</li>
	 *             <li>2&nbsp&nbsp	Vehicle nose up</li>
	 *             <li>3&nbsp&nbsp	Vehicle nose down</li>
	 *          </ul>
	 *        <p>
	 * Error:&nbsp&nbsp	{@link CANServerSDK#INT_ERROR}
	 */
	public int getInclineStatus() {

		Integer sigValue;
		final Double value = currentValuesMap.get(WadeInclineDisplay);
		if (value >= 0 && value <= 3) {
			sigValue = value.intValue();
		} else {
			sigValue = INT_ERROR;
		}

		return sigValue;
	}


	// @formatter:off
	/**
	 * This indicates the maximum water wade depth capability of
	 * the vehicle.
	 * 
	 * @return Max Wade Depth. Valid Range: 0 - 1.5m
	 *        <p>
	 * Error:&nbsp&nbsp	{@link CANServerSDK#DBL_ERROR}
	 */
	public double getMaxWadeDepth() {

		final Double value = currentValuesMap.get(WadeMaxDepth);
		if (value >= 0 && value <= 1.5) {
			return value;
		} else {
			return DBL_ERROR;
		}
	}


	// @formatter:off
	/**
	 * This indicates the maximum speed at which the vehicle
	 * can wade through water.
	 * 
	 * @return Max Wade Speed. Valid Range: 0 - 30 Km/h
	 *        <p>
	 * Error:&nbsp&nbsp	{@link CANServerSDK#DBL_ERROR}
	 */
	public double getMaxWadeSpeed() {

		final Double value = currentValuesMap.get(WadeMaxSpeed);
		if (value >= 0 && value <= 30) {
			return value;
		} else {
			return DBL_ERROR;
		}
	}


	// @formatter:off
	/**
	 * The signal indicates the current state of vehicle wading.
	 * 
	 * @return int representing current wade state. Valid Range:
	 *         <ul>
	 *             <li>0&nbsp&nbsp	No Message</li>
	 *             <li>1&nbsp&nbsp	Near Max Depth</li>
	 *             <li>2&nbsp&nbsp	Max Depth Reached</li>
	 *             <li>3&nbsp&nbsp	Max Depth Exceeded</li>
	 *             <li>4&nbsp&nbsp	Manual Display Only</li>
	 *             <li>5&nbsp&nbsp	Sensor Blocked</li>
	 *             <li>6&nbsp&nbsp	Fault</li>
	 *             <li>7&nbsp&nbsp	Reserved 1</li>
	 *             <li>8&nbsp&nbsp	Wade View</li>
	 *             <li>9&nbsp&nbsp	Over Speed</li>
	 *             <li>10&nbsp&nbsp	Reserved 3</li>
	 *             <li>11&nbsp&nbsp	Cold Weather</li>
	 *             <li>12&nbsp&nbsp	Reduce Speed</li>
	 *             <li>13&nbsp&nbsp	Gradient Exceeded</li>
	 *             <li>14&nbsp&nbsp	Reserved 3</li>
	 *             <li>15&nbsp&nbsp	Exit Wade</li>
	 *          </ul>
	 *        <p>
	 * Error:&nbsp&nbsp	{@link CANServerSDK#INT_ERROR}
	 */
	public int getWadeStatus() {

		Integer sigValue;
		final Double value = currentValuesMap.get(WadeMessageReq);
		if (value >= 0 && value <= 6) {
			sigValue = value.intValue();
		} else {
			sigValue = INT_ERROR;
		}

		return sigValue;
	}


	// @formatter:off
	/**
	 * This is the detected water depth at the deepest point around
	 * the vehicle and takes into account the vehicle suspension
	 * height (if fitted) and vehicle gradient.
	 * 
	 * @return Wade Depth Speed. Valid Range: 0 - 1.5m
	 *        <p>
	 * Error:&nbsp&nbsp	{@link CANServerSDK#DBL_ERROR}
	 */
	public double getWadeDepth() {

		final Double value = currentValuesMap.get(WadeWaterDepth);
		if (value >= 0 && value <= 1.5) {
			return value;
		} else {
			return DBL_ERROR;
		}
	}


	// @formatter:off
	/**
	 * Transfer box range signal.
	 * 
	 * @return int representing transfer box range signal. Valid Range:
	 *         <ul>
	 *             <li>0&nbsp&nbsp	Low Range</li>
	 *             <li>1&nbsp&nbsp	Neutral Tow mode selected</li>
	 *             <li>2&nbsp&nbsp	High Range</li>
	 *             <li>3&nbsp&nbsp	Range change in process</li>
	 *          </ul>
	 *        <p>
	 * Error:&nbsp&nbsp	{@link CANServerSDK#INT_ERROR}
	 */
	public int getDiffActualRange() {

		Integer sigValue;
		final Double value = currentValuesMap.get(CDiffRangeActual);
		if (value >= 0 && value <= 3) {
			sigValue = value.intValue();
		} else {
			sigValue = INT_ERROR;
		}

		return sigValue;
	}



	// @formatter:off
	/**
	 * Indicates that the Auto Terrain Response program is on or off.
	 * 
	 * @return int representing Auto Terrain Response program status. Valid Range:
	 *         <ul>
	 *             <li>0&nbsp&nbsp	Off</li>
	 *             <li>1&nbsp&nbsp	On</li>
	 *          </ul>
	 *        <p>
	 * Error:&nbsp&nbsp	{@link CANServerSDK#INT_ERROR}
	 */
	public int getAutoTerrainStatus() {

		Integer sigValue;
		final Double value = currentValuesMap.get(AutoTRStatus);
		if (value == 0 || value == 1) {
			sigValue = value.intValue();
		} else {
			sigValue = INT_ERROR;
		}

		return sigValue;

	}


	// @formatter:off
	/**
	 * The 'Drive mode' or 'Terrain mode' selected by the driver.
	 * 
	 * @return int representing the driver selected drive mode. Valid Range:
	 *         <ul>
	 *             <li>0&nbsp&nbsp	Standard Mode</li>
	 *             <li>1&nbsp&nbsp	TO: Grass/Gravel/Snow / JDO: Winter Mode</li>
	 *             <li>2&nbsp&nbsp	Undefined</li>
	 *             <li>3&nbsp&nbsp	TO: Sand</li>
	 *             <li>4&nbsp&nbsp	TO: Mud</li>
	 *             <li>5&nbsp&nbsp	TO: Rock Crawl</li>
	 *             <li>6&nbsp&nbsp	Dynamic Mode</li>
	 *             <li>7&nbsp&nbsp	Fail Safe Default</li>
	 *          </ul>
	 *        <p>
	 * Error:&nbsp&nbsp	{@link CANServerSDK#INT_ERROR}
	 */
	public int getManualTerrainStatus() {

		Integer sigValue;
		final Double value = currentValuesMap.get(TerrainModeReq);
		if (value >= 0 && value <= 7) {
			sigValue = value.intValue();
		} else {
			sigValue = INT_ERROR;
		}

		return sigValue;
	}

	// @formatter:off
	/**
	 * Gets the driver rating status.
	 * 
	 * @return The driver rating status:
	 *         <ul>
	 *             <li>0&nbsp&nbsp	Enabled</li>
	 *             <li>1&nbsp&nbsp	Sand</li>
	 *             <li>2&nbsp&nbsp	Mud-Ruts</li>
	 *             <li>3&nbsp&nbsp	Rock-Crawl</li>
	 *             <li>4&nbsp&nbsp	Low-Range</li>
	 *             <li>5&nbsp&nbsp	Hill Descent Control (HDC)</li>
	 *             <li>6&nbsp&nbsp	ACC</li>
	 *             <li>7&nbsp&nbsp	Cruise</li>
	 *             <li>8&nbsp&nbsp	Other</li>
	 *             <li>9&nbsp&nbsp	Unknown</li>
	 *             <li>10&nbsp&nbsp	Initialisation</li>
	 *          </ul>
	 *          <p>
	 * Error:&nbsp&nbsp	{@link CANServerSDK#INT_ERROR}
	 */
	// @formatter:on
	public int getDriverRatingStatus() {

		final Double value = currentValuesMap.get(DriverRatingStatus);
		if (value >= 0 && value <= 10) {
			return value.intValue();
		} else {
			return INT_ERROR;
		}
	}

	// ========================================================================================================================
	// Synchronous Methods: ADAS
	// ========================================================================================================================

	// @formatter:off
	/**
	 * Gets the current traffic sign recognised speed - unitless
	 *
	 * @return The current traffic sign recognised speed. Valid range: 0 - 31
	 *          <p>
	 * Error:&nbsp&nbsp	{@link CANServerSDK#INT_ERROR}
	 *
	 */
	// @formatter:on
	public int getTSRSpdLimit() {


		final Double value = currentValuesMap.get(TSRSpdLimit);

		if (value >= 0 && value <= 31) {
			return value.intValue();
		} else {
			return INT_ERROR;
		}
	}

	// @formatter:off
	/**
	 * Gets the current traffic sign recognised speed units
	 *
	 * @return The current traffic sign recognised speed. Valid range: 0 - 31
	 *          <p>
	 * Error:&nbsp&nbsp	{@link CANServerSDK#INT_ERROR}
	 *
	 */
	// @formatter:on
	public int getTSRSpdLimitUnits() {


		final Double value = currentValuesMap.get(TSRSpdLimitUnits);

		if (value >= 0 && value <= 3) {
			return value.intValue();
		} else {
			return INT_ERROR;
		}
	}


	// @formatter:off
	/**
	 * Gets the current blind spot monitor alert (left) status
	 *
	 * @return blind spot monitor alert (left) status. Valid range: 0 - 1
	 *          <p>
	 * Error:&nbsp&nbsp	{@link CANServerSDK#INT_ERROR}
	 *
	 */
	// @formatter:on
	public int getBSMLeftAlertStatus() {


		final Double value = currentValuesMap.get(BSMLeftAlert);

		if (value >= 0 && value <= 1) {
			return value.intValue();
		} else {
			return INT_ERROR;
		}
	}

	// @formatter:off
	/**
	 * Gets the current blind spot monitor alert (right) status
	 *
	 * @return The current blind spot monitor alert (right) status. Valid range: 0 - 1
	 *          <p>
	 * Error:&nbsp&nbsp	{@link CANServerSDK#INT_ERROR}
	 *
	 */
	// @formatter:on
	public int getBSMRightAlertStatus() {


		final Double value = currentValuesMap.get(BSMRightAlert);

		if (value >= 0 && value <= 1) {
			return value.intValue();
		} else {
			return INT_ERROR;
		}
	}


	// ========================================================================================================================
	// Synchronous Methods: Trip Info
	// ========================================================================================================================


	// @formatter:off
	/**
	 * Gets the HUD requirement status
	 *
	 * @return The current HUD nav requirement status. Valid range: 0 - 3
	 *          <p>
	 * Error:&nbsp&nbsp	{@link CANServerSDK#INT_ERROR}
	 *
	 */
	// @formatter:on
	public int getHUDNaviControlReq() {


		final Double value = currentValuesMap.get(HUDNaviControlReq);

		if (value >= 0 && value <= 3) {
			return value.intValue();
		} else {
			return INT_ERROR;
		}
	}

	// @formatter:off
	/**
	 * Gets the HUD requirement status
	 *
	 * @return The current HUD nav count down bar status. Valid range: 0 - 15
	 *          <p>
	 * Error:&nbsp&nbsp	{@link CANServerSDK#INT_ERROR}
	 *
	 */
	// @formatter:on
	public int getHUDNaviCurCntDwnBarReq() {


		final Double value = currentValuesMap.get(HUDNaviCurCntDwnBarReq);

		if (value >= 0 && value <= 15) {
			return value.intValue();
		} else {
			return INT_ERROR;
		}
	}




	// ************************************************************************************************************************
	// Registration and Un-registration Methods
	// ************************************************************************************************************************

	/**
	 * Register application with a specified port number for CAN Simulator.
	 * 
	 * @param application
	 *            The application context.
	 * @param portNo
	 *            The port no. Valid ports are in the range 1 - 64999.
	 * @throws SocketException
	 * 
	 * @throws PortOutOfRange
	 *             The supplied port is outside of the valid range.
	 * @throws PortAlreadyInUse
	 *             If the supplied port number cannot be obtained.
	 * 
	 */
	public void registerApplication(final Application application,
			final int portNo) throws SocketException {

		// Using Multicast sockets instead of Datagram sockets as Multicast
		// sockets can re-use the port number and we are getting an Exception if
		// we use Datagram packets
		// http://stackoverflow.com/a/7842832/391401
		MulticastSocket dataSocket = null;

		if (application == null) {
			Log.e(TAG, "applicationContext must not be null");
		} else {

			if (appContextList.contains(application) == false) {
				appContextList.add(application);

				// check the range of the supplied port number
				if (portNo < 1 || portNo > 64999) {
					// throw exception if port is out of range
					throw new RuntimeException(PortOutOfRange);
				} else {

					try {
						dataSocket = new MulticastSocket(portNo);
						if (dataSocket != null) {
							runUdpClient(dataSocket);
							appRegistered = true;
							Log.d(TAG, "Valid application context registered");
						}

					} catch (final Exception e) {
						Log.e(TAG, "Socket exception called");
						throw new RuntimeException(PortAlreadyInUse);
						// e.printStackTrace();
					}
				}
			} else {
				Log.d(TAG, "applicationContext already registered!!");
			}

		}
	}

	/**
	 * Un-register application. In un-registering and application context, all
	 * associated listener registrations will also be un-registered.
	 * <p>
	 * Note:
	 * <p>
	 * Once an application is un-registered, reception of vehicle data is
	 * terminated until the application is registered again. Invoking
	 * synchronous APIs require application registration, so all will return
	 * errors until the application registers.
	 * 
	 * @param application
	 *            The application context.
	 */
	public void unRegisterApplication(final Application application) {

		if (application == null) {
			Log.d(TAG, "This application context supplied is null!");
		} else {

			if (appContextList.contains(application)) {

				// remove application context
				appContextList.remove(application);
				appRegistered = false;

				// clear all activity contexts
				activityContextList.clear();

				// remove all async listeners
				highFreqDataListenerList.clear();
				lowFreqListenerList.clear();
				ecoCurrentTripListenerList.clear();
				locationListenerList.clear();
				seatBeltListenerList.clear();
				suspensionListenerList.clear();
				equipmentListenerList.clear();
				offRoadListenerList.clear();

				// init current values map
				initSignalValueMap();

			} else {
				Log.d(TAG,
						"This application context cannot be un-registered because it was never registered!!");
			}

		}
	}

	// @formatter:off
	/**
	 * Returns whether the app (client) is registered to the CSI SDK.
	 * 
	 * @return appRegistered appRegistered = true, app registered appRegistered
	 *         = false, app un-registered
	 */
	// @formatter:on
	private boolean isAppRegistered() {

		return appRegistered;
	}

	/**
	 * Register high frequency data listener.
	 * <p>
	 * The {@link CANServerSDK#registerApplication} must be called prior to
	 * calling this method.
	 * 
	 * @param activity
	 *            The activity context.
	 * @param listener
	 *            The high frequency data listener.
	 * 
	 * @throws AppNotRegistered
	 *             If application is not registered.
	 */
	public void registerIHighFrequencyDataListener(final Context activity,
			final IHighFrequencyDataListener listener) {

		if (isAppRegistered() == false) {
			throw new RuntimeException(AppNotRegistered);
		}

		if (activity == null) {
			Log.e(TAG, "Activity must not be null");
		} else {

			if (activityContextList.contains(activity) == false) {
				activityContextList.add(activity);
			}

			if (listener != null
					&& highFreqDataListenerList.contains(listener) == false) {
				highFreqDataListenerList.add(listener);
				Log.d(TAG, "Valid High Frequency listener registered");
			} else {
				Log.d(TAG, "Invalid High Frequency Listener not registered");
			}

		}
	}

	/**
	 * Un-Register the high frequency data listener
	 * 
	 * @param listener
	 *            The high frequency vehicle data listener.
	 */
	public void unRegisterIHighFrequencyDataListener(
			final IHighFrequencyDataListener listener) {

		if (listener != null) {
			if (highFreqDataListenerList.contains(listener) == true) {
				highFreqDataListenerList.remove(listener);
			} else {
				Log.d(TAG, "Invalid listener supplied for un-registration of IHighFrequencyDataListener.");
			}
		} else {
			Log.d(TAG, "Listener supplied is null.");
		}
	}

	/**
	 * Register low frequency data listener.
	 * <p>
	 * The {@link CANServerSDK#registerApplication} must be called prior to
	 * calling this method.
	 * 
	 * @param activity
	 *            The activity context.
	 * @param listener
	 *            The low frequency data listener.
	 * @throws AppNotRegistered
	 *             If application is not registered.
	 */
	public void registerILowFrequencyDataListener(final Context activity,
			final ILowFrequencyDataListener listener) {

		if (isAppRegistered() == false) {
			throw new RuntimeException(AppNotRegistered);
		}

		if (activity == null) {
			Log.e(TAG, "Activity must not be null");
		} else {

			if (activityContextList.contains(activity) == false) {
				activityContextList.add(activity);
			}

			if (listener != null
					&& lowFreqListenerList.contains(listener) == false) {
				lowFreqListenerList.add(listener);
				Log.d(TAG, "Valid Low Frquency Data listener registered");
			} else {
				Log.d(TAG, "Invalid Low Frquency Data listener not registered");
			}
		}
	}

	/**
	 * Un-Register the low frequency data listener
	 * 
	 * @param listener
	 *            The low frequency vehicle data listener.
	 */
	public void unRegisterILowFrequencyDataListener(
			final ILowFrequencyDataListener listener) {

		if (listener != null) {
			if (lowFreqListenerList.contains(listener) == true) {
				lowFreqListenerList.remove(listener);
			} else {
				Log.d(TAG, "Invalid listener supplied for un-registration.");
			}
		} else {
			Log.d(TAG, "Listener supplied is null.");
		}
	}

	/**
	 * Register the IEcoCurrentTripListener.
	 * <p>
	 * The {@link CANServerSDK#registerApplication} must be called prior to
	 * calling this method.
	 * 
	 * @param activity
	 *            The activity context.
	 * @param listener
	 *            The Eco-data current trip listener.
	 * 
	 * @throws AppNotRegistered
	 *             If application is not registered.
	 */
	public void registerIEcoCurrentTripListener(final Context activity,
			final IEcoCurrentTripListener listener) {

		if (isAppRegistered() == false) {
			throw new RuntimeException(AppNotRegistered);
		}

		if (activity == null) {
			Log.e(TAG, "Activity must not be null");
		} else {

			if (activityContextList.contains(activity) == false) {
				activityContextList.add(activity);
			}

			if (listener != null
					&& ecoCurrentTripListenerList.contains(listener) == false) {
				ecoCurrentTripListenerList.add(listener);
				Log.d(TAG, "Valid Eco Data listener registered");
			} else {
				Log.d(TAG, "Invalid Eco Data Listener not registered");
			}

		}
	}

	/**
	 * Un-Register the IEcoCurrentTripListener.
	 * 
	 * @param listener
	 *            The eco-data current trip listener.
	 */
	public void unRegisterIEcoCurrentTripListener(
			final IEcoCurrentTripListener listener) {

		if (listener != null) {
			if (ecoCurrentTripListenerList.contains(listener) == true) {
				ecoCurrentTripListenerList.remove(listener);
			} else {
				Log.d(TAG, "Invalid listener supplied for un-registration.");
			}
		} else {
			Log.d(TAG, "Listener supplied is null.");
		}
	}

	// @formatter:off
	/**
	 * Register CSI Simulation location listener. This listener will provide
	 * simulated location updates for the selected journey type.
	 * <p>
	 * The {@link CANServerSDK#registerApplication} must be called prior to
	 * calling this method.
	 * 
	 * @param activity
	 *            The activity context.
	 * @param listener
	 *            The location listener.
	 * 
	 * @throws AppNotRegistered
	 *             If application is not registered.
	 */
	// @formatter:on
	public void registerICurrentLocationListener(final Context activity,
			final ICurrentLocationListener listener) {

		if (isAppRegistered() == false) {
			throw new RuntimeException(AppNotRegistered);
		}

		if (activity == null) {
			Log.e(TAG, "Activity must not be null");
		} else {

			if (activityContextList.contains(activity) == false) {
				activityContextList.add(activity);
			}

			if (listener != null
					&& locationListenerList.contains(listener) == false) {
				locationListenerList.add(listener);
				Log.d(TAG, "Valid location data listener registered");
			} else {
				Log.d(TAG, "Invalid location Data Listener not registered");
			}

		}
	}

	/**
	 * Un-Register the simulation location Listener.
	 * 
	 * @param listener
	 *            The location listener.
	 */
	public void unRegisterICurrentLocationListener(
			final ICurrentLocationListener listener) {

		if (listener != null) {
			if (locationListenerList.contains(listener) == true) {
				locationListenerList.remove(listener);
			} else {
				Log.d(TAG, "Invalid listener supplied for un-registration.");
			}
		} else {
			Log.d(TAG, "Listener supplied is null.");
		}
	}

	// @formatter:off
	/**
	 * Register the seat belt listener. This listener will provide
	 * a status of seat belt activity.
	 * <p>
	 * The {@link CANServerSDK#registerApplication} must be called prior to
	 * calling this method.
	 * 
	 * @param activity
	 *            The activity context.
	 * @param listener
	 *            The seat belt listener.
	 * @throws AppNotRegistered
	 *             If application is not registered.
	 */
	// @formatter:on
	public void registerISeatBeltStatusListener(final Context activity,
			final ISeatBeltStatusListener listener) {

		if (isAppRegistered() == false) {
			throw new RuntimeException(AppNotRegistered);
		}

		if (activity == null) {
			Log.e(TAG, "Activity must not be null");
		} else {

			if (activityContextList.contains(activity) == false) {
				activityContextList.add(activity);
			}
			if (listener != null
					&& seatBeltListenerList.contains(listener) == false) {
				seatBeltListenerList.add(listener);
				Log.d(TAG, "Valid seat belt listener registered");
			} else {
				Log.d(TAG, "Invalid seat belt Listener not registered");
			}

		}
	}

	/**
	 * Un-Register the seat belt Listener.
	 * 
	 * @param listener
	 *            The seat belt listener.
	 */
	public void unRegisterISeatBeltStatusListener(
			final ISeatBeltStatusListener listener) {

		if (listener != null) {
			if (seatBeltListenerList.contains(listener) == true) {
				seatBeltListenerList.remove(listener);
			} else {
				Log.d(TAG, "Invalid listener supplied for un-registration.");
			}
		} else {
			Log.d(TAG, "Listener supplied is null.");
		}
	}

	// @formatter:off
	/**
	 * Register the vehicle suspension listener. This listener will provide
	 * access to individual suspension heights.
	 * <p>
	 * The {@link CANServerSDK#registerApplication} must be called prior to
	 * calling this method.
	 * 
	 * @param activity
	 *            The activity context.
	 * @param listener
	 *            The vehicle suspension listener.
	 * @throws AppNotRegistered
	 *             If application is not registered.
	 */
	// @formatter:on
	public void registerIVehicleSuspensionListener(final Context activity,
			final IVehicleSuspensionListener listener) {

		if (isAppRegistered() == false) {
			throw new RuntimeException(AppNotRegistered);
		}

		if (activity == null) {
			Log.e(TAG, "Activity must not be null");
		} else {

			if (activityContextList.contains(activity) == false) {
				activityContextList.add(activity);
			}
			if (listener != null
					&& suspensionListenerList.contains(listener) == false) {
				suspensionListenerList.add(listener);
				Log.d(TAG, "Valid vehicle suspension listener registered");
			} else {
				Log.d(TAG,
						"Invalid vehicle suspension. Listener not registered");
			}

		}
	}

	/**
	 * Un-Register the vehicle suspension listener.
	 * 
	 * @param listener
	 *            The vehicle suspension listener.
	 */
	public void unRegisterIVehicleSuspensionListener(
			final IVehicleSuspensionListener listener) {

		if (listener != null) {
			if (suspensionListenerList.contains(listener) == true) {
				suspensionListenerList.remove(listener);
			} else {
				Log.d(TAG, "Invalid listener supplied for un-registration.");
			}
		} else {
			Log.d(TAG, "Listener supplied is null.");
		}
	}

	// @formatter:off
	/**
	 * Register the vehicle equipment listener. This listener will provide
	 * access to the status of selected driver equipment within the vehicle.
	 * <p>
	 * The {@link CANServerSDK#registerApplication} must be called prior to
	 * calling this method.
	 * 
	 * @param activity
	 *            The activity context.
	 * @param listener
	 *            The vehicle equipment listener.
	 * @throws AppNotRegistered
	 *             If application is not registered.
	 */
	// @formatter:on
	public void registerIVehicleEquipmentListener(final Context activity,
			final IVehicleEquipmentListener listener) {

		if (isAppRegistered() == false) {
			throw new RuntimeException(AppNotRegistered);
		}

		if (activity == null) {
			Log.e(TAG, "Activity must not be null");
		} else {

			if (activityContextList.contains(activity) == false) {
				activityContextList.add(activity);
			}
			if (listener != null
					&& equipmentListenerList.contains(listener) == false) {
				equipmentListenerList.add(listener);
				Log.d(TAG, "Valid vehicle equipment listener registered");
			} else {
				Log.d(TAG, "Invalid vehicle equipment. Listener not registered");
			}

		}
	}

	/**
	 * Un-Register the vehicle equipment listener.
	 * 
	 * @param listener
	 *            The vehicle equipment listener.
	 */
	public void unRegisterIVehicleEquipmentListener(
			final IVehicleEquipmentListener listener) {

		if (listener != null) {
			if (equipmentListenerList.contains(listener) == true) {
				equipmentListenerList.remove(listener);
			} else {
				Log.d(TAG, "Invalid listener supplied for un-registration.");
			}
		} else {
			Log.d(TAG, "Listener supplied is null.");
		}
	}

	// @formatter:off
	/**
	 * Register the vehicle off-road listener. This listener will provide
	 * access to specific Off-Road related CAN signals.
	 * <p>
	 * The {@link CANServerSDK#registerApplication} must be called prior to
	 * calling this method.
	 * 
	 * @param activity
	 *            The activity context.
	 * @param listener
	 *            The vehicle off-road listener.
	 * @throws AppNotRegistered
	 *             If application is not registered.
	 */
	// @formatter:on
	public void registerIOffRoadStatusListener(final Context activity,
			final IOffRoadStatusListener listener) {

		if (isAppRegistered() == false) {
			throw new RuntimeException(AppNotRegistered);
		}

		if (activity == null) {
			Log.e(TAG, "Activity must not be null");
		} else {

			if (activityContextList.contains(activity) == false) {
				activityContextList.add(activity);
			}
			if (listener != null
					&& offRoadListenerList.contains(listener) == false) {
				offRoadListenerList.add(listener);
				Log.d(TAG, "Valid vehicle off-road listener registered");
			} else {
				Log.d(TAG, "Invalid vehicle off-road. Listener not registered");
			}

		}
	}

	/**
	 * Un-Register the vehicle off-road listener.
	 * 
	 * @param listener
	 *            The vehicle off-road listener.
	 */
	public void unRegisterIOffRoadStatusListener(
			final IOffRoadStatusListener listener) {

		if (listener != null) {
			if (offRoadListenerList.contains(listener) == true) {
				offRoadListenerList.remove(listener);
			} else {
				Log.d(TAG, "Invalid listener supplied for un-registration.");
			}
		} else {
			Log.d(TAG, "Listener supplied is null.");
		}
	}


	/**
	 * Register ADAS data listener.
	 * <p>
	 * The {@link CANServerSDK#registerApplication} must be called prior to
	 * calling this method.
	 *
	 * @param activity
	 *            The activity context.
	 * @param listener
	 *            The ADAS data listener.
	 * @throws AppNotRegistered
	 *             If application is not registered.
	 */
	public void registerIADASListener(final Context activity,
												  final IADASListener listener) {

		if (isAppRegistered() == false) {
			throw new RuntimeException(AppNotRegistered);
		}

		if (activity == null) {
			Log.e(TAG, "Activity must not be null");
		} else {

			if (activityContextList.contains(activity) == false) {
				activityContextList.add(activity);
			}

			if (listener != null
					&& adasListenerList.contains(listener) == false) {
				adasListenerList.add(listener);
				Log.d(TAG, "Valid ADAS listener registered");
			} else {
				Log.d(TAG, "Invalid ADAS listener not registered");
			}
		}
	}

	/**
	 * Un-Register the ADAS data listener
	 *
	 * @param listener
	 *            The ADAS data listener.
	 */
	public void unRegisterIADASListener(
			final IADASListener listener) {

		if (listener != null) {
			if (adasListenerList.contains(listener) == true) {
				adasListenerList.remove(listener);
			} else {
				Log.d(TAG, "Invalid listener supplied for un-registration of IADASListener.");
			}
		} else {
			Log.d(TAG, "Listener supplied is null.");
		}
	}






	/**
	 * Register Trip Info listener.
	 * <p>
	 * The {@link CANServerSDK#registerApplication} must be called prior to
	 * calling this method.
	 *
	 * @param activity
	 *            The activity context.
	 * @param listener
	 *            The ADAS data listener.
	 * @throws AppNotRegistered
	 *             If application is not registered.
	 */
	public void registerITripInfoListener(final Context activity,
									  final ITripInfoListener listener) {

		if (isAppRegistered() == false) {
			throw new RuntimeException(AppNotRegistered);
		}

		if (activity == null) {
			Log.e(TAG, "Activity must not be null");
		} else {

			if (activityContextList.contains(activity) == false) {
				activityContextList.add(activity);
			}

			if (listener != null
					&& tripInfoListenerList.contains(listener) == false) {
				tripInfoListenerList.add(listener);
				Log.d(TAG, "Valid Trip Info listener registered");
			} else {
				Log.d(TAG, "Invalid Trip Info listener not registered");
			}
		}
	}

	/**
	 * Un-Register the Trip Info listener
	 *
	 * @param listener
	 *            The Trip Info listener.
	 */
	public void unRegisterITripInfoListener(
			final ITripInfoListener listener) {

		if (listener != null) {
			if (tripInfoListenerList.contains(listener) == true) {
				tripInfoListenerList.remove(listener);
			} else {
				Log.d(TAG, "Invalid listener supplied for un-registration of ITripInfoListener.");
			}
		} else {
			Log.d(TAG, "Listener supplied is null.");
		}
	}




	// ************************************************************************************************************************
	// End Registration and Un-registration Methods
	// ************************************************************************************************************************

	/**
	 * Hex string to byte array.
	 * 
	 * @param s
	 *            the s
	 * @return the byte[]
	 */
	private static byte[] hexStringToByteArray(final String s) {
		final int len = s.length();
		final byte[] data = new byte[len / 2];

		for (int i = 0; i < len; i += 2) {
			data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character
					.digit(s.charAt(i + 1), 16));
		}

		return data;
	}

	/**
	 * Process frame.
	 * 
	 * @param line
	 *            the line
	 */
	private void processFrame(final String line) {

		final String[] lines = line.split(",");
		if (lines != null && lines.length >= 6) {
			final Pair<String, String> currentBusCanPair = new Pair<String, String>(
					lines[0], lines[2]);

			if (lines[0].compareToIgnoreCase(CAN_BUS_CO) == 0
					&& lines[2].compareToIgnoreCase("06A") != 0) {
				manageSignalData(currentBusCanPair, lines[4], false);

			} else if (lines[0].compareToIgnoreCase(CAN_BUS_CO) == 0
					&& lines[2].compareToIgnoreCase("06A") == 0) {
				// multi-plexed Eco-data signal

				manageSignalData(currentBusCanPair, lines[4], true);
			} else {
				// Log.d(TAG, "Unknown Frame");
			}
		} else if (lines.length >= 4) {
			// Location data
			// eg: Location,-1.264773,51.683338,81.0
			if (lines[0].compareToIgnoreCase(LOCATION) == 0) {
				manageLocationData(lines[1], lines[2], lines[3]);
			}
		}
	}

	/**
	 * Byte swap a single short value.
	 * 
	 * @param value
	 *            Value to byte swap.
	 * @return Byte swapped representation.
	 */
	private static short swap(final short value) {
		final int b1 = value & 0xff;
		final int b2 = value >> 8 & 0xff;

		return (short) (b1 << 8 | b2 << 0);
	}

	/**
	 * Manages the extraction of signal data from multiplexed CAN frames.
	 * 
	 * @param currentBusCanPair
	 *            The current bus can pair
	 * @param line
	 *            The line string representing the 8 byte data
	 * @param multiplexed
	 *            the multiplexed
	 */
	private void manageSignalData(final Pair<String, String> currentBusCanPair,
			final String line, final boolean multiplexed) {

		double result = 0;

		// get the signal list objects
		final ArrayList<Signal> signalList = canSignalMap
				.get(currentBusCanPair);

		// extract 8-bytes from string to byte array
		final String data = line.replace(" ", "");
		byte[] newBytes = new byte[8];

		if (multiplexed) {
			newBytes = hexStringToByteArray(data);
		} else {
			final byte[] oldBytes = hexStringToByteArray(data);

			// reverse the byte order into another byte array because RDM data
			// comes in Motorola Backwards notation
			final int length = oldBytes.length;
			for (int i = 0; i < length; i++) {
				newBytes[i] = oldBytes[length - i - 1];
			}
		}

		// cycle through all the signal objects
		for (final Signal signal : signalList) {

			if (multiplexed) {
				if (signal.getBlockNumber() != newBytes[0]) {
					continue;
				}
			}

			// create BigInteger, representing payload data
			BigInteger payloadData = new BigInteger(1, newBytes);

			// create BigInteger representing the byte mask
			final byte input[] = { (byte) 0xff, (byte) 0xff, (byte) 0xff,
					(byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff,
					(byte) 0xff };
			BigInteger byteMask = new BigInteger(1, input);

			if (signal.getByteSwap() != 0) {
				payloadData = payloadData.shiftRight(signal.getStartBit() - 8);
				byteMask = byteMask.shiftRight(MAX_BITS - signal.getLength());
				payloadData = payloadData.and(byteMask);
				result = swap(payloadData.shortValue());
			} else {
				payloadData = payloadData.shiftRight(signal.getStartBit());
				byteMask = byteMask.shiftRight(MAX_BITS - signal.getLength());
				payloadData = payloadData.and(byteMask);
				result = payloadData.doubleValue();
			}

			// apply scale and offset
			result = result * signal.getScale();
			result = result + signal.getOffset();

			// get current value of signal from the currentValuesMap
			final double currSignalValue = currentValuesMap.get(signal
					.getName());

			// write new value into currentValuesMap
			currentValuesMap.put(signal.getName(), result);

			// Notify any subscribers on change of value of signal
			if (currSignalValue != result) {

				// notify any subscribers
				notifySubscribers(signal.getName());
			}

		}
	}

	/**
	 * Manage location data.
	 * 
	 * @param longitude
	 *            the longitude
	 * @param latitude
	 *            the latitude
	 * @param altitude
	 *            the altitude
	 */
	private void manageLocationData(final String longitude,
			final String latitude, final String altitude) {
		final Location location = new Location("Sample");
		location.setLongitude(Double.parseDouble(longitude));
		location.setLatitude(Double.parseDouble(latitude));
		location.setAltitude(Double.parseDouble(altitude));

		for (final ICurrentLocationListener listener : locationListenerList) {
			listener.onNewLocationUpdate(location);
		}
	}

	/**
	 * Manages the identifying of which callback method to call with the signals
	 * new value.
	 * 
	 * @param theSignalName
	 *            The signal name
	 */
	private void notifySubscribers(final String theSignalName) {

		Runnable callbackRunnable = null;

		// For interface APIs where we are amalgamating a number of CAN signals
		// into a single super-interface method, we need to have the specific
		// updated CAN signal name in the runnable method. The existing
		// architecture does not allow for this (CAN signal name) to be passed
		// through as an argument.

		// For the seat belt and suspension interfaces, the following scheme of
		// wrapping a runnable in a method, allowing the run() method to call a
		// function to which the specific updated CAN signal name is passed,
		// will be used.
		if (theSignalName.contains("Belt") || theSignalName.contains("Susp")
				|| theSignalName.contains("Door")) {
			callbackRunnable = Super_Runnable(theSignalName);
		} else {
			callbackRunnable = aSyncMap.get(theSignalName);
		}
		callbackRunnable.run();
	}

	/**
	 * Run udp client.
	 * 
	 * @param dataSocket
	 *            The UDP data socket created from the user defined port number.
	 */
	private void runUdpClient(final MulticastSocket dataSocket) {

		if (udpAlreadyRunning == false) {
			udpAlreadyRunning = true;
		} else {
			return;
		}

		new AsyncTask<Void, Void, String>() {
			@Override
			protected String doInBackground(final Void... params) {
				try {
					final byte[] lMsg = new byte[MAX_UDP_DATAGRAM_LEN];
					final DatagramPacket dp = new DatagramPacket(lMsg,
							lMsg.length);

					while (true) {
						dataSocket.receive(dp);
						final String lText = new String(lMsg, 0, dp.getLength());

						// TODO: Should we save the data even if there are no
						// current listeners? Whats if an app registers and
						// queries for a previous value?
						if (activityContextList.isEmpty() == false) {
							final String[] lines = lText.split("\n");
							if (lines != null && lines.length >= 1) {

								// Concurrent processing
								final ExecutorService executorService = Executors
										.newFixedThreadPool(4);
								for (final String line : lines) {
									executorService.submit(new Runnable() {
										@Override
										public void run() {
											processFrame(line);
										}
									});
								}
								executorService.shutdown();
								try {
									executorService.awaitTermination(
											Long.MAX_VALUE,
											TimeUnit.NANOSECONDS);
								} catch (final InterruptedException e) {

								}
								// printCurrentValuesMap();
								if (isAppRegistered() == false) {
									break;
								}
							}
						}
					}

				} catch (final SocketException e) {
					e.printStackTrace();
				} catch (final IOException e) {
					e.printStackTrace();
				} finally {
					if (dataSocket != null) {
						dataSocket.close();
					}
				}
				return "true";
			}
		}.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
	}

}
