/*
 * Copyright Notice: This software is proprietary to Jaguar Land Rover Limited.
 *  ©  2015 Jaguar Land Rover Limited
 *
 */


package com.jlr.ica.csi;

/**
 * The IADASListener interface will report CAN data related to the
 * ADAS-data collated by the vehicle for the current trip
 *
 * A class wishing to implement this is interface is to register itself with the
 * interface and implement all the abstract methods contained within.
 * <p>
 * See {@link CANServerSDK#registerIADASCurrentTripListener}.
 * <p>
 * CAN Signals used in this interface include:
 * <p>
 * <ul>
 * <li>Traffic Sign Recognised Speed</li>
 * <li>Traffic Sign Recognised Speed Units</li>

 * </ul>
 *
 * @author JLR
 * @version 2015.01
 * @since 1.0
 *
 * @see IHighFrequencyDataListener
 * @see ILowFrequencyDataListener
 * @see ICurrentLocationListener
 * @see ISeatBeltStatusListener
 * @see IVehicleEquipmentListener
 * @see IVehicleSuspensionListener
 * @see IEcoCurrentTripListener
 */
public interface IADASListener {

    // @formatter:off
    /**
     * The recognised value of the traffic sign recognition speed - unitless
     *
     * @param tsrSpeedLimit Valid range:
     *   	     <ul>
     *             <li>0&nbsp&nbsp	No display</li>
     *             <li>1&nbsp&nbsp	Cancelled</li>
     *             <li>2&nbsp&nbsp	5</li>
     *             <li>3&nbsp&nbsp	10</li>
     *             <li>4&nbsp&nbsp	15</li>
     *             <li>5&nbsp&nbsp	20</li>
     *             <li>6&nbsp&nbsp	25</li>
     *             <li>7&nbsp&nbsp	30</li>
     *             <li>8&nbsp&nbsp	35</li>
     *             <li>9&nbsp&nbsp	40</li>
     *             <li>10&nbsp&nbsp	45</li>
     *             <li>11&nbsp&nbsp	50</li>
     *             <li>12&nbsp&nbsp	55</li>
     *             <li>13&nbsp&nbsp	60</li>
     *             <li>14&nbsp&nbsp	65</li>
     *             <li>15&nbsp&nbsp	70</li>
     *             <li>16&nbsp&nbsp	75</li>
     *             <li>17&nbsp&nbsp	80</li>
     *             <li>18&nbsp&nbsp	85</li>
     *             <li>19&nbsp&nbsp	90</li>
     *             <li>20&nbsp&nbsp	95</li>
     *             <li>21&nbsp&nbsp	100</li>
     *             <li>22&nbsp&nbsp	105</li>
     *             <li>23&nbsp&nbsp	110</li>
     *             <li>24&nbsp&nbsp	115</li>
     *             <li>25&nbsp&nbsp	120</li>
     *             <li>26&nbsp&nbsp	125</li>
     *             <li>27&nbsp&nbsp	130</li>
     *             <li>28&nbsp&nbsp	Reserved</li>
     *             <li>29&nbsp&nbsp	Reserved</li>
     *             <li>30&nbsp&nbsp	Reserved</li>
     *             <li>31&nbsp&nbsp	Reserved</li>
     *          </ul>
     *
     * @param tsrSpeedLimit Error:&nbsp&nbsp&nbsp{@link CANServerSDK#INT_ERROR}
     *
     */
    // @formatter:on
    public void onTSRSpeedLimitChanged(int tsrSpeedLimit);

    // @formatter:off
    /**
     * The recognised value of the traffic sign recognition speed unit
     *
     * @param tsrSpeedLimitUnits Valid range:
     *   	    <ul>
     *             <li>0&nbsp&nbsp	Unknown</li>
     *             <li>1&nbsp&nbsp	Km/h</li>
     *             <li>2&nbsp&nbsp	Mph</li>
     *             <li>3&nbsp&nbsp	Not defined</li>
     *          </ul>
     *
     * @param tsrSpeedLimitUnits Error:&nbsp&nbsp&nbsp{@link CANServerSDK#INT_ERROR}
     *
     */
    // @formatter:on
    public void onTSRSpeedLimitUnitsChanged(int tsrSpeedLimitUnits);


    // @formatter:off
    /**
     * The status of the Blind Spot Monitor (left)
     *
     * @param bsmLeftAlert Valid range:
     *   	    <ul>
     *             <li>0&nbsp&nbsp	No alert</li>
     *             <li>1&nbsp&nbsp	Alert</li>
     *          </ul>
     *
     * @param bsmLeftAlert Error:&nbsp&nbsp&nbsp{@link CANServerSDK#INT_ERROR}
     *
     */
    public void onBSMLeftAlertChanged(int bsmLeftAlert);


    // @formatter:off
    /**
     * The status of the Blind Spot Monitor (right)
     *
     * @param bsmRightAlert Valid range:
     *   	    <ul>
     *             <li>0&nbsp&nbsp	No alert</li>
     *             <li>1&nbsp&nbsp	Alert</li>
     *          </ul>
     *
     * @param bsmRightAlert Error:&nbsp&nbsp&nbsp{@link CANServerSDK#INT_ERROR}
     *
     */
    public void onBSMRightAlertChanged(int bsmRightAlert);


}
