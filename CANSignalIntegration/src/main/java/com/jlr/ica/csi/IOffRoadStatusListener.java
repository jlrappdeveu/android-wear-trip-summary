/*
 * Copyright Notice: This software is proprietary to Jaguar Land Rover Limited.  
 *  ©  2015 Jaguar Land Rover Limited
 * 
 */
package com.jlr.ica.csi;

/**
 * The IOffRoadStatusListener interface provides access to CAN signals related
 * to specific Off-Road driving capability. A class wishing to implement this is
 * interface is to register itself with the interface and implement all the
 * abstract methods contained within.
 * <p>
 * See {@link CANServerSDK#registerIOffRoadStatusListener}.
 * <p>
 * CAN Signals used in this interface include:
 * <p>
 * <ul>
 * <li>Wade Incline</li>
 * <li>Wade Speed</li>
 * <li>Wade Depth</li>
 * <li>Low-range Gear Selection</li>
 * <li>Auto-Terrain Mode On/Off Status</li>
 * <li>Driver Selected Manual Terrain Mode</li>
 * </ul>
 * 
 * @author JLR
 * @version 2015.01
 * @since 1.0
 * 
 * @see IHighFrequencyDataListener
 * @see ILowFrequencyDataListener
 * @see ICurrentLocationListener
 * @see IEcoCurrentTripListener
 * @see ISeatBeltStatusListener
 * @see IVehicleSuspensionListener
 * @see IVehicleEquipmentListener
 * 
 */
public interface IOffRoadStatusListener {

	// @formatter:off
	/**
	 * The signal indicates whether the vehicle is level
	 * or on an incline nose up or nose down. 
	 * 
	 * @param inclineStatus Valid range:
	 * 
	 *         <ul>
     *             <li>0&nbsp&nbsp	Reserved</li>
     *             <li>1&nbsp&nbsp	Vehicle is level</li>
     *             <li>2&nbsp&nbsp	Vehicle nose up</li>   
     *             <li>3&nbsp&nbsp	Vehicle nose down</li>   
     *          </ul>
     *          <P>
	 * @param inclineStatus Error:&nbsp&nbsp&nbsp{@link CANServerSDK#INT_ERROR}
	 */
	// @formatter:on
	public void onInclineStatusChanged(int inclineStatus);

	// @formatter:off
	/**
	 * The signal indicates the current state of vehicle wading.   
	 * 
	 * @param wadeStatus Valid range:
	 * 
	 *         <ul>
     *             <li>0&nbsp&nbsp	No Message</li>
     *             <li>1&nbsp&nbsp	Near Max Depth</li>
     *             <li>2&nbsp&nbsp	Max Depth Reached</li>   
     *             <li>3&nbsp&nbsp	Max Depth Exceeded</li>   
     *             <li>4&nbsp&nbsp	Manual Display Only</li>   
     *             <li>5&nbsp&nbsp	Sensor Blocked</li>             
     *             <li>6&nbsp&nbsp	Fault</li>                                       
     *             <li>7&nbsp&nbsp	Reserved 1</li>
     *             <li>8&nbsp&nbsp	Wade View</li>
     *             <li>9&nbsp&nbsp	Over Speed</li>   
     *             <li>10&nbsp&nbsp	Reserved 3</li>   
     *             <li>11&nbsp&nbsp	Cold Weather</li>   
     *             <li>12&nbsp&nbsp	Reduce Speed</li>             
     *             <li>13&nbsp&nbsp	Gradient Exceeded</li>
     *             <li>14&nbsp&nbsp	Reserved 3</li>
     *             <li>15&nbsp&nbsp	Exit Wade</li>                                    
     *          </ul>
     *          <P>
	 * @param wadeStatus Error:&nbsp&nbsp&nbsp{@link CANServerSDK#INT_ERROR}
	 */
	// @formatter:on
	public void onWadeStatusChanged(int wadeStatus);

	// @formatter:off
	/**
	 * This is the detected water depth at the deepest point around 
	 * the vehicle and takes into account the vehicle suspension 
	 * height (if fitted) and vehicle gradient. 
	 * 
	 * @param wadeDepth Valid range: 0 - 1.5m 
     * <P>
	 * @param wadeDepth Error:&nbsp&nbsp&nbsp{@link CANServerSDK#DBL_ERROR}
	 */
	// @formatter:on
	public void onWadeDepthChanged(double wadeDepth);

	// @formatter:off
	/**
	 * Transfer box range signal.
	 *  
	 * @param diffActualRange Valid range:
	 * 
	 *         <ul>
     *             <li>0&nbsp&nbsp	Low Range</li>
     *             <li>1&nbsp&nbsp	Neutral Tow mode selected</li>
     *             <li>2&nbsp&nbsp	High Range</li>   
     *             <li>3&nbsp&nbsp	Range change in process</li>   
     *          </ul>
     *          <P>
	 * @param diffActualRange Error:&nbsp&nbsp&nbsp{@link CANServerSDK#INT_ERROR}
	 */
	// @formatter:on
	public void onDiffRangeActualChanged(int diffActualRange);

	// @formatter:off
	/**
	 * Indicates that the Auto Terrain Response program is on or off. 
	 * 
	 * @param autoTerrainProgram Valid range:
	 * 
	 *         <ul>
     *             <li>0&nbsp&nbsp	Off</li>
     *             <li>1&nbsp&nbsp	On</li>   
     *          </ul>
     *          <P>
	 * @param autoTerrainProgram Error:&nbsp&nbsp&nbsp{@link CANServerSDK#INT_ERROR}
	 */
	// @formatter:on
	public void onAutoTerrainProgramChanged(int autoTerrainProgram);

	// @formatter:off
	/**
	 * The 'Drive mode' or 'Terrain mode' selected by the driver. 
	 *  
	 * @param manualTerrainProgram Valid range:
	 * 
	 *         <ul>
     *             <li>0&nbsp&nbsp	Standard Mode</li>
     *             <li>1&nbsp&nbsp	TO: Grass/Gravel/Snow / JDO: Winter Mode</li>
     *             <li>2&nbsp&nbsp	Undefined</li>   
     *             <li>3&nbsp&nbsp	TO: Sand</li>
     *             <li>4&nbsp&nbsp	TO: Mud</li>
     *             <li>5&nbsp&nbsp	TO: Rock Crawl</li>
     *             <li>6&nbsp&nbsp	Dynamic Mode</li>   
     *             <li>7&nbsp&nbsp	Fail Safe Default</li>                   
     *          </ul>
     *          <P>
	 * @param manualTerrainProgram Error:&nbsp&nbsp&nbsp{@link CANServerSDK#INT_ERROR}
	 */
	// @formatter:on
	public void onManualTerrainProgramChanged(int manualTerrainProgram);

	// @formatter:off
	/**
	 * The driver rating status has changed.
	 * 
	 * @param driverRatingStatus Valid range:
	 * 
	 *         <ul>
     *             <li>0&nbsp&nbsp	Enabled</li>
     *             <li>1&nbsp&nbsp	Sand</li>
     *             <li>2&nbsp&nbsp	Mud-Ruts</li>   
     *             <li>3&nbsp&nbsp	Rock-Crawl</li>   
     *             <li>4&nbsp&nbsp	Low-Range</li>   
     *             <li>5&nbsp&nbsp	Hill Descent Control (HDC)</li>             
     *             <li>6&nbsp&nbsp	ACC</li>
     *             <li>7&nbsp&nbsp	Cruise</li>
     *             <li>8&nbsp&nbsp	Other</li>   
     *             <li>9&nbsp&nbsp	Unknown</li>   
     *             <li>10&nbsp&nbsp	Initialisation</li>   
     *          </ul>
     *           
	 *  @param driverRatingStatus Error:&nbsp&nbsp&nbsp{@link CANServerSDK#INT_ERROR}
	 */
	// @formatter:on
	public void onDriverRatingStatusChanged(int driverRatingStatus);
}
