/*
 * Copyright Notice: This software is proprietary to Jaguar Land Rover Limited.  
 *  ©  2015 Jaguar Land Rover Limited
 * 
 */
package com.jlr.ica.csi;

/**
 * The ILowFrequencyDataListener interface represents specific dynamic CAN
 * signals whose values change at a lower frequency.
 * 
 * A class wishing to implement this is interface is to register itself with the
 * interface and implement all the abstract methods contained within.
 * <p>
 * See {@link CANServerSDK#registerILowFrequencyDataListener}.
 * <p>
 * CAN Signals used in this interface include:
 * <p>
 * <ul>
 * <li>Vehicle Power Mode</li>
 * <li>Driver Rating Status</li>
 * <li>Brake Pressure</li>
 * <li>Selected Gear</li>
 * <li>Distance to Empty in KM</li>
 * <li>Low Fuel Status</li>
 * <li>Electronic Park Brake Status</li>
 * </ul>
 * 
 * @author JLR
 * @version 2015.01
 * @since 1.0
 * 
 * @see IHighFrequencyDataListener
 * @see ICurrentLocationListener
 * @see IEcoCurrentTripListener
 * @see ISeatBeltStatusListener
 * @see IVehicleEquipmentListener
 * @see IVehicleSuspensionListener
 * @see IOffRoadStatusListener
 */
public interface ILowFrequencyDataListener {

	// @formatter:off
	/**
	 * The power mode has changed.
	 * 
	 * @param powerMode Valid range:
	 *         <ul>
     *             <li>0&nbsp&nbsp	KeyOut</li>
     *             <li>1&nbsp&nbsp	KeyRecentlyOut</li>
     *             <li>2&nbsp&nbsp	KeyApproved_0</li>   
     *             <li>3&nbsp&nbsp	PostAccessory_0</li>   
     *             <li>4&nbsp&nbsp	Accessory_1</li>   
     *             <li>5&nbsp&nbsp	PostIgnition_1</li>   
     *             <li>6&nbsp&nbsp	IgnitionOn_2</li>   
     *             <li>7&nbsp&nbsp	Running_2</li>   
     *             <li>8&nbsp&nbsp	(Not used)</li>   
     *             <li>9&nbsp&nbsp	Crank_3</li>                   
     *          </ul>
     *          
     * @param powerMode Error:&nbsp&nbsp&nbsp{@link CANServerSDK#INT_ERROR}
     *    
	 */
	// @formatter:on
	public void onPowerModeChanged(int powerMode);

	// @formatter:off
	/**
	 * The signal informs about the pressure in the master cylinder.
	 * 
	 * @param brakePressure Valid range: 0 - 205 bar 
     *           
	 * @param brakePressure Error:&nbsp&nbsp&nbsp{@link CANServerSDK#DBL_ERROR} 
	 * 
	 */
	// @formatter:on
	public void onBrakePressureChanged(double brakePressure);

	// @formatter:off
	/**
	 * Trip computer range function displayed value in kilometres only.
	 * Used by navigation function to compare journey length with available
	 * fuel and advise if there is insufficient fuel for the journey. 
	 * 
	 * @param distanceToEmpty Valid range: 0 - 1022 Km 
     *           
	 * @param distanceToEmpty Error:&nbsp&nbsp&nbsp{@link CANServerSDK#INT_ERROR} 
	 * 
	 */
	// @formatter:on
	public void onDistanceToEmptyChanged(int distanceToEmpty);

	// @formatter:off
	/**
	 * The gear lever position.
	 * 
	 * @param gearLeverPosition Valid range:
	 *   	    <ul>
     *             <li>0&nbsp&nbsp	P (Park)</li>
     *             <li>1&nbsp&nbsp	R (Reverse)</li>
     *             <li>2&nbsp&nbsp	N (Neutral)</li>   
     *             <li>3&nbsp&nbsp	D (Drive)</li>   
     *             <li>4&nbsp&nbsp	4th</li>   
     *             <li>5&nbsp&nbsp	3rd</li>                      
     *             <li>6&nbsp&nbsp	L (Low)</li>   
     *             <li>7&nbsp&nbsp	Not defined</li>             
     *          </ul>
     *           
	 * @param gearLeverPosition Error:&nbsp&nbsp&nbsp{@link CANServerSDK#INT_ERROR} 
	 * 
	 */
	// @formatter:on
	public void onGearLeverChanged(int gearLeverPosition);

	// @formatter:off
	/**
	 * Low Fuel Warning Light On/Off
	 * 
	 * @param lowFuelLevelStatus Valid range:
	 *   	    <ul>
     *             <li>0&nbsp&nbsp	Low Fuel Light Off (Fuel Level Ok)</li>
     *             <li>1&nbsp&nbsp	Low Fuel Light On (Fuel Level Low)</li>             
     *          </ul>
     *           
	 * @param lowFuelLevelStatus Error:&nbsp&nbsp&nbsp{@link CANServerSDK#INT_ERROR} 
	 * 
	 */
	// @formatter:on
	public void onLowFuelLevelChanged(int lowFuelLevelStatus);

	// @formatter:off
	/**
	 * Summary of the mode that the Electric Park Brake is in.
	 * 
	 * @param parkBrakeStatus Valid range:
	 *   	    <ul>
     *             <li>0&nbsp&nbsp	Electric Park Brake disengaged.</li>
     *             <li>1&nbsp&nbsp	Electric Park Brake engaged.</li>             
     *          </ul>
     *           
	 * @param parkBrakeStatus Error:&nbsp&nbsp&nbsp{@link CANServerSDK#INT_ERROR} 
	 * 
	 */
	// @formatter:on
	public void onParkBrakeChanged(int parkBrakeStatus);




}
