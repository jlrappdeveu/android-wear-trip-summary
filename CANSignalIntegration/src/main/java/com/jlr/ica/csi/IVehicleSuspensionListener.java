/*
 * Copyright Notice: This software is proprietary to Jaguar Land Rover Limited.  
 *  ©  2015 Jaguar Land Rover Limited
 * 
 */
package com.jlr.ica.csi;

import com.jlr.ica.csi.CANServerSDK.SuspensionID;

/**
 * The IVehicleSuspensionListener interface provides access to the individual
 * air suspension heights of each wheel. A class wishing to implement this is
 * interface is to register itself with the interface and implement all the
 * abstract methods contained within.
 * <p>
 * See {@link CANServerSDK#registerIVehicleSuspensionListener}.
 * <p>
 * CAN Signals used in this interface include:
 * <p>
 * <ul>
 * <li>Air suspension height, Front Left</li>
 * <li>Air suspension height, Front Right</li>
 * <li>Air suspension height, Rear Left</li>
 * <li>Air suspension height, Rear Right</li>
 * </ul>
 * 
 * @author JLR
 * @version 2015.01
 * @since 1.0
 * 
 * @see IHighFrequencyDataListener
 * @see ILowFrequencyDataListener
 * @see ICurrentLocationListener
 * @see IEcoCurrentTripListener
 * @see ISeatBeltStatusListener
 * @see IVehicleEquipmentListener
 * @see IOffRoadStatusListener
 * 
 */
public interface IVehicleSuspensionListener {

	// @formatter:off
	/**
	 * The vehicle suspension has changed.
	 * 
	 * @param SuspensionID. The following suspension IDs are supported:
	 *   	    <ul>
     *             <li>0&nbsp&nbsp	FRONT_LEFT</li>
     *             <li>1&nbsp&nbsp	FRONT_RIGHT</li>   
     *             <li>2&nbsp&nbsp	REAR_LEFT</li>   
     *             <li>3&nbsp&nbsp	REAR_RIGHT</li>   
     *          </ul>
     *           
	 * @param suspensionHeight Valid range:&nbsp&nbsp (-200) - 300mm
	 *        <p>
	 * Error:&nbsp&nbsp	{@link CANServerSDK#INT_ERROR} 
	 */
	// @formatter:on	
	public void onSuspensionChanged(SuspensionID suspensionID,
			int suspensionHeight);

}
