package com.jlr.ica.app2;

import java.util.Date;

/**
 * Created by luke on 11/02/2016.
 */
public class EcoDataPoint {
    Double fuelEconomy;
    String units;
    Date date;

    public EcoDataPoint(Double fuelEconomy, String units, Date date) {
        this.fuelEconomy = fuelEconomy;
        this.units = units;
        this.date = date;
    }
}
