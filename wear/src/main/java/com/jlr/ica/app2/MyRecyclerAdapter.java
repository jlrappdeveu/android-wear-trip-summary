package com.jlr.ica.app2;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.liulishuo.magicprogresswidget.MagicProgressBar;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerAdapter.ViewHolder> implements View.OnClickListener{
    private ArrayList<EcoDataEntry> mDataset;
    private EcoDataEntry.DataType dataType;
    private Context context;


    @Override
    public void onClick(View v) {
        ViewHolder holder = (ViewHolder) v.getTag();

    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public RelativeLayout frameLayout;
        public TextView economy;
        public MagicProgressBar progressBar;
        public TextView date;
        public TextView units;
        public ViewHolder(RelativeLayout v) {
            super(v);
            frameLayout = v;
            economy = (TextView) frameLayout.findViewById(R.id.fuel_ec);
            progressBar = (MagicProgressBar) frameLayout.findViewById(R.id.progress_bar);
            date = (TextView) frameLayout.findViewById(R.id.date);
            units = (TextView) frameLayout.findViewById(R.id.units);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public MyRecyclerAdapter(ArrayList myDataset, EcoDataEntry.DataType dType, Context c) {
        mDataset = myDataset;
        dataType = dType;
        context = c;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public MyRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                           int viewType) {
        // create a new view
        RelativeLayout v = (RelativeLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);

        // Sets the click adapter for the entire cell
        // to the one in this class.
        vh.itemView.setOnClickListener(MyRecyclerAdapter.this);
        vh.itemView.setTag(vh);

        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        EcoDataEntry ecoDataEntry = mDataset.get(position);

        DecimalFormat df = new DecimalFormat("#.#");
        String textStr = df.format(ecoDataEntry.getFloat(dataType));
        holder.economy.setText(textStr);

        float percent = (float) ((double) ecoDataEntry.getFloat(dataType))/60f;
        holder.progressBar.setPercent(percent);

        String dateStr = ecoDataEntry.getDateString();
        holder.date.setText(dateStr);

        holder.units.setText(ecoDataEntry.getUnits(dataType));

        if (position%2==0) {
            holder.frameLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.background));
        } else {
            holder.frameLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.backgroundLight));
        }


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}