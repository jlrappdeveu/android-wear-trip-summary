package com.jlr.ica.app2;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by luke on 22/02/2016.
 */
public class ClockView extends View {
    Context context;
    float mSize = 0.38f;
    float hSize = 0.25f;
    float hWidth = 0.06f;
    float mWidth = 0.06f;
    Calendar date = Calendar.getInstance();
    float hours;
    float minutes;



    public ClockView(Context context) {
        super(context);
        this.context = context;
        date.set(Calendar.HOUR_OF_DAY,12);
        date.set(Calendar.MINUTE,0);
        hours = date.get(Calendar.HOUR) + date.get(Calendar.MINUTE)/60;
        minutes = date.get(Calendar.MINUTE);
    }
    public ClockView(Context context, AttributeSet attrs){
        super(context, attrs);
        this.context = context;
        date.set(Calendar.HOUR_OF_DAY, 12);
        date.set(Calendar.MINUTE,0);
        hours = date.get(Calendar.HOUR) + date.get(Calendar.MINUTE)/60;
        minutes = date.get(Calendar.MINUTE);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int x = getWidth();
        int y = getHeight();


        Paint paint = new Paint();
        paint.setFlags(Paint.ANTI_ALIAS_FLAG);

        //Circle
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(ContextCompat.getColor(getContext(), R.color.alphabb));
        paint.setStrokeWidth(hWidth * x);
        canvas.drawCircle(x / 2, y / 2, -hWidth * x / 2 + x / 2, paint);

        //Markers
        /*paint.setStyle(Paint.Style.FILL);
        paint.setColor(ContextCompat.getColor(getContext(), R.color.alphabb));
        canvas.drawCircle(x / 2, -hSize * x + y / 2, hWidth * x / 2, paint);
        canvas.drawCircle(x / 2, hSize * x + y / 2, hWidth * x / 2, paint);
        canvas.drawCircle(-hSize * x + x / 2, y / 2, hWidth * x / 2, paint);
        canvas.drawCircle(+hSize * x + x / 2, y / 2, hWidth * x / 2, paint);*/


        //hour hand
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(ContextCompat.getColor(getContext(), R.color.alphabb));
        canvas.rotate(360 * hours / 12, x / 2, y / 2);
        canvas.drawRoundRect(-hWidth * x / 2 + x / 2, -y * hSize - hWidth * x / 2 + y / 2, hWidth * x / 2 + x / 2, hWidth * x / 2+y / 2, hWidth * x / 2, hWidth * x / 2, paint);
        canvas.rotate(-360 * hours / 12, x / 2, y / 2);

        //min hand
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(ContextCompat.getColor(getContext(), R.color.alphabb));
        canvas.rotate(360 * minutes / 60, x / 2, y / 2);
        canvas.drawRoundRect(-hWidth*x/2+x/2,-y*mSize- hWidth * x / 2 +y/2,hWidth*x/2+x/2,hWidth * x / 2+y/2,hWidth*x/2,hWidth*x/2,paint);
        canvas.rotate(-360 * minutes / 60, x / 2, y / 2);

    }

    float dt;
    public void setDate(Calendar calendar, final DataShowActivity dataShowActivity){
        date = calendar;
        final float hoursTarget = date.get(Calendar.HOUR) + date.get(Calendar.MINUTE)/60;
        final float minutesTarget = date.get(Calendar.MINUTE);

        final float hoursDelta = hoursTarget-hours;
        final float minutesDelta = minutesTarget-minutes;

        final float steps = 50;
        dt = 2f/steps;
        final float ddt = dt/steps;

        final Timer t = new Timer();
        t.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (hours >= hoursTarget) {
                    t.cancel();
                } else {
                    hours += dt*hoursDelta;
                    minutes += dt*minutesDelta;
                    dt -= ddt;
                    dataShowActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            invalidate();
                        }
                    });
                        }
                    }
                },40, 10);

    }
}