package com.jlr.ica.app2;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

/**
 * Created by luke on 22/02/2016.
 */
public class CustomMapView extends View {
    Bitmap mBitmap;

    public CustomMapView(Context context) {
        super(context);

        mBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.large_map);

        //Log.d("mBitmap2", String.valueOf(mBitmap.getHeight()));
    }

    public CustomMapView(Context context, AttributeSet attrs) {
        super(context, attrs);

            mBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.large_map);

        //Log.d("mBitmap2", String.valueOf(mBitmap.getHeight()));
    }

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int x = getWidth();
        int y = getHeight();
        Log.d("x,y",x+","+y);
        float scale = 0.5f;
        int xOff = 795;
        int yOff = 918;
        Log.d("xOff,yOff",xOff+","+yOff);
        // Load the bitmap as a shader to the paint.
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        Shader shader = new BitmapShader(mBitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
        Matrix localmatrix = new Matrix();
        localmatrix.setScale(scale,scale);
        localmatrix.setTranslate((-xOff+x/2)/scale, (-yOff+y/2)/scale); // a translation for example
        shader.setLocalMatrix(localmatrix);
        paint.setShader(shader);

        // Draw a circle with the required radius.
        float halfWidth = x/2;
        float halfHeight = y/2;
        float radius = Math.min(halfWidth, halfHeight);
        canvas.drawCircle(halfWidth, halfHeight, radius, paint);
    }


}
