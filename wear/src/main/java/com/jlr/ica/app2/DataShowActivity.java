/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jlr.ica.app2;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.wearable.view.CardFragment;
import android.support.wearable.view.DotsPageIndicator;
import android.support.wearable.view.FragmentGridPagerAdapter;
import android.support.wearable.view.GridViewPager;
import android.support.wearable.view.WatchViewStub;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.wearable.DataMap;
import com.txusballesteros.widgets.AnimationMode;
import com.txusballesteros.widgets.FitChart;
import com.txusballesteros.widgets.FitChartValue;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.Column;
import lecho.lib.hellocharts.model.ColumnChartData;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.model.SubcolumnValue;
import lecho.lib.hellocharts.model.Viewport;
import lecho.lib.hellocharts.view.ColumnChartView;
import lecho.lib.hellocharts.view.LineChartView;

public class DataShowActivity extends Activity {
    private static final int NUM_ROWS = 1;
    private static final int NUM_COLS = 4;

    MainAdapter mAdapter;
    FragmentViewPager mPager;
    ArrayList<DataMap> dataMapList;
    ArrayList<EcoDataEntry> ecoDataEntries = new ArrayList<>();
    DotsPageIndicator dotsPageIndicator;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.data_show_activity);

        Intent intent = getIntent();
        Bundle b = getIntent().getExtras();
        //Bundle dataMapBundle = getIntent().getBundleExtra("bundle");
        if (b!=null) {
            dataMapList = DataMap.fromBundle(b).getDataMapArrayList(Constants.NOTIFICATION_ARRAYLIST);
            Log.d("dataMap", "not null");
        } else {
            dataMapList = null;
            Log.d("dataMap", "null");
        }
        ecoDataEntries = getDataEntries();




        mPager = (FragmentViewPager) findViewById(R.id.fragment_container);
        mAdapter = new MainAdapter(this, getFragmentManager());
        mPager.setAdapter(mAdapter);
        mPager.setOffscreenPageCount(10);
        dotsPageIndicator = (DotsPageIndicator) findViewById(R.id.page_indicator);
        dotsPageIndicator.setPager(mPager);
        //dotsPageIndicator.setDotFadeWhenIdle(true);
        dotsPageIndicator.setDotShadowColor(Color.TRANSPARENT);
        dotsPageIndicator.setDotFadeWhenIdle(false);
        dotsPageIndicator.setVisibility(View.INVISIBLE);
        mPager.setOnPageChangeListener(new GridViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, int i1, float v, float v1, int i2, int i3) {
                dotsPageIndicator.onPageScrolled(i, i1, v, v1, i2, i3);
            }

            @Override
            public void onPageSelected(int i, int i1) {
                dotsPageIndicator.onPageSelected(i, i1);
                Log.d("page", String.valueOf(i1));
                if (i1 == 0) {
                    dotsPageIndicator.setVisibility(View.INVISIBLE);
                } else {
                    dotsPageIndicator.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {
                dotsPageIndicator.onPageScrollStateChanged(i);
                if (i == 1) {
                    int curr = mPager.getCurrentItem().x;
                    if (curr > 0) {
                        int scrollPos = ((DetailFragment) mAdapter.retrieveFragment(curr)).getScrollPos();
                        Log.d("scroll", curr + "," + String.valueOf(scrollPos));
                        for (int j = 1; j < NUM_COLS; j++) {
                            if (j != curr) {
                                ((DetailFragment) mAdapter.retrieveFragment(j)).setScrollPos(scrollPos);
                            }
                        }
                    }
                }
            }
        });





    }

    private class MainAdapter extends FragmentGridPagerAdapter {
        Map<Point, Drawable> mBackgrounds = new HashMap<Point, Drawable>();
        private Context mContext;
        private ArrayList<Fragment> fragments = new ArrayList<>();

        public MainAdapter(Context ctx, FragmentManager fm) {
            super(fm);
            mContext = ctx;
        }

        @Override
        public int getRowCount() {
            return NUM_ROWS;
        }

        @Override
        public int getColumnCount(int rowNum) {
            return NUM_COLS;
        }

        @Override
        public Fragment getFragment(int rowNum, int colNum) {
            switch (colNum){
                case 0:
                    SummaryFragment fragment = new SummaryFragment();
                    fragments.add(0, fragment);
                    return fragment;
                case 1:
                    DetailFragment fragment1 = new DetailFragment(EcoDataEntry.DataType.FUEL_EC);
                    fragments.add(1, fragment1);
                    return fragment1;
                case 2:
                    DetailFragment fragment2 = new DetailFragment(EcoDataEntry.DataType.AV_SPEED);
                    fragments.add(2, fragment2);
                    return fragment2;
                case 3:
                    DetailFragment fragment3 = new DetailFragment(EcoDataEntry.DataType.DISTANCE);
                    fragments.add(3, fragment3);
                    return fragment3;
                default:
                    return new SummaryFragment();
            }
        }

        public Fragment retrieveFragment(int i){
            return fragments.get(i);
        }

        @Override
        public Drawable getBackgroundForPage(int row, int column) {
            Point pt = new Point(column, row);
            Drawable drawable = mBackgrounds.get(pt);
            if (drawable == null) {
                Bitmap bm = Bitmap.createBitmap(200, 200, Bitmap.Config.ARGB_8888);
                Canvas c = new Canvas(bm);
                Paint p = new Paint();
                // Clear previous image.
                c.drawRect(0, 0, 200, 200, p);
                p.setAntiAlias(true);
                p.setTypeface(Typeface.DEFAULT);
                p.setTextSize(64);
                p.setColor(Color.LTGRAY);
                p.setTextAlign(Align.CENTER);
                //c.drawText(column+ "-" + row, 100, 100, p);
                drawable = new BitmapDrawable(mContext.getResources(), bm);
                mBackgrounds.put(pt, drawable);
            }
            return drawable;
        }
    }

    public static class MainFragment extends CardFragment {
        private static MainFragment newInstance(int rowNum, int colNum) {
            Bundle args = new Bundle();
            args.putString(CardFragment.KEY_TITLE, "Row:" + rowNum);
            args.putString(CardFragment.KEY_TEXT, "Col:" + colNum);
            MainFragment f = new MainFragment();
            f.setArguments(args);
            return f;
        }
    }

    public class DetailFragment extends Fragment {

        DetailFragment fragment;
        LayoutInflater inflater;
        EcoDataEntry.DataType dataType = EcoDataEntry.DataType.FUEL_EC;
        RecyclerView detailRv;
        LinearLayoutManager detailLayoutManager;

        public DetailFragment(EcoDataEntry.DataType dType){
            dataType=dType;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            fragment = this;
            this.inflater = inflater;


            View v = (View) this.inflater.inflate(R.layout.detail_fragment,
                    container, false);

            TextView title = (TextView) v.findViewById(R.id.title);
            title.setText(EcoDataEntry.getTitle(dataType));


            detailRv = (RecyclerView) v.findViewById(R.id.recycler_view);
            detailLayoutManager = new LinearLayoutManager(getContext());// use a linear layout manager
            detailRv.setLayoutManager(detailLayoutManager);
            MyRecyclerAdapter detailRa = new MyRecyclerAdapter(ecoDataEntries,dataType,getContext());// specify an adapter (see also next example)
            detailRv.setAdapter(detailRa);
            detailRv.setOverScrollMode(View.OVER_SCROLL_IF_CONTENT_SCROLLS);


            return v;
        }

        public int getScrollPos(){
            return detailRv.computeVerticalScrollOffset();
        }

        public void setScrollPos(int newScrollPos){
            int delta = newScrollPos - getScrollPos();
            Log.d("setScroll", String.valueOf(delta));
            detailRv.scrollBy(0, delta);
        }
    }

    public class SummaryFragment extends Fragment implements OnMapReadyCallback,
            GoogleMap.OnMapLongClickListener, GoogleMap.OnMapClickListener{

        ArrayList<Button> items;
        FrameLayout buttonLayout;
        SummaryFragment fragment;
        float translateAlpha = 10;
        int focus = -1;
        int prevFocus = -1;
        int buttonSize = 50; //Default
        int buttonSizeBig = 100; //Default
        float hexD;
        float hexDBig;
        float cX;
        float cY;
        ArrayList<DataPoint> dataPoints;
        LayoutInflater inflater;
        View detailInset;


        EcoDataEntry ecoDataEntryTop;


        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            fragment = this;
            this.inflater = inflater;


            View v = (View) this.inflater.inflate(R.layout.summary_fragment,
                    container, false);

            final WatchViewStub stub = (WatchViewStub) v;


            stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
                @Override
                public void onLayoutInflated(WatchViewStub stub) {
                    buttonLayout = (FrameLayout) stub.findViewById(R.id.button_layout);

                    ViewTreeObserver vto = buttonLayout.getViewTreeObserver();
                    vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                        @Override
                        public void onGlobalLayout() {
                            buttonLayout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                            //dataPoints = getDataPoints();

                            ecoDataEntryTop = ecoDataEntries.get(0);



                            float width = buttonLayout.getMeasuredWidth();
                            float height = buttonLayout.getMeasuredWidth();
                            cX = width/2;
                            cY = height/2;

                            hexD = (float) 0.65*Math.min(width,height);
                            hexDBig = (float) 0.9*Math.min(width,height);
                            buttonSize = (int) Math.round(0.45*hexD);
                            buttonSizeBig = (int) Math.round(0.9*hexDBig);


                            items = new ArrayList<Button>() {};
                            /*for (int i = 0; i < dataPoints.size(); i++) {
                                items.add(createButton(dataPoints.get(i), cX + getHexX(hexD, i), cY + getHexY(hexD, i),i));
                                //On click
                                final int pos = i;
                                items.get(i).setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        animateChangeFocus(pos);
                                    }
                                });
                            }*/
                            for (int i = 0; i < 7; i++) {
                                items.add(createButtonEntry(ecoDataEntryTop, cX + getHexX(hexD, i), cY + getHexY(hexD, i), i));
                                //On click
                                final int pos = i;
                                items.get(i).setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        animateChangeFocus(pos);
                                    }
                                });
                            }



                            for (int i = 0; i < items.size(); i++) {
                                buttonLayout.addView(items.get(i));
                            }



                        }
                    });





                    /*
                    items = new ArrayList<Button>() {};
                    items.add((Button) stub.findViewById(R.id.bt_middle));
                    items.add((Button) stub.findViewById(R.id.bt_up));
                    items.add((Button) stub.findViewById(R.id.bt_right_up));
                    items.add((Button) stub.findViewById(R.id.bt_right_low));
                    items.add((Button) stub.findViewById(R.id.bt_low));
                    items.add((Button) stub.findViewById(R.id.bt_left_low));
                    items.add((Button) stub.findViewById(R.id.bt_left_up));



                    //ArrayList<DataPoint> dataPoints = getDataPoints();
                    for (int i = 0; i < dataPoints.size(); i++) {
                        final int pos = i;
                        DataPoint dp = dataPoints.get(i);
                        String str = dp.score + "\n" + dp.units;

                        items.get(i).setText(str);

                        //On click
                        items.get(i).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                animateChangeFocus(pos);
                            }
                        });
                    }*/
                }
            });

            return v;
        }

        private float getHexX(float D, int point){
            float alpha = D/4;
            float beta = 1.73205080757f*alpha;

            float X = 0;

            switch (point){
                case 0:
                    X=0;
                    break;
                case 1:
                    X=0;
                    break;
                case 2:
                    X=beta;
                    break;
                case 3:
                    X=beta;
                    break;
                case 4:
                    X=0;
                    break;
                case 5:
                    X=-beta;
                    break;
                case 6:
                    X=-beta;
                    break;
            }

            return X;
        }
        public void generateTimeInset() {
            detailInset = (View) inflater.inflate(R.layout.time_inset,
                    buttonLayout, false);

            TextView tvStart = (TextView) detailInset.findViewById(R.id.time_start);
            TextView tvEnd = (TextView) detailInset.findViewById(R.id.time_end);

            SimpleDateFormat formatter = new SimpleDateFormat("h:mm a");

            ClockView clockStart = (ClockView) detailInset.findViewById(R.id.clock_start);
            ClockView clockEnd = (ClockView) detailInset.findViewById(R.id.clock_end);

            Calendar dateStart = CustomUtilities.getDate(ecoDataEntryTop.date-ecoDataEntryTop.duration*1000);
            clockStart.setDate(dateStart,DataShowActivity.this);
            tvStart.setText(formatter.format(dateStart.getTime()));

            Calendar dateEnd = CustomUtilities.getDate(ecoDataEntryTop.date);
            clockEnd.setDate(dateEnd,DataShowActivity.this);
            tvEnd.setText(formatter.format(dateEnd.getTime()));

            buttonLayout.addView(detailInset);
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    detailInset.setElevation(20);
                }
            }, 1);
        }

        private GoogleMap mMap;
        private MapFragment mMapFragment = null;
        private LatLng latLngMap = new LatLng(52.1930018,-1.4818266);
        public void generateMapInset() {
            detailInset = null;
            detailInset = (View) inflater.inflate(R.layout.map_inset,
                    buttonLayout, false);

            //Cover
            CircleCoverView circleCoverView = (CircleCoverView) detailInset.findViewById(R.id.circle_cover);
            circleCoverView.setInnerSize(buttonSizeBig);
            circleCoverView.setVisibility(View.VISIBLE);




            GoogleMapOptions options = new GoogleMapOptions().liteMode(true);
            mMapFragment = MapFragment.newInstance(options);
            FragmentTransaction fragmentTransaction =
                    getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_content, mMapFragment);
            fragmentTransaction.commit();


            // Obtain the MapFragment and set the async listener to be notified when the map is ready.
            mMapFragment.getMapAsync(this);

            //Mask
            CircleMaskView circleMaskView = (CircleMaskView) detailInset.findViewById(R.id.circle_mask);
            //FrameLayout frameLayout = (FrameLayout) detailInset.findViewById(R.id.map_frame);
            circleMaskView.setInnerSize(buttonSizeBig);
            circleMaskView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    animateChangeFocus(-1);
                }
            });
            Animation myFadeInAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.abc_fade_in);
            myFadeInAnimation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {

                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });



            buttonLayout.addView(detailInset);
            detailInset.startAnimation(myFadeInAnimation); //Set animation to your ImageView
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    detailInset.setElevation(20);
                }
            }, 1);
        }
        @Override
        public void onMapLongClick(LatLng latLng) {
            animateChangeFocus(-1);
        }

        @Override
        public void onMapReady(GoogleMap googleMap) {
            // Map is ready to be used.
            mMap = googleMap;

            // Set the long click listener as a way to exit the map.
            mMap.setOnMapLongClickListener(this);

            // Add a marker with a title that is shown in its info window.
            mMap.addMarker(new MarkerOptions().position(latLngMap));

            // Move the camera to show the marker.
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLngMap, 3));

            // Instantiates a new CircleOptions object and defines the center and radius
            final float dist =ecoDataEntryTop.distanceToEmpty;
            CircleOptions circleOptions = new CircleOptions().center(latLngMap)
                    .radius(dist*1000)
                    .strokeColor(ContextCompat.getColor(getContext(),R.color.colorAccent))
                    .strokeWidth(5); // In meters
            mMap.addCircle(circleOptions);

            // Bounds
            float offset = dist/120;
            LatLng latLngSW = new LatLng(latLngMap.latitude-offset, latLngMap.longitude-offset);
            LatLng latLngNE = new LatLng(latLngMap.latitude+offset, latLngMap.longitude+offset);
            final LatLngBounds latLngBounds = new LatLngBounds(latLngSW, latLngNE);

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, 0));
                }
            }, 10);

            final CircleCoverView circleCoverView = (CircleCoverView) detailInset.findViewById(R.id.circle_cover);
            Animation myFadeInAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.abc_fade_out);
            myFadeInAnimation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    circleCoverView.setVisibility(View.INVISIBLE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            circleCoverView.startAnimation(myFadeInAnimation); //Set animation to your ImageView

        }

        @Override
        public void onMapClick(LatLng latLng) {
            animateChangeFocus(-1);
        }


        /*public void generateMapInset() {
            detailInset = null;
            detailInset = (View) inflater.inflate(R.layout.map_inset,
                    buttonLayout, false);

            ViewHolder holder = new ViewHolder();
            holder.mapView = (MapView) detailInset.findViewById(R.id.lite_listrow_map);

            // Initialise the MapView
            holder.initializeMapView();

            Log.d("LatLng", SphericalMercator.lat2y(52.471524)+","+ SphericalMercator.lat2y(-1.891207));

            TextView rangeLabel = (TextView) detailInset.findViewById(R.id.label);
            rangeLabel.setText(Math.round(ecoDataEntryTop.distanceToEmpty)+" km");

            MapView mapView = (MapView) detailInset.findViewById(R.id.map_view);
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mapView.getLayoutParams();
            params.width = (int) Math.round(buttonSizeBig*0.95);
            params.height = (int) Math.round(buttonSizeBig * 0.95);
            mapView.invalidate();

            View view = (View) detailInset.findViewById(R.id.line);
            RelativeLayout.LayoutParams paramsV = (RelativeLayout.LayoutParams) view.getLayoutParams();
            paramsV.width = (int) Math.round(0.5*buttonSizeBig*0.95);
            paramsV.height = 2;
            paramsV.leftMargin = (int) Math.round(cX);

            Animation myFadeInAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.abc_fade_in);
            mapView.startAnimation(myFadeInAnimation); //Set animation to your ImageView


            buttonLayout.addView(detailInset);
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    detailInset.setElevation(20);
                }
            }, 1);
        }

        class ViewHolder implements OnMapReadyCallback {

            MapView mapView;

            TextView title;

            GoogleMap map;

            @Override
            public void onMapReady(GoogleMap googleMap) {
                MapsInitializer.initialize(getApplicationContext());
                map = googleMap;
                LatLng latLng = new LatLng(51.500208, -0.126729);
                if (latLng != null) {
                    MapUtils.setMapLocation(map, latLng);
                }
            }


            public void initializeMapView() {
                if (mapView != null) {
                    // Initialise the MapView
                    mapView.onCreate(null);
                    // Set the map ready callback to receive the GoogleMap object
                    mapView.getMapAsync(this);
                }
            }

        }*/

        public void generateSpeedChartInset(){
            detailInset = (View) inflater.inflate(R.layout.graph_inset,
                    buttonLayout, false);

            TextView title = (TextView) detailInset.findViewById(R.id.title);
            title.setText("Trip Speed");

            Button button = (Button) detailInset.findViewById(R.id.button);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mPager.setCurrentItem(0, 2);
                }
            });

            // Initiate chart
            LineChartView lineChartView = (LineChartView) detailInset.findViewById(R.id.chart);

            // Points
            List<PointValue> values = new ArrayList<PointValue>();
            for (int i = 0; i<100;i++) {
                values.add(new PointValue(i,  0));
            }

            // Line
            Line line = new Line(values).setColor(Color.BLUE);
            line.setFilled(true);
            line.setHasLabels(false);
            line.setColor(Color.WHITE);
            //line.setPointColor(Color.WHITE);
            line.setHasPoints(false);
            //line.setPointRadius(3);
            line.setAreaTransparency(75);
            line.setStrokeWidth(1);

            // Compile lines
            List<Line> lines = new ArrayList<Line>();
            lines.add(line);

            // Define axes
            Axis axisX = new Axis();
            Axis axisY = new Axis().setHasLines(true);

            axisY.setName("mph");
            axisY.setTextColor(Color.WHITE);





            // Zoom
            lineChartView.setZoomEnabled(false);
            //lineChartView.setZoomType(ZoomType.HORIZONTAL);
            //lineChartView.setZoomLevel(1,1,4);

            // Data
            LineChartData data = new LineChartData();
            data.setLines(lines);
            data.setBaseValue(Float.NEGATIVE_INFINITY);
            data.setAxisYLeft(axisY);

            // Final
            lineChartView.setLineChartData(data);

            // re points
            float speed = 0;
            float maxSpeed = 0;
            int i = 0;
            for (PointValue value : line.getValues()) {
                // Here I modify target only for Y values but it is OK to modify X targets as well.
                if (i%10<5) {
                    speed = (float) Math.max(speed + (Math.random() - 0.45) * 20, 0);
                } else {
                    speed = (float) Math.max(speed + (Math.random() - 0.55) * 20, 0);
                }
                maxSpeed = (float) Math.max(maxSpeed,speed);
                value.setTarget(value.getX(), speed);
                i++;
            }

            // Viewport
            final Viewport v = new Viewport(lineChartView.getMaximumViewport());
            v.bottom = 0;
            v.top = maxSpeed;
            v.left = 0;
            v.right = values.size();
            lineChartView.setViewportCalculationEnabled(false);
            lineChartView.setMaximumViewport(v);
            lineChartView.setCurrentViewport(v);

            lineChartView.startDataAnimation();



            //HorizontalScrollView sv = (HorizontalScrollView) detailInset.findViewById(R.id.scrollView);
            //sv.scrollTo(0, sv.getMaxScrollAmount());

            buttonLayout.addView(detailInset);

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    detailInset.setElevation(20);
                }
            }, 10);


        }

        public void generateMPGChartInset(){
            detailInset = (View) inflater.inflate(R.layout.graph_inset,
                    buttonLayout, false);

            TextView title = (TextView) detailInset.findViewById(R.id.title);
            title.setText("Fuel Economy History");

            Button button = (Button) detailInset.findViewById(R.id.button);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mPager.setCurrentItem(0,1);
                }
            });

            // Initiate chart
            LineChartView lineChartView = (LineChartView) detailInset.findViewById(R.id.chart);

            // Points
            int numPoints = Math.min(10,ecoDataEntries.size());
            List<PointValue> values = new ArrayList<PointValue>();
            for (int i = 0; i<numPoints;i++) {
                values.add(new PointValue(i,  0));
            }

            // Line
            Line line = new Line(values).setColor(Color.BLUE).setCubic(true);
            line.setFilled(true);
                line.setHasLabels(false);
                line.setColor(Color.WHITE);
                line.setPointColor(Color.WHITE);
            line.setPointRadius(3);
                line.setAreaTransparency(75);
            line.setStrokeWidth(2);

            // Compile lines
            List<Line> lines = new ArrayList<Line>();
            lines.add(line);

            // Define axes
            Axis axisX = new Axis();
            Axis axisY = new Axis().setHasLines(true);

            axisY.setName("mpg");
            axisY.setTextColor(Color.WHITE);

            // Get fuel ec max values
            int i = numPoints-1;
            float maxFuelEc = 0;
            float minFuelEc = 999;
            for (PointValue value : line.getValues()) {
                float val = ecoDataEntries.get(i).fuelEc;
                maxFuelEc = Math.max(maxFuelEc,val);
                minFuelEc = Math.min(minFuelEc, val);
                i--;
            }



            // Viewport
            final Viewport v = new Viewport(lineChartView.getMaximumViewport());
            v.bottom = Math.max(minFuelEc-5,0);
            v.top = maxFuelEc+5;
            v.left = 0;
            v.right = values.size()-1;
            lineChartView.setViewportCalculationEnabled(false);
            lineChartView.setMaximumViewport(v);
            lineChartView.setCurrentViewport(v);

            // Zoom
            lineChartView.setZoomEnabled(false);
            //lineChartView.setZoomType(ZoomType.HORIZONTAL);
            //lineChartView.setZoomLevel(1,1,4);

            // Data
            LineChartData data = new LineChartData();
            data.setLines(lines);
            data.setBaseValue(Float.NEGATIVE_INFINITY);
            data.setAxisYLeft(axisY);

            // Final
            lineChartView.setLineChartData(data);

            // re points
            i = numPoints-1;
            for (PointValue value : line.getValues()) {
                // Here I modify target only for Y values but it is OK to modify X targets as well.
                value.setTarget(value.getX(), ecoDataEntries.get(i).fuelEc);
                i--;
            }
            lineChartView.startDataAnimation();



            //HorizontalScrollView sv = (HorizontalScrollView) detailInset.findViewById(R.id.scrollView);
            //sv.scrollTo(0, sv.getMaxScrollAmount());

            buttonLayout.addView(detailInset);

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    detailInset.setElevation(20);
                }
            }, 10);


        }

        public void generateColumnInset(){
            detailInset = (View) inflater.inflate(R.layout.column_inset,
                    buttonLayout, false);

            Button button = (Button) detailInset.findViewById(R.id.button);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mPager.setCurrentItem(0,3);
                }
            });

            TextView title = (TextView) detailInset.findViewById(R.id.titleCol);
            title.setText("Mileage - 7 Day");
            // Initiate chart
            ColumnChartView chart = (ColumnChartView) detailInset.findViewById(R.id.chartCol);
            chart.setZoomEnabled(false);

            int numSubcolumns = 1;
            int numColumns = 7;
            // Column can have many stacked subcolumns, here I use 4 stacke subcolumn in each of 4 columns.
            List<Column> columns = new ArrayList<Column>();
            List<SubcolumnValue> values;
            for (int i = 0; i < numColumns; ++i) {

                values = new ArrayList<SubcolumnValue>();
                for (int j = 0; j < numSubcolumns; ++j) {
                    values.add(new SubcolumnValue(0, MyChartUtils.pickColor()));
                }

                Column column = new Column(values);
                column.setHasLabels(false);
                column.setHasLabelsOnlyForSelected(false);
                columns.add(column);
            }

            ColumnChartData data = new ColumnChartData(columns);

            // Set stacked flag.
            data.setStacked(true);




            Axis axisX = new Axis();
            Axis axisY = new Axis().setHasLines(true);
            axisY.setName("Mileage");
            data.setAxisXBottom(null);
            data.setAxisYLeft(axisY);


            chart.setColumnChartData(data);

            // Sort data in to days
            float[] distances = new float[numColumns];
            for (EcoDataEntry ede: ecoDataEntries){
                int age = CustomUtilities.getAgeDays(ede.date);
                if (age>=numColumns){break;}
                distances[age] +=ede.distance;
            }


            float maxVal = 0;
            int i = numColumns;
            for (Column column : data.getColumns()) {
                i--;
                float valCol = 0;
                for (SubcolumnValue value : column.getValues()) {
                    float val = distances[i];
                    valCol += val;
                    value.setTarget(val);
                }
                maxVal = Math.max(maxVal,valCol);
            }

            // Viewport
            final Viewport v = new Viewport(chart.getMaximumViewport());
            v.bottom = 0;
            v.top = maxVal;
            v.left = -1;
            v.right = numColumns;
            chart.setViewportCalculationEnabled(false);
            chart.setMaximumViewport(v);
            chart.setCurrentViewport(v);


            chart.startDataAnimation(500);

            buttonLayout.addView(detailInset);

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    detailInset.setElevation(20);
                }
            }, 10);


        }

        public void generateScoreInset(){
            detailInset = (View) inflater.inflate(R.layout.fit_circle_inset,
                    buttonLayout, false);

            float tripCurrSpeedFdbScr = ecoDataEntryTop.scoreSpeed;
            float tripCurrThrottleFdbScr = ecoDataEntryTop.scoreThrottle;
            float tripCurrBrakeFdbScr = ecoDataEntryTop.scoreBrake;

            TextView speed = (TextView) detailInset.findViewById(R.id.num_speed);
            TextView throttle = (TextView) detailInset.findViewById(R.id.num_throttle);
            TextView braking = (TextView) detailInset.findViewById(R.id.num_braking);
            TextView overall = (TextView) detailInset.findViewById(R.id.num_overall);
            speed.setText(String.valueOf(Math.round(tripCurrSpeedFdbScr)));
            throttle.setText(String.valueOf(Math.round(tripCurrThrottleFdbScr)));
            braking.setText(String.valueOf(Math.round(tripCurrBrakeFdbScr)));
            overall.setText(String.valueOf(Math.round(ecoDataEntryTop.scoreSummary)));


            final FitChart fitChart = (FitChart) detailInset.findViewById(R.id.fitChart);




            fitChart.setAnimationMode(AnimationMode.LINEAR);
            fitChart.setMinValue(0f);
            fitChart.setMaxValue(100f);

            Collection<FitChartValue> values = new ArrayList<>();
            values.add(new FitChartValue((100/3)*tripCurrSpeedFdbScr/5, ContextCompat.getColor(getContext(), R.color.colorAccent)));
            values.add(new FitChartValue((100/3)*tripCurrThrottleFdbScr/5, ContextCompat.getColor(getContext(), R.color.colorAccent1)));
            values.add(new FitChartValue((100/3)*tripCurrBrakeFdbScr/5, ContextCompat.getColor(getContext(), R.color.colorAccent2)));

            fitChart.setValues(values);




            buttonLayout.addView(detailInset);

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    detailInset.setElevation(20);
                }
            }, 10);
        }


        float animationFuelRatio;
        public void generateFuelCostInset(){
            detailInset = (View) inflater.inflate(R.layout.fuel_cost_inset,
                    buttonLayout, false);

            TextView title = (TextView) detailInset.findViewById(R.id.title);
            title.setText("Fuel used this week");

            Typeface custom_font = Typeface.createFromAsset(getAssets(), "fonts/LCD-BOLD.TTF");

            final TextView fuelLitres = (TextView) detailInset.findViewById(R.id.fuel_l);
            final TextView fuelCost = (TextView) detailInset.findViewById(R.id.fuel_cost);
            final TextView fuelPrice = (TextView) detailInset.findViewById(R.id.fuel_price);

            fuelLitres.setTypeface(custom_font);
            fuelCost.setTypeface(custom_font);
            fuelPrice.setTypeface(custom_font);

            final DecimalFormat df = new DecimalFormat("0.00");
            final DecimalFormat df1 = new DecimalFormat("0.0");
            fuelPrice.setText(df1.format(100*ecoDataEntryTop.fuelPrice));

            float numLitres = 0;
            float numCost = 0;
            for (EcoDataEntry ede:ecoDataEntries){
                if (System.currentTimeMillis()-Constants.WEEK_SECONDS<ede.date) {
                    numLitres += ede.getFuelLitres();
                    numCost += ede.getFuelCost();
                }
            }

            final float targetLitres = numLitres;
            final float targetCost = numCost;
            final float dt = 0.01f;

            animationFuelRatio = 0;
            //Declare the timer
            final Timer t = new Timer();
            t.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    animationFuelRatio += dt;
                    if (animationFuelRatio > 1) {
                        t.cancel();
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                fuelLitres.setText(df.format(animationFuelRatio * targetLitres));
                                fuelCost.setText(df.format(animationFuelRatio * targetCost));
                            }
                        });
                            }
                        }
                    },0, 10);


                    buttonLayout.addView(detailInset);

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            detailInset.setElevation(20);
                        }
                    }, 10);
                }

                private float getHexY(float D, int point) {
                    float alpha = D / 4;
                    float beta = 1.73205080757f * alpha;

                    float Y = 0;

                    switch (point) {
                        case 0:
                            Y = 0;
                            break;
                        case 1:
                            Y = -2 * alpha;
                            break;
                        case 2:
                            Y = -alpha;
                            break;
                        case 3:
                            Y = alpha;
                            break;
                        case 4:
                            Y = 2 * alpha;
                            break;
                        case 5:
                            Y = alpha;
                            break;
                        case 6:
                            Y = -alpha;
                            break;
                    }

                    return Y;
                }

                private Button createButton(DataPoint dp, float x, float y, int col) {
                    Button btn = new Button(getContext());
                    btn.setWidth(buttonSize);
                    btn.setHeight(buttonSize);
                    String str = "";
                    Drawable btnDrawable = ContextCompat.getDrawable(getContext(), R.drawable.round_button);
                    DecimalFormat df = new DecimalFormat("#.#");
                    switch (col) {
                        case 0:
                            btnDrawable.setColorFilter((ContextCompat.getColor(getContext(), R.color.md_blue_800)), PorterDuff.Mode.MULTIPLY);
                            str = df.format(dp.score) + "\n" + dp.units;
                            break;
                        case 1:
                            btnDrawable.setColorFilter((ContextCompat.getColor(getContext(), R.color.md_amber_800)), PorterDuff.Mode.MULTIPLY);
                            str = df.format(dp.score) + "\n" + dp.units;
                            break;
                        case 2:
                            btnDrawable.setColorFilter((ContextCompat.getColor(getContext(), R.color.md_green_800)), PorterDuff.Mode.MULTIPLY);
                            str = Math.round(dp.score) + "\n" + dp.units;
                            break;
                        case 3:
                            btnDrawable.setColorFilter((ContextCompat.getColor(getContext(), R.color.md_cyan_800)), PorterDuff.Mode.MULTIPLY);
                            str = Math.round(dp.score) + "\n" + dp.units;
                            break;
                        case 4:
                            btnDrawable.setColorFilter((ContextCompat.getColor(getContext(), R.color.md_purple_800)), PorterDuff.Mode.MULTIPLY);
                            str = Math.round(dp.score) + "\n" + dp.units;
                            break;
                        case 5:
                            btnDrawable.setColorFilter((ContextCompat.getColor(getContext(), R.color.md_lime_800)), PorterDuff.Mode.MULTIPLY);
                            str = formatTime((int) Math.round(dp.score));
                            break;
                        case 6:
                            btnDrawable.setColorFilter((ContextCompat.getColor(getContext(), R.color.md_indigo_800)), PorterDuff.Mode.MULTIPLY);
                            str = formatTime((int) Math.round(dp.score));
                            break;
                        default:
                            btnDrawable.setColorFilter((ContextCompat.getColor(getContext(), R.color.md_blue_600)), PorterDuff.Mode.MULTIPLY);
                            str = dp.score + "\n" + dp.units;
                            break;
                    }
                    btnDrawable.setColorFilter((ContextCompat.getColor(getContext(), R.color.colorPrimary)), PorterDuff.Mode.MULTIPLY);
                    btn.setBackground(btnDrawable);
                    btn.setTextColor(ContextCompat.getColor(getContext(), R.color.white));
                    btn.setAllCaps(false);
                    btn.setElevation(0);
                    FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(buttonSize, buttonSize);
                    params.leftMargin = Math.round(x - buttonSize / 2);
                    params.topMargin = Math.round(y - buttonSize / 2);
                    btn.setLayoutParams(params);

                    btn.setText(str);
                    return btn;
                }

                private Button createButtonEntry(EcoDataEntry dp, float x, float y, int i) {
                    Button btn = new Button(getContext());
                    btn.setWidth(buttonSize);
                    btn.setHeight(buttonSize);
                    String str;
                    Drawable btnDrawable = ContextCompat.getDrawable(getContext(), R.drawable.round_button);
                    DecimalFormat df = new DecimalFormat("0.0");
                    DecimalFormat df2 = new DecimalFormat("0.00");
                    switch (i) {
                        case 0:
                            btnDrawable.setColorFilter((ContextCompat.getColor(getContext(), R.color.md_blue_800)), PorterDuff.Mode.MULTIPLY);
                            str = df.format(dp.fuelEc) + "\n" + Constants.getFuelEcString(dp.fuelEconomyUnits);
                            break;
                        case 1:
                            btnDrawable.setColorFilter((ContextCompat.getColor(getContext(), R.color.md_amber_800)), PorterDuff.Mode.MULTIPLY);
                            str = df.format(dp.avSpeed) + "\n" + Constants.getHUDVehicleSpeedString(dp.hudVehicleSpeedUnits);
                            break;
                        case 2:
                            btnDrawable.setColorFilter((ContextCompat.getColor(getContext(), R.color.md_green_800)), PorterDuff.Mode.MULTIPLY);
                            str = Math.round(dp.distance) + "\n" + Constants.getTripUnitString(dp.tripUnits);
                            break;
                        case 3:
                            btnDrawable.setColorFilter((ContextCompat.getColor(getContext(), R.color.md_cyan_800)), PorterDuff.Mode.MULTIPLY);
                            str = Math.round(dp.scoreSummary) + "\n" + "pts";
                            break;
                        case 4:
                            btnDrawable.setColorFilter((ContextCompat.getColor(getContext(), R.color.md_purple_800)), PorterDuff.Mode.MULTIPLY);
                            str = Math.round(dp.distanceToEmpty) + "\n" + "km";
                            break;
                        case 5:
                            btnDrawable.setColorFilter((ContextCompat.getColor(getContext(), R.color.md_lime_800)), PorterDuff.Mode.MULTIPLY);
                            str = formatTime((int) Math.round(dp.duration));
                            break;
                        case 6:
                            btnDrawable.setColorFilter((ContextCompat.getColor(getContext(), R.color.md_indigo_800)), PorterDuff.Mode.MULTIPLY);
                            Log.d("cost", String.valueOf(dp.getFuelCost()));
                            str = df2.format(dp.getFuelCost()) + "\n" + "GBP";
                            break;
                        default:
                            btnDrawable.setColorFilter((ContextCompat.getColor(getContext(), R.color.md_blue_600)), PorterDuff.Mode.MULTIPLY);
                            str = "";
                            break;
                    }
                    btnDrawable.setColorFilter((ContextCompat.getColor(getContext(), R.color.colorPrimary)), PorterDuff.Mode.MULTIPLY);
                    btn.setBackground(btnDrawable);
                    btn.setTextColor(ContextCompat.getColor(getContext(), R.color.white));
                    btn.setAllCaps(false);
                    btn.setElevation(0);
                    FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(buttonSize, buttonSize);
                    params.leftMargin = Math.round(x - buttonSize / 2);
                    params.topMargin = Math.round(y - buttonSize / 2);
                    btn.setLayoutParams(params);

                    btn.setText(str);
                    return btn;
                }

                public String formatTime(int seconds) {

                    String string = "";

                    int hours = (int) Math.round(Math.floor((float) seconds / 3600));
                    seconds -= hours * 3600;
                    int mins = (int) Math.round(Math.floor((float) seconds / 60));
                    seconds -= mins * 60;

                    if (hours > 0) {
                        string = hours + "h " + mins + "m";
                    } else if (mins > 0) {
                        string = mins + "m " + seconds + "s";
                    } else {
                        string = seconds + "s";
                    }


                    return string;
                }

                public void animateChangeFocus(int pos) {
                    prevFocus = focus;
                    focus = pos;
                    if (pos != -1) {
                        float translateBeta = 1.73205080757f * translateAlpha;
                        for (int i = 0; i < items.size(); i++) {
                            if (i != pos) {//move all others out
                                items.get(i).setElevation(0);
                                if (i == 0) {
                                    translateButton(i, getHexX(hexD, i), getHexX(hexDBig, pos), getHexY(hexD, i), getHexY(hexDBig, pos));
                                } else {
                                    translateButton(i, getHexX(hexD, i), getHexX(hexDBig, i), getHexY(hexD, i), getHexY(hexDBig, i));
                                }
                            } else {
                                items.get(i).setElevation(5);
                                translateButton(i, getHexX(hexD, i), 0, getHexY(hexD, i), 0);
                            }
                        }
                    } else {

                        for (int i = 0; i < items.size(); i++) {
                            if (i != prevFocus) {
                                if (i == 0) {
                                    translateButton(i, getHexX(hexDBig, prevFocus), getHexX(hexD, i), getHexY(hexDBig, prevFocus), getHexY(hexD, i));
                                } else {
                                    translateButton(i, getHexX(hexDBig, i), getHexX(hexD, i), getHexY(hexDBig, i), getHexY(hexD, i));
                                }
                            } else {
                                translateButton(i, 0, getHexX(hexD, i), 0, getHexY(hexD, i));
                            }
                        }

                    }
                }

                private void translateButton(int i, float x1, float x2, float y1, float y2) {
                    final int X1 = Math.round(x1);
                    final int X2 = Math.round(x2);
                    final int Y1 = Math.round(y1);
                    final int Y2 = Math.round(y2);

                    // Animate


                    AnimationSet animSet = new AnimationSet(true);
                    animSet.setFillAfter(true);
                    animSet.setDuration(200);
                    animSet.setInterpolator(new AccelerateDecelerateInterpolator());
                    TranslateAnimation translateAnimation;
                    if (i == focus) {
                        float scaling = ((float) buttonSizeBig) / ((float) buttonSize);
                        ScaleAnimation scaleAnimation = new ScaleAnimation(1f, scaling, 1f, scaling, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                        animSet.addAnimation(scaleAnimation);

                        translateAnimation = new TranslateAnimation(0, X2 - X1, 0, Y2 - Y1);
                        translateAnimation.setAnimationListener(new animationEndListener2(i, X1, X2, Y1, Y2));
                    } else if (i == prevFocus) {
                        buttonLayout.removeView(detailInset);
                        float scaling = ((float) buttonSizeBig) / ((float) buttonSize);
                        ScaleAnimation scaleAnimation = new ScaleAnimation(1f, 1 / scaling, 1f, 1 / scaling, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                        animSet.addAnimation(scaleAnimation);

                        translateAnimation = new TranslateAnimation(0, X2 - X1, 0, Y2 - Y1);
                        translateAnimation.setAnimationListener(new animationEndListener2(i, X1, X2, Y1, Y2));
                    } else {
                        translateAnimation = new TranslateAnimation(0, X2 - X1, 0, Y2 - Y1);
                        translateAnimation.setAnimationListener(new animationEndListener2(i, X1, X2, Y1, Y2));

                        if (focus == -1) {
                            items.get(i).setAlpha(1.0f);
                        }
                    }

                    animSet.addAnimation(translateAnimation);

                    items.get(i).startAnimation(animSet);
                }




        public class animationEndListener implements Animation.AnimationListener {

                    int I;
                    int X1;
                    int X2;
                    int Y1;
                    int Y2;

                    public animationEndListener(int i, int X1, int X2, int Y1, int Y2) {
                        I = i;
                        this.X1 = X1;
                        this.X2 = X2;
                        this.Y1 = Y1;
                        this.Y2 = Y2;
                    }


                    @SuppressLint("NewApi")
                    public void onAnimationEnd(Animation animation) {
                        //Handler handler = new Handler();
                        //handler.postDelayed(new Runnable() {
                        //@Override
                        //public void run() {
                        Button newBtn = createButtonEntry(ecoDataEntryTop, 0, 0, I);


                        newBtn.setOnClickListener(new View.OnClickListener() {

                            public void onClick(View v) {
                                if (focus != -1) {
                                    animateChangeFocus(-1);
                                } else {
                                    animateChangeFocus(I);
                                }
                            }
                        });
                        int custButtonSize = buttonSize;
                        if (I == focus) {
                            newBtn.setElevation(10);
                            custButtonSize = buttonSizeBig;

                        } else {
                            newBtn.setElevation(0);

                        }
                        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(custButtonSize, custButtonSize);
                        params.leftMargin = Math.round(cX + X2 - custButtonSize / 2);
                        params.topMargin = Math.round(cY + Y2 - custButtonSize / 2);
                        newBtn.setLayoutParams(params);


                        if (I == focus) {
                            //buttonLayout.addView(newBtn);
                        } else {
                            //buttonLayout.addView(newBtn, 0);
                        }

                        //buttonLayout.removeView(items.get(I));
                        //items.get(I).setVisibility(View.INVISIBLE);


                        buttonLayout.addView(newBtn, I);
                        buttonLayout.invalidate();
                        final Button btn = newBtn;
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                buttonLayout.removeView(items.get(I));
                                buttonLayout.invalidate();
                                items.set(I, btn);
                            }
                        }, 1);


                        //}
                        //},400*I);


                    }

                    public void onAnimationRepeat(Animation animation) {

                    }

                    public void onAnimationStart(Animation animation) {
                    }
                }

                public class animationEndListener2 implements Animation.AnimationListener {

                    int I;
                    int X1;
                    int X2;
                    int Y1;
                    int Y2;

                    public animationEndListener2(int i, int X1, int X2, int Y1, int Y2) {
                        I = i;
                        this.X1 = X1;
                        this.X2 = X2;
                        this.Y1 = Y1;
                        this.Y2 = Y2;
                    }


                    public void onAnimationEnd(Animation animation) {

                        items.get(I).clearAnimation();
                        items.get(I).setOnClickListener(new View.OnClickListener() {

                            public void onClick(View v) {
                                if (focus != -1) {
                                    animateChangeFocus(-1);
                                } else {
                                    animateChangeFocus(I);
                                }
                            }
                        });

                        int custButtonSize = buttonSize;
                        if (I == focus) {
                            items.get(I).setElevation(10);
                            custButtonSize = buttonSizeBig;
                            //items.get(I).setEnabled(true);
                            switch (I) {
                                case 0:
                                    generateMPGChartInset();
                                    break;
                                case 1:
                                    generateSpeedChartInset();
                                    break;
                                case 2:
                                    generateColumnInset();
                                    break;
                                case 3:
                                    generateScoreInset();
                                    break;
                                case 4:
                                    generateMapInset();
                                    break;
                                case 5:
                                    generateTimeInset();
                                    break;
                                case 6:
                                    generateFuelCostInset();
                                    break;
                                default:
                                    generateMPGChartInset();
                                    break;
                            }

                        } else {
                            items.get(I).setElevation(0);
                            items.get(I).setTextColor(ContextCompat.getColor(getContext(), R.color.white));
                            if (focus != -1) {
                                items.get(I).setAlpha(0.5f);
                            }

                        }
                        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(custButtonSize, custButtonSize);
                        params.leftMargin = Math.round(cX + X2 - custButtonSize / 2);
                        params.topMargin = Math.round(cY + Y2 - custButtonSize / 2);
                        items.get(I).setLayoutParams(params);
                    }

                    public void onAnimationRepeat(Animation animation) {

                    }

                    public void onAnimationStart(Animation animation) {
                        if (I == focus) {
                            items.get(I).setTextColor(ContextCompat.getColor(getContext(), R.color.transparent));
                        }
                        //items.get(I).setEnabled(true);
                    }
                }

            }





    /*public ArrayList<DataPoint> getDataPoints(){
        EcoDataEntry ecoDataEntryTop;

        Double tripCurrAvSpeed;
        Double tripCurrFuelEconomy;
        Double tripCurrDistance;
        Double tripCurrECOStopTimer; //seconds
        Double tripCurrSpeedFdbScr;
        Double tripCurrThrottleFdbScr;
        Double tripCurrBrakeFdbScr;
        Double tripCurrFdbSummary;
        String fuelEconomyUnits;
        String tripUnits;
        String hudVehicleSpeedUnits;
        Double distanceToEmpty; //km

        if (dataMap==null) {
            tripCurrAvSpeed = 41.2;
            tripCurrFuelEconomy = 47.3;
            tripCurrDistance = 103.0;
            tripCurrECOStopTimer = 156.0; //seconds
            tripCurrSpeedFdbScr = 4.0;
            tripCurrThrottleFdbScr = 4.0;
            tripCurrBrakeFdbScr = 3.0;
            tripCurrFdbSummary = 67.0;
            fuelEconomyUnits = "mpg";
            tripUnits = "miles";
            hudVehicleSpeedUnits = "mph";
            distanceToEmpty = 534.0; //km
            Log.d("dataMap","null2");

            ecoDataEntryTop = new EcoDataEntry();
            ecoDataEntryTop.avSpeed = 30 + (float) (Math.random()*30);
            ecoDataEntryTop.fuelEc = 30 + (float) (Math.random()*20);
            ecoDataEntryTop.distance = 0 + (float) (Math.random()*100);
            ecoDataEntryTop.ecoStopTimer = 30 + (int) (Math.random()*120);
            ecoDataEntryTop.scoreSpeed = 1 + (float) (Math.random()*4);
            ecoDataEntryTop.scoreThrottle = 1 + (float) (Math.random()*4);
            ecoDataEntryTop.scoreBrake = 1 + (float) (Math.random()*4);
            ecoDataEntryTop.scoreSummary = 30 + (float) (Math.random()*70);
            ecoDataEntryTop.fuelEconomyUnits = "mpg";
            ecoDataEntryTop.tripUnits = "miles";
            ecoDataEntryTop.hudVehicleSpeedUnits = "mph";
            ecoDataEntryTop.distanceToEmpty = 30 + (float) (Math.random()*500);
            ecoDataEntryTop.date = System.currentTimeMillis();
        } else {
            dataMap.getInt("id");
            tripCurrAvSpeed = (double) dataMap.getFloat("avSpeed");
            tripCurrFuelEconomy = (double) dataMap.getFloat("fuelEc");
            tripCurrDistance = (double) dataMap.getFloat("distance");
            tripCurrECOStopTimer = (double) dataMap.getInt("ecoStopTimer");
            tripCurrSpeedFdbScr = (double) dataMap.getFloat("scoreSpeed");
            tripCurrBrakeFdbScr = (double) dataMap.getFloat("scoreThrottle");
            tripCurrBrakeFdbScr = (double) dataMap.getFloat("scoreBrake");
            tripCurrFdbSummary = (double) dataMap.getFloat("scoreSummary");
            fuelEconomyUnits = dataMap.getString("fuelEconomyUnits");
            tripUnits = dataMap.getString("tripUnits");
            hudVehicleSpeedUnits = dataMap.getString("hudVehicleSpeedUnits");
            distanceToEmpty = (double) dataMap.getFloat("distanceToEmpty");
            dataMap.getLong("date");

            ecoDataEntryTop = new EcoDataEntry(dataMap);
        }


        ArrayList<DataPoint> dataPoints = new ArrayList<DataPoint>(){};

        // Fuel economy
        dataPoints.add(new DataPoint(tripCurrFuelEconomy,0.0,fuelEconomyUnits,"Fuel Economy", "Fuel Economy for the previous trip"));
        // Average speed
        dataPoints.add(new DataPoint(tripCurrAvSpeed,0.0,hudVehicleSpeedUnits,"Speed", "Average speed for the previous trip"));
        // Dist
        dataPoints.add(new DataPoint(tripCurrDistance,0.0,tripUnits,"Distance", "Trip distance"));
        // Driver score
        dataPoints.add(new DataPoint(tripCurrFdbSummary,100.0,"pts","ECO", "Driver eco score"));
        // Remaining fuel
        dataPoints.add(new DataPoint(distanceToEmpty,100.0,"km","Fuel range", "Distance to empty"));
        // Duration
        Double tripDuration = 3600*tripCurrDistance/tripCurrAvSpeed; // sec
        dataPoints.add(new DataPoint(tripDuration,0.0,"sec","duration", "Trip duration"));
        // Time eco stop
        dataPoints.add(new DataPoint(tripCurrECOStopTimer,0.0,"s","ECO stop", "Time in ECO stop"));

        return dataPoints;
    }*/

            public ArrayList<EcoDataEntry> getDataEntries(){
        ArrayList<EcoDataEntry> ecoDataEntries = new ArrayList<>();

        if (dataMapList!=null&&dataMapList.size()>0) {
            for (DataMap dataMap : dataMapList) {
                EcoDataEntry ecoDataEntry;
                if (dataMap == null) {
                    ecoDataEntry = new EcoDataEntry();
                } else {
                    ecoDataEntry = new EcoDataEntry(dataMap);
                }
                ecoDataEntries.add(ecoDataEntry);
            }
        } else {
            for (int i = 0;i<10;i++) {
                ecoDataEntries.add(new EcoDataEntry());
            }
        }

        return ecoDataEntries;
    }
}
