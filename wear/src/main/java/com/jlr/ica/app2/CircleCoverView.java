package com.jlr.ica.app2;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by luke on 24/02/2016.
 */
public class CircleCoverView extends View {
    Context context;
    float innerSize = 150;
    public CircleCoverView(Context context) {
        super(context);
        this.context = context;
    }

    public CircleCoverView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int w = getWidth();
        int h = getHeight();
        float r = innerSize/2;

        /*Paint paint = new Paint();
        paint.setFlags(Paint.ANTI_ALIAS_FLAG);

        Bitmap image = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        image.eraseColor(ContextCompat.getColor(context,R.color.colorPrimaryDark));
        canvas.drawBitmap(image, 0, 0, paint);

        Paint p = new Paint();
        p.setFlags(Paint.ANTI_ALIAS_FLAG);
        p.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        canvas.drawCircle(w/2, h/2, 0.8f*w/2, p);*/

        Paint paint = new Paint();
        paint.setFlags(Paint.ANTI_ALIAS_FLAG);

        //Circle
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
        canvas.drawCircle(w/2,h/2,r,paint);
    }

    public void setInnerSize(float size){
        this.innerSize = size;
    }

}
