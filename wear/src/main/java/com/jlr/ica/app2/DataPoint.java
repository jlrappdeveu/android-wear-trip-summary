package com.jlr.ica.app2;

/**
 * Created by luke on 11/02/2016.
 */
public class DataPoint {
    Double score;
    Double maxScore;
    String units;
    String shortDesc;
    String longDesc;

    public DataPoint(Double score, Double maxScore, String units, String shortDesc, String longDesc) {
        this.score = score;
        this.maxScore = maxScore;
        this.units = units;
        this.shortDesc = shortDesc;
        this.longDesc = longDesc;
    }
}
