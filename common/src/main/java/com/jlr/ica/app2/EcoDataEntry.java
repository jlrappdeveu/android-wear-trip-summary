package com.jlr.ica.app2;

import android.util.Log;

import com.google.android.gms.wearable.DataMap;

import java.util.Calendar;

/**
 * Created by luke on 16/02/2016.
 */
public class EcoDataEntry {

    float avSpeed;
    float fuelEc;
    float distance;
    int ecoStopTimer;
    float scoreSpeed;
    float scoreThrottle;
    float scoreBrake;
    float scoreSummary;
    int fuelEconomyUnits;
    int tripUnits;
    int hudVehicleSpeedUnits;
    float distanceToEmpty;
    long date;
    int duration;
    int id;
    float fuelPrice;

    public enum DataType{
        AV_SPEED, FUEL_EC, DISTANCE, ECO_STOP,
        SCORE_SPEED, SCORE_THROTTLE, SCORE_BRAKE, SCORE_SUMMARY, FUEL_EC_UNITS, TRIP_UNITS,
        SPEED_UNITS,DIST_TO_EMPTY,DATE,DURATION,FUEL_PRICE,ID
    }

    public EcoDataEntry(float avSpeed, float fuelEc, float distance, int ecoStopTimer, float scoreSpeed, float scoreThrottle, float scoreBrake, float scoreSummary, int fuelEconomyUnits, int tripUnits, int hudVehicleSpeedUnits, float distanceToEmpty, long date, float fuelPrice) {
        this.avSpeed = avSpeed;
        this.fuelEc = fuelEc;
        this.distance = distance;
        this.ecoStopTimer = ecoStopTimer;
        this.scoreSpeed = scoreSpeed;
        this.scoreThrottle = scoreThrottle;
        this.scoreBrake = scoreBrake;
        this.scoreSummary = scoreSummary;
        this.fuelEconomyUnits = fuelEconomyUnits;
        this.tripUnits = tripUnits;
        this.hudVehicleSpeedUnits = hudVehicleSpeedUnits;
        this.distanceToEmpty = distanceToEmpty;
        this.date = date;
        this.duration = Math.round(distance/avSpeed)*3600;
        this.fuelPrice = fuelPrice;
    }

    public EcoDataEntry(long date, float fuelPrice) {
        this.date = date;
        this.fuelPrice = fuelPrice;
    }

    public EcoDataEntry(int id, float avSpeed, float fuelEc, float distance, int ecoStopTimer, float scoreSpeed, float scoreThrottle, float scoreBrake, float scoreSummary, int fuelEconomyUnits, int tripUnits, int hudVehicleSpeedUnits, float distanceToEmpty, long date, float fuelPrice) {
        this.avSpeed = avSpeed;
        this.fuelEc = fuelEc;
        this.distance = distance;
        this.ecoStopTimer = ecoStopTimer;
        this.scoreSpeed = scoreSpeed;
        this.scoreThrottle = scoreThrottle;
        this.scoreBrake = scoreBrake;
        this.scoreSummary = scoreSummary;
        this.fuelEconomyUnits = fuelEconomyUnits;
        this.tripUnits = tripUnits;
        this.hudVehicleSpeedUnits = hudVehicleSpeedUnits;
        this.distanceToEmpty = distanceToEmpty;
        this.date = date;
        this.id = id;
        this.duration = Math.round(3600*distance/avSpeed);
        this.fuelPrice = fuelPrice;
    }

    public EcoDataEntry() {
        this.avSpeed = 30 + (float) (Math.random() * 30);
        this.fuelEc = 30 + (float) (Math.random() * 20);
        this.distance = 0 + (float) (Math.random() * 100);
        this.ecoStopTimer = 30 + (int) (Math.random() * 120);
        this.scoreSpeed = 1 + (float) (Math.random() * 4);
        this.scoreThrottle = 1 + (float) (Math.random() * 4);
        this.scoreBrake = 1 + (float) (Math.random() * 4);
        this.scoreSummary = 30 + (float) (Math.random() * 70);
        this.fuelEconomyUnits = 0;
        this.tripUnits = 1;
        this.hudVehicleSpeedUnits = 1;
        this.distanceToEmpty = 30 + (float) (Math.random() * 500);
        this.date = System.currentTimeMillis();
        this.duration = Math.round(3600 * this.distance / this.avSpeed);
        this.fuelPrice = 1.029f;
    }

    public EcoDataEntry(DataMap map){

        this.avSpeed = map.getFloat("avSpeed");
        this.fuelEc = map.getFloat("fuelEc");
        this.distance = map.getFloat("distance");
        this.ecoStopTimer = map.getInt("ecoStopTimer");
        this.scoreSpeed = map.getFloat("scoreSpeed");
        this.scoreThrottle = map.getFloat("scoreThrottle");
        this.scoreBrake = map.getFloat("scoreBrake");
        this.scoreSummary = map.getFloat("scoreSummary");
        this.fuelEconomyUnits = map.getInt("fuelEconomyUnits");
        this.tripUnits = map.getInt("tripUnits");
        this.hudVehicleSpeedUnits = map.getInt("hudVehicleSpeedUnits");
        this.distanceToEmpty = map.getFloat("distanceToEmpty");
        this.date = map.getLong("date");
        this.id = map.getInt("id");
        this.duration = map.getInt("duration");
        this.fuelPrice = map.getFloat("fuelPrice");
    }

    public String getString(){
        return id+" sp:"+Math.round(avSpeed)+" d:"+Math.round(distance)+" s:"+Math.round(scoreSummary)+" time:"+CustomUtilities.getDate(date,"hh:mm");
    }

    public String getDateString(){
        return CustomUtilities.getDate(date,"HH:mm dd/MM");
    }

    public DataMap putToDataMap(DataMap map) {
        map.putInt("id", id);
        map.putFloat("avSpeed", avSpeed);
        map.putFloat("fuelEc", fuelEc);
        map.putFloat("distance", distance);
        map.putInt("ecoStopTimer", ecoStopTimer);
        map.putFloat("scoreSpeed", scoreSpeed);
        map.putFloat("scoreThrottle", scoreThrottle);
        map.putFloat("scoreBrake", scoreBrake);
        map.putFloat("scoreSummary", scoreSummary);
        map.putInt("fuelEconomyUnits", fuelEconomyUnits);
        map.putInt("tripUnits", tripUnits);
        map.putInt("hudVehicleSpeedUnits", hudVehicleSpeedUnits);
        map.putFloat("distanceToEmpty", distanceToEmpty);
        map.putLong("date", date);
        map.putInt("duration", duration);
        map.putFloat("fuelPrice", fuelPrice);
        return map;
    }

    public float getFloat(DataType dType){

        switch (dType){
            case AV_SPEED:
                return avSpeed;
            case FUEL_EC:
                return fuelEc;
            case DISTANCE:
                return distance;
            case ECO_STOP:
                break;
            case SCORE_SPEED:
                break;
            case SCORE_THROTTLE:
                break;
            case SCORE_BRAKE:
                break;
            case SCORE_SUMMARY:
                break;
            case FUEL_EC_UNITS:
                break;
            case TRIP_UNITS:
                break;
            case SPEED_UNITS:
                break;
            case DIST_TO_EMPTY:
                break;
            case DATE:
                break;
            case DURATION:
                break;
            case ID:
                break;
            case FUEL_PRICE:
                return fuelPrice;
        }
        return 0;
    }

    public String getUnits(DataType dType){

        switch (dType){
            case AV_SPEED:
                return Constants.getHUDVehicleSpeedString(hudVehicleSpeedUnits);
            case FUEL_EC:
                return Constants.getFuelEcString(fuelEconomyUnits);
            case DISTANCE:
                return Constants.getTripUnitString(tripUnits);
            case ECO_STOP:
                break;
            case SCORE_SPEED:
                break;
            case SCORE_THROTTLE:
                break;
            case SCORE_BRAKE:
                break;
            case SCORE_SUMMARY:
                break;
            case FUEL_EC_UNITS:
                break;
            case TRIP_UNITS:
                break;
            case SPEED_UNITS:
                break;
            case DIST_TO_EMPTY:
                break;
            case DATE:
                break;
            case DURATION:
                break;
            case ID:
                break;
        }
        return "";
    }

    public static String getTitle(DataType dType){

        switch (dType){
            case AV_SPEED:
                return "Average Speed";
            case FUEL_EC:
                return "Fuel Economy";
            case DISTANCE:
                return "Trip Distance";
            case ECO_STOP:
                break;
            case SCORE_SPEED:
                break;
            case SCORE_THROTTLE:
                break;
            case SCORE_BRAKE:
                break;
            case SCORE_SUMMARY:
                break;
            case FUEL_EC_UNITS:
                break;
            case TRIP_UNITS:
                break;
            case SPEED_UNITS:
                break;
            case DIST_TO_EMPTY:
                break;
            case DATE:
                break;
            case DURATION:
                break;
            case ID:
                break;
        }
        return "";
    }

    public float getFuelCost(){

        return (fuelPrice*4.54609f*(distance/fuelEc));
    }

    public float getFuelLitres(){

        return (4.54609f*(distance/fuelEc));
    }

    public String getFuelEcString(){
        return Constants.getFuelEcString(fuelEconomyUnits);
    }

    public String getHUDVehicleSpeedString(){
        return Constants.getHUDVehicleSpeedString(hudVehicleSpeedUnits);
    }

    public String getTripUnitString(){
        return Constants.getTripUnitString(tripUnits);
    }
}
