package com.jlr.ica.app2;

public class Constants {

    public static final String NOTIFICATION_PATH = "/notification";
    public static final String NOTIFICATION_TIMESTAMP = "timestamp";
    public static final String NOTIFICATION_TITLE = "title";
    public static final String NOTIFICATION_CONTENT = "content";

    public static final String NOTIFICATION_ARRAYLIST = "dataMapList";

    public static final String ACTION_DISMISS = "com.jlr.ica.app2.DISMISS";

    public static int WEEK_SECONDS = 604800000;

    public final static String NOTIFICATION_GROUP = "group_trip_summary";

    public static String getFuelEcString(int i){
        switch (i){
            case 0:
                return "mpg";
            case 1:
                return "mpl";
            case 2:
                return "km/l";
            case 3:
                return "l/100km";
        }
        return "";
    }

    public static String getHUDVehicleSpeedString(int i){
        switch (i){
            case 0:
                return "km/h";
            case 1:
                return "mph";
        }
        return "";
    }

    public static String getTripUnitString(int i){
        switch (i){
            case 0:
                return "km";
            case 1:
                return "miles";
        }
        return "";
    }
}
