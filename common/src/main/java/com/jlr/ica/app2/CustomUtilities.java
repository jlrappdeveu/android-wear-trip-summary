package com.jlr.ica.app2;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by luke on 17/02/2016.
 */
public class CustomUtilities {

    /**
     * Return date in specified format.
     * @param milliSeconds Date in milliseconds
     * @param dateFormat Date format
     * @return String representing date in specified format
     */
    public static String getDate(long milliSeconds, String dateFormat)
    {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    public static Calendar getDate(long milliSeconds)
    {
        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return calendar;
    }

    public static int getAgeDays(long milliSeconds){
        Calendar date = Calendar.getInstance();
        date.setTimeInMillis(milliSeconds);

        Calendar now = Calendar.getInstance();
        now.setTimeInMillis(System.currentTimeMillis());

        if(now.get(Calendar.DAY_OF_YEAR)==date.get(Calendar.DAY_OF_YEAR)) {
            return 0;
        }
        for (int i =1; i<10; i++) {
            now.add(Calendar.DATE, -1);
            if(now.get(Calendar.DAY_OF_YEAR)==date.get(Calendar.DAY_OF_YEAR)) {
                return i;
            }
        }
        return -1;
    }
}
