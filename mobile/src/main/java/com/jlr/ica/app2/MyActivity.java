package com.jlr.ica.app2;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.bosch.myspin.serversdk.MySpinException;
import com.bosch.myspin.serversdk.MySpinServerSDK;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;
import com.jlr.ica.csi.CANServerSDK;
import com.jlr.ica.csi.IEcoCurrentTripListener;
import com.jlr.ica.csi.IHighFrequencyDataListener;
import com.jlr.ica.csi.ILowFrequencyDataListener;

import java.net.SocketException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;






public class MyActivity extends Activity implements MySpinServerSDK.ConnectionStateListener, IHighFrequencyDataListener, IEcoCurrentTripListener, ILowFrequencyDataListener {

    private static final String TAG = "PhoneActivity";
    private GoogleApiClient mGoogleApiClient;

    ArrayList<EcoDataEntry> ecoDataEntries;
    ArrayList<String> ecoDataStrings;
    ArrayAdapter arrayAdapter;
    ListView lv;

    EcoDataEntry currentEcoDataEntry = null;

    DBHelper mydb;

    TextView displayTv;

    // CSI Port data
    private final int CSI_WIFI_PORT = 8080;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        //Register Application with the mySPIN server
        try {
            MySpinServerSDK.sharedInstance().registerApplication(getApplication());
        } catch (MySpinException e) {
            e.printStackTrace();
        }

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle connectionHint) {
                        Log.d(TAG, "onConnected: " + connectionHint);
                    }
                    @Override
                    public void onConnectionSuspended(int cause) {
                        Log.d(TAG, "onConnectionSuspended: " + cause);
                    }
                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(ConnectionResult result) {
                        Log.d(TAG, "onConnectionFailed: " + result);
                    }
                })
                .addApi(Wearable.API)
                .build();

        //Data base
        mydb = new DBHelper(this);
        ecoDataEntries = mydb.getDataEntries();

        // Select Layout
        setCorrectLayout();

    }

    private void layoutDisconnected(){
        setContentView(R.layout.activity_my);

        //Send Notification
        Button button = (Button) findViewById(com.jlr.ica.app2.R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendNotification();
            }
        });

        // ListView
        ecoDataStrings = new ArrayList<>();
        arrayAdapter=new ArrayAdapter(this,android.R.layout.simple_list_item_1, ecoDataStrings);
        lv = (ListView)findViewById(R.id.list_view);
        lv.setAdapter(arrayAdapter);
        refreshList();
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
                mydb.deleteEntry(ecoDataEntries.get(pos).id);
                refreshList();
            }
        });

        Button button2 = (Button) findViewById(com.jlr.ica.app2.R.id.button2);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addJourney(new EcoDataEntry());
            }
        });

        Button button3 = (Button) findViewById(com.jlr.ica.app2.R.id.button3);
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addJourney(currentEcoDataEntry);
            }
        });

        // Speed readout
        displayTv = (TextView) findViewById(R.id.display);
    }

    private void layoutConnected(){
        setContentView(R.layout.activity_connected);

        // Speed readout
        displayTv = (TextView) findViewById(R.id.display);
        displayTv.setText("No Data Connection");
    }

    private void setCorrectLayout(){

        Log.d(TAG, " : setCorrectLayout()");
        /*
        Make sure objects are reinitialised.
        http://stackoverflow.com/a/8781581
        As with VoiceMemo, create an initaliseUI method
        */

        if(MySpinServerSDK.sharedInstance().isConnected()) {
            Log.d(TAG, " : setCorrectLayout() : isConnected");
            layoutConnected();
        }
        else
        {
            Log.d(TAG ," : setCorrectLayout() : NOTconnected");
            layoutDisconnected();
        }
    }

    private void addJourney(EcoDataEntry ecoDataEntry) {
        mydb.addEntry(ecoDataEntry);
        refreshList();
    }


    public void registerApplication(final Application application, final int portNo) throws SocketException {
        // CSI registration
        try {
            CANServerSDK.sharedInstance().registerApplication(this.getApplication(), portNo);

        } catch (final SocketException e) {

            e.printStackTrace();

        }
    }


    public void unRegisterIHighFrequencyDataListener(
            final IHighFrequencyDataListener listener){
        // Unregister listener for vehicle speed
        CANServerSDK.sharedInstance().unRegisterIHighFrequencyDataListener(this);
    }

    public void unRegisterApplication(final Application application){
        // Disconnect from CSI SDK
        CANServerSDK.sharedInstance().unRegisterApplication(getApplication());
    }

    public void refreshList() {
        ecoDataEntries = mydb.getDataEntries();
        ecoDataStrings.clear();
        for (EcoDataEntry dataEntry: ecoDataEntries){
            ecoDataStrings.add(dataEntry.getString());
        }
        arrayAdapter.notifyDataSetChanged();

    }

    public EcoDataEntry randomJourney(){
        EcoDataEntry ecoDataEntry = new EcoDataEntry();
        ecoDataEntry.avSpeed = 30 + (float) (Math.random()*30);
        ecoDataEntry.fuelEc = 30 + (float) (Math.random()*20);
        ecoDataEntry.distance = 0 + (float) (Math.random()*100);
        ecoDataEntry.ecoStopTimer = 30 + (int) (Math.random()*120);
        ecoDataEntry.scoreSpeed = 1 + (float) (Math.random()*4);
        ecoDataEntry.scoreThrottle = 1 + (float) (Math.random()*4);
        ecoDataEntry.scoreBrake = 1 + (float) (Math.random()*4);
        ecoDataEntry.scoreSummary = 30 + (float) (Math.random()*70);
        ecoDataEntry.fuelEconomyUnits = 0;
        ecoDataEntry.tripUnits = 1;
        ecoDataEntry.hudVehicleSpeedUnits = 1;
        ecoDataEntry.distanceToEmpty = 30 + (float) (Math.random()*500);
        ecoDataEntry.date = System.currentTimeMillis();
        ecoDataEntry.fuelPrice = 1.019f;
        return ecoDataEntry;
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();

        // When this activity gets started register for mySPIN connection events in // order to adapt views for the according connection state.
        try {
            MySpinServerSDK.sharedInstance().registerConnectionStateListener(this); } catch (MySpinException e) {
            e.printStackTrace();
        }

        try {
            registerApplication(getApplication(),CSI_WIFI_PORT);
            startNewJourney();
        } catch (SocketException e) {
            e.printStackTrace();
        }

        CANServerSDK.sharedInstance().registerIHighFrequencyDataListener(this,this);
        CANServerSDK.sharedInstance().registerIEcoCurrentTripListener(this, this);

    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();

        // When this activity gets stopped unregister for mySPIN connection events.
        try {
            MySpinServerSDK.sharedInstance().unregisterConnectionStateListener(this);
        }
        catch (MySpinException e) {
            e.printStackTrace();
        }

        //unRegisterApplication(getApplication());
        //unRegisterIHighFrequencyDataListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setCorrectLayout();
    }

    private void sendNotification() {
        if (mGoogleApiClient.isConnected()) {
            PutDataMapRequest dataMapRequest = PutDataMapRequest.create(Constants.NOTIFICATION_PATH);
            // Make sure the data item is unique. Usually, this will not be required, as the payload
            // (in this case the title and the content of the notification) will be different for almost all
            // situations. However, in this example, the text and the content are always the same, so we need
            // to disambiguate the data item by adding a field that contains teh current time in milliseconds.
            ArrayList<DataMap> dataMapList = new ArrayList<>();
            int listSize = ecoDataEntries.size();
            for (int i =listSize-1;i>=0;i--){
                dataMapList.add(ecoDataEntries.get(i).putToDataMap(new DataMap()));
            }
            Log.d("numberDataMapSent", String.valueOf(dataMapList.size()));

            dataMapRequest.getDataMap().putDouble(Constants.NOTIFICATION_TIMESTAMP, System.currentTimeMillis());
            dataMapRequest.getDataMap().putString(Constants.NOTIFICATION_TITLE, "Vehicle trip summary");
            dataMapRequest.getDataMap().putString(Constants.NOTIFICATION_CONTENT, "Well done, you achieved " + Math.round(ecoDataEntries.get(listSize - 1).fuelEc) + Constants.getFuelEcString(ecoDataEntries.get(listSize - 1).fuelEconomyUnits));
            dataMapRequest.getDataMap().putDataMapArrayList(Constants.NOTIFICATION_ARRAYLIST, dataMapList);
            PutDataRequest putDataRequest = dataMapRequest.asPutDataRequest();
            Wearable.DataApi.putDataItem(mGoogleApiClient, putDataRequest);
        }
        else {
            Log.e(TAG, "No connection to wearable available!");
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(com.jlr.ica.app2.R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == com.jlr.ica.app2.R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void startNewJourney(){
        currentEcoDataEntry = new EcoDataEntry(System.currentTimeMillis(),1.039f);

        currentEcoDataEntry.avSpeed = CANServerSDK.sharedInstance().getCurrentTripAverageSpeed();
        currentEcoDataEntry.fuelEc = (float) CANServerSDK.sharedInstance().getCurrentFuelEconomy();
        currentEcoDataEntry.distance = (float) CANServerSDK.sharedInstance().getCurrentTripDistance();
        currentEcoDataEntry.ecoStopTimer = (int) CANServerSDK.sharedInstance().getCurrentEcoStopTimer();
        currentEcoDataEntry.scoreSpeed = (float) CANServerSDK.sharedInstance().getCurrentSpeedScore();
        currentEcoDataEntry.scoreThrottle = (float) CANServerSDK.sharedInstance().getCurrentThrottleScore();
        currentEcoDataEntry.scoreBrake = (float) CANServerSDK.sharedInstance().getCurrentBrakeScore();
        currentEcoDataEntry.scoreSummary = (float) CANServerSDK.sharedInstance().getCurrentTripScore();
        currentEcoDataEntry.fuelEconomyUnits = CANServerSDK.sharedInstance().getFuelEconomyUnits();
        currentEcoDataEntry.tripUnits = 1;
        currentEcoDataEntry.hudVehicleSpeedUnits = 1;
        currentEcoDataEntry.distanceToEmpty = CANServerSDK.sharedInstance().getDistanceToEmpty();
        currentEcoDataEntry.duration = Math.round(3600 * currentEcoDataEntry.distance/currentEcoDataEntry.avSpeed);
    }

    @Override
    public void onVehicleSpeedChanged(final double vehicleSpeed) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                displayTv.setText("currSpd"+Math.round(vehicleSpeed)+" "+currentEcoDataEntry.getString());
            }
        });
    }

    @Override
    public void onPedalPositionChanged(double pedalPosition) {

    }

    @Override
    public void onSteeringWheelAngleChanged(int steeringWheelAngle) {

    }

    @Override
    public void onLateralAccelerationChanged(double lateralAcceleration) {

    }

    @Override
    public void onCurrentSpeedScorechanged(double currSpeedScore) {
        currentEcoDataEntry.scoreSpeed = (float) currSpeedScore;
    }

    @Override
    public void onCurrentThrottleScoreChanged(double currThrottleScore) {
        currentEcoDataEntry.scoreThrottle = (float) currThrottleScore;
    }

    @Override
    public void onCurrentBrakeScoreChanged(double currBrakeScore) {
        currentEcoDataEntry.scoreBrake = (float) currBrakeScore;
    }

    @Override
    public void onCurrentFuelEconomyChanged(double currFuelEconomy) {
        currentEcoDataEntry.fuelEc = (float) currFuelEconomy;
    }

    @Override
    public void onCurrentTripScoreChanged(int currTripScore) {
        currentEcoDataEntry.scoreSummary = (float) currTripScore;
    }

    @Override
    public void onCurrentTripDistanceChanged(double currTripDistance) {
        currentEcoDataEntry.distance = (float) currTripDistance;
        currentEcoDataEntry.duration = Math.round(3600 * currentEcoDataEntry.distance/currentEcoDataEntry.avSpeed);
    }

    @Override
    public void onCurrentEcoStopTimerChanged(double currEcoStopTimer) {
        currentEcoDataEntry.ecoStopTimer = (int) currEcoStopTimer;
    }

    @Override
    public void onCurrentTripAverageSpeedChanged(int currTripAverageSpeed) {
        currentEcoDataEntry.avSpeed = (float) currTripAverageSpeed;
        currentEcoDataEntry.duration = Math.round(3600 * currentEcoDataEntry.distance/currentEcoDataEntry.avSpeed);
    }

    @Override
    public void onFuelEconomyUnitsChanged(int economyUnits) {
        currentEcoDataEntry.fuelEconomyUnits = economyUnits;
    }

    @Override
    public void onStopStartStatusChanged(int stopStartStatus) {

    }

    @Override
    public void onTransShiftIndChanged(int transShiftInd) {

    }

    @Override
    public void onPowerModeChanged(int powerMode) {

    }

    @Override
    public void onBrakePressureChanged(double brakePressure) {

    }

    @Override
    public void onDistanceToEmptyChanged(int distanceToEmpty) {
        currentEcoDataEntry.distanceToEmpty = (float) distanceToEmpty;
        Log.d("distToEmp", String.valueOf(distanceToEmpty));
    }

    @Override
    public void onGearLeverChanged(int gearLeverPosition) {

    }

    @Override
    public void onLowFuelLevelChanged(int lowFuelLevelStatus) {

    }

    @Override
    public void onParkBrakeChanged(int parkBrakeStatus) {

    }

    @Override
    public void onConnectionStateChanged(boolean b) {
        setCorrectLayout();
        if(!MySpinServerSDK.sharedInstance().isConnected()) {
            Log.d(TAG, "MySpin disconnect");
            if (validateJourney()){
                addJourney(currentEcoDataEntry);
            }
            sendNotification();
        }
    }

    public boolean validateJourney(){
        if (currentEcoDataEntry==null){
            return false;
        }
        if (currentEcoDataEntry.distance<=0){
            return false;
        }
        if (currentEcoDataEntry.avSpeed<=0){
            return false;
        }

        return true;
    }
}
