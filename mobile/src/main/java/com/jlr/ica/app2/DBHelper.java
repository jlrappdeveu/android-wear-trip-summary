package com.jlr.ica.app2;

/**
 * Created by luke on 16/02/2016.
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class DBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "HistoryDB.db";
    public static final String HISTORY_TABLE_NAME = "history";
    public static final String HISTORY_COLUMN_ID = "id";
    public static final String[] HISTORY_COLUMN_NAMES = new String[]{
            "avSpeed",
            "fuelEc",
            "distance",
            "ecoStopTimer",
            "scoreSpeed",
            "scoreThrottle",
            "scoreBrake",
            "scoreSummary",
            "fuelEconomyUnits",
            "tripUnits",
            "hudVehicleSpeedUnits",
            "distanceToEmpty",
            "date",
            "fuelPrice"
    };
    public static final String[] HISTORY_COLUMN_TYPES = new String[]{
            "float",
            "float",
            "float",
            "integer",
            "float",
            "float",
            "float",
            "float",
            "integer",
            "integer",
            "integer",
            "float",
            "long",
            "float"
    };
    private HashMap hp;

    public DBHelper(Context context)
    {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String sqlStr = "create table "+HISTORY_TABLE_NAME+" " +
                "(id integer primary key";
        for (int i = 0; i<HISTORY_COLUMN_NAMES.length; i++){
            sqlStr += ","+HISTORY_COLUMN_NAMES[i] + " "+HISTORY_COLUMN_TYPES[i];
        }
        sqlStr += ")";
        Log.d("sqlStr",sqlStr);
        db.execSQL(sqlStr);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        String sqlStr = "DROP TABLE IF EXISTS " + HISTORY_TABLE_NAME;
        db.execSQL(sqlStr);
        Log.d("sqlStr", sqlStr);
        onCreate(db);
    }

    public boolean addEntry(EcoDataEntry data)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(HISTORY_COLUMN_NAMES[0], data.avSpeed);
        contentValues.put(HISTORY_COLUMN_NAMES[1], data.fuelEc);
        contentValues.put(HISTORY_COLUMN_NAMES[2], data.distance);
        contentValues.put(HISTORY_COLUMN_NAMES[3], data.ecoStopTimer);
        contentValues.put(HISTORY_COLUMN_NAMES[4], data.scoreSpeed);
        contentValues.put(HISTORY_COLUMN_NAMES[5], data.scoreThrottle);
        contentValues.put(HISTORY_COLUMN_NAMES[6], data.scoreBrake);
        contentValues.put(HISTORY_COLUMN_NAMES[7], data.scoreSummary);
        contentValues.put(HISTORY_COLUMN_NAMES[8], data.fuelEconomyUnits);
        contentValues.put(HISTORY_COLUMN_NAMES[9], data.tripUnits);
        contentValues.put(HISTORY_COLUMN_NAMES[10], data.hudVehicleSpeedUnits);
        contentValues.put(HISTORY_COLUMN_NAMES[11], data.distanceToEmpty);
        contentValues.put(HISTORY_COLUMN_NAMES[12], data.date);
        contentValues.put(HISTORY_COLUMN_NAMES[13], data.fuelPrice);
        db.insert(HISTORY_TABLE_NAME, null, contentValues);
        return true;
    }

    public Cursor getData(int id){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from "+HISTORY_TABLE_NAME+" where id="+id+"", null );
        return res;
    }

    public int numberOfRows(){
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, HISTORY_TABLE_NAME);
        return numRows;
    }

    public Integer deleteEntry (Integer id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(HISTORY_TABLE_NAME,
                "id = ? ",
                new String[] { Integer.toString(id) });
    }

    public ArrayList<Float> getAllEntries()
    {
        ArrayList<Float> array_list = new ArrayList<Float>();

        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from "+HISTORY_TABLE_NAME, null );
        res.moveToFirst();

        while(res.isAfterLast() == false) {
            array_list.add(res.getFloat(res.getColumnIndex(HISTORY_COLUMN_NAMES[0])));
            res.moveToNext();
        }
        return array_list;
    }

    public ArrayList<EcoDataEntry> getDataEntries()
    {
        ArrayList<EcoDataEntry> array_list = new ArrayList<EcoDataEntry>();

        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from "+HISTORY_TABLE_NAME, null );
        res.moveToFirst();

        while(res.isAfterLast() == false) {
            EcoDataEntry ecoData = new EcoDataEntry(res.getInt(0),
                    res.getFloat(1),
                    res.getFloat(2),
                    res.getFloat(3),
                    res.getInt(4),
                    res.getFloat(5),
                    res.getFloat(6),
                    res.getFloat(7),
                    res.getFloat(8),
                    res.getInt(9),
                    res.getInt(10),
                    res.getInt(11),
                    res.getInt(12),
                    res.getLong(13),
                    res.getFloat(14));
            Log.d("getFloat", String.valueOf(res.getFloat(14)));
            array_list.add(ecoData);
            res.moveToNext();
        }
        return array_list;
    }
}